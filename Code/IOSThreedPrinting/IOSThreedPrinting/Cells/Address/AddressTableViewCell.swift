//
//  AddressTableViewCell.swift
//  IOSThreedPrinting
//
//  Created by Majd Ajaj on 3/11/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import UIKit

class AddressTableViewCell: UITableViewCell {
    
    static let nibName = "AddressTableViewCell"
    
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var selectContainerView: UIView!
    @IBOutlet weak var selectView: UIView!
    
    @IBAction func editButtonPressed(_ sender: Any) {
        delegate.didTapEdit(indexPath)
    }
    
    var delegate : AddressTableViewCellDelegate!
    var indexPath : IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectView.layer.cornerRadius = selectView.frame.width/2
        selectContainerView.layer.cornerRadius = selectContainerView.frame.width/2
        selectContainerView.layer.borderColor = UIColor.gray.cgColor
        selectContainerView.layer.borderWidth = 2
        selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selectView.isHidden = !selected
    }
    
}

protocol AddressTableViewCellDelegate {
    func didTapEdit(_ indexPath : IndexPath)
}

