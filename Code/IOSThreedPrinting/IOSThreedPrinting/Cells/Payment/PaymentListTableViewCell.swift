//
//  PaymentListTableViewCell.swift
//  IOSThreedPrinting
//
//  Created by Majd Ajaj on 3/16/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import UIKit

class PaymentListTableViewCell: UITableViewCell {
    
    static let nibName = "PaymentListTableViewCell"
    
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var selectContainerView: UIView!
    @IBOutlet weak var selectView: UIView!
    
    @IBAction func editButtonPressed(_ sender: Any) {
        delegate.didTapEdit(indexPath)
    }
    
    var delegate : PaymentListTableViewCellDelegate!
    var indexPath : IndexPath!

    override func awakeFromNib() {
        super.awakeFromNib()
        selectContainerView.layer.borderColor = UIColor.gray.cgColor
        selectContainerView.layer.borderWidth = 2
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selectView.isHidden = !selected
    }
    
}

protocol PaymentListTableViewCellDelegate {
    func didTapEdit(_ indexPath : IndexPath)
}
