//
//  AppConstants.swift
//  IOSThreedPrinting
//
//  Created by Alaa Dergham on 3/10/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import Foundation
import UIKit

struct AppConstants {
    
    /// all keys regarding network request
    struct requestKeys {
        static let username = "userName"
        static let password = "password"
    }
    
    /** add the common constants between all requests
     */
    static func loadConstants (parameters: [String : String]) -> [String : String]{
        let constantsDictionary = ["" : ""] /// please if there is any repeated params is request add them here
        
        let newParameters = parameters.merging(constantsDictionary) { (_, new) in new }
        
        return newParameters
    }
}
