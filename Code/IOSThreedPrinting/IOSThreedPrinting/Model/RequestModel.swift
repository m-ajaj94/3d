//
//  RequestModel.swift
//  IOSThreedPrinting
//
//  Created by Alaa Dergham on 3/10/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class RequestModel{
    
    //TODO: Error handling
    
    private static let defaultsHashKey = "hashValue"
    

    private static func getURL(function : String) -> String{
        return ServerURL.url + function
    }
    
    // new
    public static func get(function : String, parameters : [String : String], completionHandler : @escaping (Data?, Error?) -> ()) {
        if Helper.networking.isConnectedToInternet() {
            let url = getURL(function: function)
            print(url)
            print(parameters)
            Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: ["Content-Type" : "application/json; charset=utf-8"]).responseData { response in
                switch response.result{
                case .success(let value):
                    completionHandler(value, nil)
                case .failure:
                    print("Add Error Model")
                    //TODO: returning suitable error model (should be created)
                    //                    let errorObject = ErrorModel(errorCode: "500", errorMessage: "There was an error\nPlease try again! ".localized())
                    //                    completionHandler(errorObject,error)
                }
            }
        } else {
            //TODO: returning suitable error model (should be created)
        }
    }
    // new
    public static func post(function : String, parameters : [String:String], completionHandler : @escaping (Any?, Error?)->()){
        if Helper.networking.isConnectedToInternet(){
            let newParameters = AppConstants.loadConstants(parameters: parameters)
            let url = getURL(function: function)
            print(newParameters)
            print(url)
            Alamofire.request(url, method: .post, parameters: newParameters, encoding: URLEncoding.default, headers : ["Content-Type" : "application/x-www-form-urlencoded"]).responseData{
                response in
                switch response.result {
                case .success(let value):
                    completionHandler(value,nil)
                case .failure:
                    print("Add Error Model")
                    //TODO: returning suitable error model (should be created)
                    //                    let errorObject = ErrorModel(errorCode: "500", errorMessage: "There was an error\nPlease try again! ".localized())
                    //                    completionHandler(errorObject,error)
                }
            }
        }
        else{
            //TODO: returning suitable error model (should be created)
            //            let errorObject = ErrorModel(errorCode: "-220", errorMessage: "There isn't internet connection\nPlease check the internet connection and try again".localized())
            //            completionHandler(errorObject, NSError(domain: "", code: -220, userInfo: nil))
        }
    }
    
    // new
    public static func put(function : String, parameters : [String:String], completionHandler : @escaping (Any?, Error?)->()){
        if Helper.networking.isConnectedToInternet(){
            let newParameters = AppConstants.loadConstants(parameters: parameters)
            let url = getURL(function: function)
            print(newParameters)
            print(url)
            Alamofire.request(url, method: .put, parameters: newParameters, encoding: URLEncoding.default, headers : ["Content-Type" : "application/x-www-form-urlencoded"]).responseData{
                response in
                switch response.result {
                case .success(let value):
                    completionHandler(value,nil)
                case .failure(let error):
                    print("Add Error Model \(error)")
                    //TODO: returning suitable error model (should be created)
                    //                    let errorObject = ErrorModel(errorCode: "500", errorMessage: "There was an error\nPlease try again! ".localized())
                    //                    completionHandler(errorObject,error)
                }
            }
        }
        else{
            //TODO: returning suitable error model (should be created)
            //            let errorObject = ErrorModel(errorCode: "-220", errorMessage: "There isn't internet connection\nPlease check the internet connection and try again".localized())
            //            completionHandler(errorObject, NSError(domain: "", code: -220, userInfo: nil))
        }
    }
    // new
    public static func delete(function : String, parameters : [String:String], completionHandler : @escaping (Any?, Error?)->()){
        if Helper.networking.isConnectedToInternet(){
            let newParameters = AppConstants.loadConstants(parameters: parameters)
            let url = getURL(function: function)
            print(newParameters)
            print(url)
            Alamofire.request(url, method: .delete, parameters: newParameters, encoding: URLEncoding.default, headers : ["Content-Type" : "application/x-www-form-urlencoded"]).responseData{
                response in
                switch response.result {
                case .success(let value):
                    completionHandler(value,nil)
                case .failure(let error):
                    print("Add Error Model \(error)")
                    //TODO: returning suitable error model (should be created)
                    //                    let errorObject = ErrorModel(errorCode: "500", errorMessage: "There was an error\nPlease try again! ".localized())
                    //                    completionHandler(errorObject,error)
                }
            }
        }
        else{
            //TODO: returning suitable error model (should be created)
            //            let errorObject = ErrorModel(errorCode: "-220", errorMessage: "There isn't internet connection\nPlease check the internet connection and try again".localized())
            //            completionHandler(errorObject, NSError(domain: "", code: -220, userInfo: nil))
        }
    }
}

