//
//  ResponseModel.swift
//  IOSThreedPrinting
//
//  Created by Alaa Dergham on 3/19/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import Foundation
import SwiftyJSON
class AuthenticationResponseModel {
    var responseArray : [String] = []
    init(_ json: JSON) {
        print(json)
        if let responseArray = json.array{
            for responseString in responseArray {
                self.responseArray.append(responseString.string!)
            }
        } else {
            self.responseArray = []
        }
    }
}
//[
//    "Passwords must be at least 8 characters.",
//    "Passwords must have at least one digit ('0'-'9').",
//    "Passwords must have at least one uppercase ('A'-'Z').",
//    "Passwords must use at least 6 different characters."
//]
