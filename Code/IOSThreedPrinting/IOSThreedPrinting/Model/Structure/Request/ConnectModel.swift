//
//  ConnectModel.swift
//  IOSThreedPrinting
//
//  Created by Alaa Dergham on 3/23/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import Foundation
import SwiftyJSON

class ConnectModel {
    var accessToken : String?
    var expiresIn : Int?
    var tokenType : String?
    
    init(_ json: JSON) {
        if let accessToken = json["access_token"].string {
            self.accessToken = accessToken
        }
        if let expiresIn = json["expires_in"].int {
            self.expiresIn = expiresIn
        }
        if let tokenType = json["token_type"].string {
            self.tokenType = tokenType
        }
    }
    
    /** connect function
     */
    static func connect(_ parameters: [String : String], completionHandler: @escaping(ConnectModel?, Error?) ->()){
        RequestModel.get(function:  Request.connectAPI(.token), parameters: parameters) { (data, error) in
            if error == nil {
                completionHandler(ConnectModel(JSON(data!)), nil)
            } else {
                //completionHandler(ConnectModel(JSON(data!)), nil)
                //TODO: error handling
            }
            
            //completionHandler (SignInModel(json : JSON(data!), selectedGSM: selectedGSM), nil)
            
            //completionHandler(data as! ErrorModel, error)
            
            
        }
    }
    
}

