//
//  RegisterCustomerRequestModel.swift
//  IOSThreedPrinting
//
//  Created by Alaa Dergham on 3/11/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import Foundation
import SwiftyJSON

class RegisterCustomerModel {
    
    var firstName : String?
    var lastName : String?
    var email : String?
    var photo : String?
    var userName : String?
    var password : String?
    
    //TODO: if the class used in response, should implement an initializer
    
    static func getDict(_ firstName: String, _ lastName: String, _ email: String, _ photo: String, _ userName: String, _ password: String) -> [String : String]{
        var dict : [String: String] = [:]
        dict = ["firstName": userName, "lastName" : lastName, "email" : email, "photo" : photo, "userName" : userName, "password" : password]
        return dict
    }
}
