//
//  CustomerAddressModel.swift
//  IOSThreedPrinting
//
//  Created by Alaa Dergham on 3/16/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import Foundation
import SwiftyJSON

class CustomerAddressModel{
    
    var customerId : String?
    var contactName : String?
    var countryCode : String?
    var mobile : String?
    var streetAddress : String?
    var streetAddressNumberPOBox : String?
    var apartmentSuiteUnitBuildingFloor : String?
    var country : String?
    var stateProvinceCounty : String?
    var city : String?
    var zipPostalCode : String?
    var securityAccessCode : String?
    var isDefaultShipping : Bool?
    var isDefaultBilling : Bool?
    
    init(_ json : JSON) {
        if let customerId = json["customerId"].string {
            self.customerId = customerId
        }
        if let contactName = json["contactName"].string {
            self.contactName = contactName
        }
        if let countryCode = json["countryCode"].string {
            self.countryCode = countryCode
        }
        if let mobile = json["mobile"].string {
            self.mobile = mobile
        }
        if let streetAddress = json["streetAddress"].string {
            self.streetAddress = streetAddress
        }
        if let streetAddressNumberPOBox = json["streetAddressNumberPOBox"].string {
            self.streetAddressNumberPOBox = streetAddressNumberPOBox
        }
        if let apartmentSuiteUnitBuildingFloor = json["apartmentSuiteUnitBuildingFloor"].string {
            self.apartmentSuiteUnitBuildingFloor = apartmentSuiteUnitBuildingFloor
        }
        if let country = json["country"].string {
            self.country = country
        }
        if let stateProvinceCounty = json["stateProvinceCounty"].string {
            self.stateProvinceCounty = stateProvinceCounty
        }
        if let city = json["city"].string {
            self.city = city
        }
        if let zipPostalCode = json["zipPostalCode"].string {
            self.zipPostalCode = zipPostalCode
        }
        if let securityAccessCode = json["securityAccessCode"].string {
            self.securityAccessCode = securityAccessCode
        }
        if let isDefaultShipping = json["isDefaultShipping"].bool {
            self.isDefaultShipping = isDefaultShipping
        }
        if let isDefaultBilling = json["isDefaultBilling"].bool {
            self.isDefaultBilling = isDefaultBilling
        }
        
        
        
    }
    static func getDict(_ customerId: String, _ contactName: String, _ countryCode: String, _ mobile: String, _ streetAddress: String, _ streetAddressNumberPOBox: String, _ apartmentSuiteUnitBuildingFloor: String, _ country: String, _ stateProvinceCounty: String, _ city: String, _ zipPostalCode:String, _ securityAccessCode: String, _ isDefaultShipping: Bool, _ isDefaultBilling: Bool) -> [String : Any]{
        var dict : [String: Any] = [:]
        dict = ["customerId": customerId, "contactName" : contactName, "countryCode" : countryCode, "mobile" : mobile, "streetAddress" : streetAddress, "streetAddressNumberPOBox" : streetAddressNumberPOBox, "apartmentSuiteUnitBuildingFloor" : apartmentSuiteUnitBuildingFloor, "country" : country, "stateProvinceCounty" : stateProvinceCounty, "city" : city, "zipPostalCode" : zipPostalCode, "securityAccessCode" : securityAccessCode, "isDefaultShipping" : isDefaultShipping, "isDefaultBilling" : isDefaultBilling]
        return dict
    }
}

