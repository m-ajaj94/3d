//
//  SignInModel.swift
//  IOSThreedPrinting
//
//  Created by Alaa Dergham on 3/23/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import Foundation
import SwiftyJSON

class SignInModel {
    var responseString : String?
    
    init (_ json: JSON) {
        if let responseString = json.string {
            self.responseString  = responseString
        }
    }
    /** sign - in function
     */
    static func signIn(_ parameters: [String : String], completionHandler: @escaping(SignInModel?, Error?) ->()){
        RequestModel.get(function:  Request.userAPI(.login), parameters: parameters) { (data, error) in
            if error == nil {
                completionHandler(SignInModel(JSON(String(data: data!, encoding: .utf8)!)), nil)
            } else {
                completionHandler(SignInModel(JSON(data!)), nil)
            }
            
            //completionHandler (SignInModel(json : JSON(data!), selectedGSM: selectedGSM), nil)
            
            //completionHandler(data as! ErrorModel, error)
            
            
        }
    }
}

