//
//  ServerURL.swift
//  IOSThreedPrinting
//
//  Created by Alaa Dergham on 3/11/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import Foundation

struct ServerURL{
    
    private static let mode = 0
    private static let development = "https://threedprintingapi20180307121324.azurewebsites.net/api/"  //2
    private static let test = ""
    private static let live = ""
    static var oldUrl : String{
        return ""   
    }
    
    static var url : String {
            if mode == 0{
                return development
            }
            else if mode == 1{
                return test
            }
            else{
                return live
        }
    }
}
