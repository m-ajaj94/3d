//
//  FunctionType.swift
//  IOSThreedPrinting
//
//  Created by Alaa Dergham on 3/11/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import Foundation

/// enum for Customer functions
enum CustomerFunctions : String {
    case registerCustomer = "RegisterCustomer"
    case modifyCustomer = "ModifyCustomer"
    case getCustomerInfo = "GetCustomerInfo"
    case verifyCustomerEmail = "VerifyCustomerEmail"
    case addAddress = "AddAddress"
    case editAddress = "EditAddress"
    case deleteAddress = "DeleteAddress"
    case getCustomerAddresses = "GetCustomerAddresses"
    case getCustomerAddress = "GetCustomerAddress"
    case addPaymentMethod = "AddPaymentMethod"
    case getPaymentMethod = "GetPaymentMethod"
    case getAllPaymentMethods = "GetAllPaymentMethods"
    case deletePaymentMethod = "DeletePaymentMethod"
    case editPaymentMethod = "EditPaymentMethod"
    case addMaterial = "AddMaterial"
    case editMaterial = "EditMaterial"
    case deleteMaterial = "DeleteMaterial"
    case getMaterial = "GetMaterial"
    case getAllMaterials = "GetAllMaterials"
}

/// enum for Role functions
enum RoleFunctions : String {
    case toAdd = "ToAdd"
}

/// enum for Service functions
enum ServiceFunctions : String {
    case createThreeDOrder = "CreateThreeDOrder"
    
}

/// enum for connect function
enum ConnectFunction : String {
    case token = "token"
}

/// enum for Color functions
enum ColorFunctions : String {
    case allColors = "AllColors"
    case editColor = "EditColor"
    case deleteColor = "DeleteColor"
    case getColor = "GetColor"
    case addColor = "AddColor"
}

/// enum for Color functions
enum UserFunctions : String {
    case login = "Login"
}

struct Request {
    /** returning last part of customer API
     */
    static func customerAPI(_ function: CustomerFunctions) -> String {
        return "Customer/\(function.rawValue)"
    }
    /** returning last part of color API
     */
    static func colorAPI(_ function: ColorFunctions) -> String {
        return "Color/\(function.rawValue)"
    }
    /** returning last part of user API
     */
    static func userAPI(_ function: UserFunctions) -> String {
        return "User/\(function.rawValue)"
    }
    /** returning last part of role API
     */
    static func roleAPI(_ function: RoleFunctions) -> String {
        return "Role/\(function.rawValue)"
    }
    /** returning last part of service API
     */
    static func serviceAPI(_ function: ServiceFunctions) -> String {
        return "Service/\(function.rawValue)"
    }
    /** returning last part of connect API
     */
    static func connectAPI(_ function: ConnectFunction) -> String {
        return "connect/\(function.rawValue)"
    }
}

