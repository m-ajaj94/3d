//
//  Cache.swift
//  IOSThreedPrinting
//
//  Created by Alaa Dergham on 3/11/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import Foundation
import SwiftyJSON

enum Language : String{
    /// returns 1 as String for English
    case english = "1"
    
    /// returns 0 as String for Arabic
    case arabic = "0"
}

struct Cache{
    //TODO: add suit name for defaults for this applications
    public static let defaults = UserDefaults(suiteName: "threedprinting")!
    static let key = "cache"
    
    /** store all data regarding application
     */
    struct system{
        
        static let key = Cache.key + "/system"
        
        static let languageKey = system.key + "/appLanguage"
        
        private static let startingLanguage = system.language
        
        static var language : Language{
            if let languageValue = defaults.string(forKey: languageKey){
                return Language(rawValue: languageValue)!
            }
            changeLanguage(language: .english)
            return .english
        }
        
        static var layoutLanguage : Language{
            return startingLanguage
        }
        
        static func changeLanguage(language : Language){
            let deviceDefaults = UserDefaults.standard
            defaults.set(language.rawValue, forKey: languageKey)
            if language == .english{
                deviceDefaults.set(["en-US", "ar"], forKey: "AppleLanguages")
            }
            else{
                deviceDefaults.set(["ar", "en-US"], forKey: "AppleLanguages")
            }
            deviceDefaults.synchronize()
            defaults.synchronize()
        }
    }
    
    
    /** all data regarding user
     */
    struct userData{
        
        /// path to User Data in User Defaults (written as paths ... but actually they are keys)
        private static let key = Cache.key + "/userData"
        private static let currentUserKey = key + "/currentUser"
    }
}

