//
//  RegisterationViewController.swift
//  IOSThreedPrinting
//
//  Created by Alaa Dergham on 3/10/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import UIKit

class RegisterationViewController: UIViewController {
    @IBOutlet weak var headerContainerView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var uploadButton: UIButton!
    @IBOutlet weak var uploadPhotoAction: UIButton!
    @IBOutlet weak var separatorView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func test() -> String {
        print(RegisterCustomerModel.getDict("alaa", "test", "test", "test", "test", "test"))
        return Request.customerAPI(.registerCustomer)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
