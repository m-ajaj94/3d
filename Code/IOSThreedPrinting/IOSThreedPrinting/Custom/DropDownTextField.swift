//
//  DropDownTextField.swift
//  IOSThreedPrinting
//
//  Created by Majd Ajaj on 3/14/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import UIKit
import DropDown

class DropDownTextField: UITextField {
    
    private let dropDown = DropDown()
    var dropDownDelegate : DropDownTextFieldDelegate!
    var dataSource : [String]{
        get{
            return dropDown.dataSource
        }
        set{
            dropDown.dataSource = newValue
        }
    }

    override func awakeFromNib() {
        tintColor = .clear
        let coverView = UIView()
        coverView.frame.size = frame.size
        coverView.frame.origin = CGPoint.zero
        addSubview(coverView)
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapCoverView(_:)))
        tap.cancelsTouchesInView = true
        coverView.addGestureRecognizer(tap)
        setupDropDown()
    }
    
    private func setupDropDown(){
        dropDown.anchorView = self
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.direction = .bottom
        dropDown.selectionAction = {[unowned self] (index, item) in
            self.text = item
            self.dropDownDelegate.didChooseItemAtIndex(self, index)
        }
    }
    
    @objc private func didTapCoverView(_ sender : UITapGestureRecognizer){
        dropDown.show()
    }

}

protocol DropDownTextFieldDelegate {
    func didChooseItemAtIndex(_ textField: DropDownTextField, _ index: Int)
}
