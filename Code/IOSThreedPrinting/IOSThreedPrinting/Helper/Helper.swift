//
//  Helper.swift
//  IOSThreedPrinting
//
//  Created by Alaa Dergham on 3/11/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration

struct Helper{
    ///Contains constatns and methods related to across the app graphics
    struct graphic{
        static let redColor = UIColor(red: 208.0/255.0, green: 7.0/255.0, blue: 30.0/255.0, alpha: 1.0)
        static let greenColor = UIColor(red: 11.0/255.0, green: 161.0/255.0, blue: 28.0/255.0, alpha: 1.0)
        static let blueColor = UIColor(red: 52.0/255.0, green: 152.0/255.0, blue: 219.0/255.0, alpha: 1.0)
        static let mainColor = UIColor(red:0.82, green:0.07, blue:0.11, alpha:1.0)
        static let lightGreyColor = UIColor(red:0.82, green:0.07, blue:0.11, alpha:1.0)
        static let shadowRadius : CGFloat = 2
        static let shadowOpacity : Float = 0.7
    }
    
    struct networking{
        /** check if there is an internet connection
         */
        static func isConnectedToInternet() -> Bool{
            var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
            zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
            zeroAddress.sin_family = sa_family_t(AF_INET)
            
            let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
                $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                    SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
                }
            }
            var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
            if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
                return false
            }
            let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
            let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
            let ret = (isReachable && !needsConnection)
            
            return ret
        }
    }
    
    
    // TODO: size that fits for hint label
    struct hint {
        
        /** Showing Popover to user includes Hint about the faced problem
         */
        static func showPopover(message: String, sourceView :UIView, sender: UIViewController) {
            var errorViewController = ErrorPopoverViewController()
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            errorViewController = storyBoard.instantiateViewController(withIdentifier: "ErrorPopoverViewController") as! ErrorPopoverViewController
            errorViewController.modalPresentationStyle =   UIModalPresentationStyle.popover
            
            errorViewController.preferredContentSize = CGSize(width: sourceView.frame.width * 1.7, height: message.height(withConstrainedWidth: sourceView.frame.width * 1.7, font:UIFont.systemFont(ofSize: 15)) + 60)
            errorViewController.errorLabelString = message
            let popoverHintViewController = errorViewController.popoverPresentationController
            if sourceView.frame.origin.y < 100 {
                popoverHintViewController!.permittedArrowDirections = .up
            } else {
                popoverHintViewController!.permittedArrowDirections = .down
            }
            popoverHintViewController!.delegate = sender
            popoverHintViewController!.sourceView = sourceView
            popoverHintViewController!.sourceRect = sourceView.bounds
            
            errorViewController.delegate = sender as? ErrorPopoverViewControllerProtocol
            sender.present(errorViewController, animated: true, completion: nil)
        }
    }
    
    struct alert {
        
        /** showing an alert includes error message
         */
        static func showAlert (alertMessage : String, alertTitle : String, sender: UIViewController){
            let alertView = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
            alertView.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertActionStyle.default, handler: nil))
            sender.present(alertView, animated: true, completion: nil)
        }
    }
    
    struct userInteraction {
        
        /** disables User's interaction with specific UIView ... usually used with network requests
         */
        static func disable(sender: UIView) {
            DispatchQueue.main.async {
                sender.isUserInteractionEnabled = false
            }
        }
        
        /** enables User's interaction with specific UIView ... usually used with network requests
         */
        static func enable (sender: UIView) {
            DispatchQueue.main.async {
                sender.isUserInteractionEnabled = true
            }
        }
    }
    struct math {
        static func round(number: Double) -> Int{
            return Int(Darwin.round(100*(number))/100)
        }
        
        static func convertEnNumber(toFarsi number: String) -> String {
            var NumberString = ""
            let Formatter = NumberFormatter()
            let locale: NSLocale? = NSLocale(localeIdentifier: "EN")
            Formatter.locale = locale as Locale!
            let newNum = Formatter.number(from: number)
            if newNum != nil {
                NumberString = "0\(newNum!)"
                print("\(NumberString)")
            }
            return NumberString
        }
    }
    static func AddToolBar(textField: UITextField, selector: Selector){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 208.0/255.0, green: 7.0/255.0, blue: 30.0/255.0, alpha: 1)
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done".localized(), style: .done, target: self, action: selector)
        toolBar.setItems([doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    static func AddToolBarToTextView(textView: UITextView, selector: Selector){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 208.0/255.0, green: 7.0/255.0, blue: 30.0/255.0, alpha: 1)
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done".localized(), style: .done, target: self, action: selector)
        toolBar.setItems([doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        textView.inputAccessoryView = toolBar
    }
    
    
}
