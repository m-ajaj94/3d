//
//  AddPaymentViewController.swift
//  IOSThreedPrinting
//
//  Created by Majd Ajaj on 3/13/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import UIKit

class AddPaymentViewController: UIViewController {

    @IBOutlet weak var cardNumberTextField: UITextField!
    @IBOutlet weak var monthDropDownTextField: DropDownTextField!
    @IBOutlet weak var yearTextField: UITextField!
    @IBOutlet weak var securityCodeTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    @IBAction func doneButtonPressed(_ sender: Any) {
        
    }
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    var selectedTextField: UIView?
    var userData : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        setupView()
    }
    
    func setupView(){
        userData.fill("", 6)
        cancelButton.title = "CANCEL_BUTTON".localized()
        hidesKeyboardOnTap()
        doneButton.layer.cornerRadius = 5
        setupTextFields()
        doneButton.setTitle("PAYMENT_SAVE_BUTTON_TITLE".localized(), for: .normal)
    }
    
    func setupTextFields(){
        monthDropDownTextField.dropDownDelegate = self
        cardNumberTextField.placeholder = "PAYMENT_CARD_NUMBER_PLACEHOLDER".localized()
        monthDropDownTextField.placeholder = "PAYMENT_MONTH_PLACEHOLDER".localized()
        yearTextField.placeholder = "PAYMENT_YEAR_PLACEHOLDER".localized()
        securityCodeTextField.placeholder = "PAYMENT_SECURITY_CODE_PLACEHOLDER".localized()
        firstNameTextField.placeholder = "PAYMENT_CARD_NUMBER_PLACEHOLDER".localized()
        lastNameTextField.placeholder = "PAYMENT_LAST_NAME_PLACEHOLDER".localized()
        
        cardNumberTextField.tag = 1
        monthDropDownTextField.tag = 2
        yearTextField.tag = 3
        securityCodeTextField.tag = 4
        firstNameTextField.tag = 5
        lastNameTextField.tag = 6
        
        monthDropDownTextField.textAlignment = .center
        cardNumberTextField.keyboardType = .numberPad
        yearTextField.keyboardType = .numberPad
        
        cardNumberTextField.delegate = self
        yearTextField.delegate = self
        securityCodeTextField.delegate = self
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        
        //MARK: Data Source
        //TODO: Month List
        monthDropDownTextField.dataSource = ["Jan", "Feb", "March", "..."]
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.bottomConstraint.constant = 32
            } else {
                if self.bottomConstraint.constant == 32{
                    if selectedTextField == nil{
                        scrollView.contentOffset = CGPoint(x:0, y:scrollView.contentOffset.y + endFrame!.size.height)
                    }
                    else{
                        let topSpace = navigationController!.navigationBar.frame.height + UIApplication.shared.statusBarFrame.height
                        scrollView.contentOffset = CGPoint(x: 0, y:selectedTextField!.frame.origin.y - topSpace - 32)
                    }
                }
                self.bottomConstraint.constant = endFrame!.size.height + 32
            }
            UIView.animate(withDuration: duration,delay: TimeInterval(0),options: animationCurve,animations: {
                self.view.layoutIfNeeded()
            },completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension AddPaymentViewController: DropDownTextFieldDelegate, UITextFieldDelegate{
    
    func didChooseItemAtIndex(_ textField: DropDownTextField, _ index: Int) {
        userData[textField.tag - 1] = textField.dataSource[index]
        switch textField {
        case monthDropDownTextField:
            print("month select: \(index)")
            break
        default:
            break
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        userData[textField.tag - 1] = textField.text!
        selectedTextField = nil
    }
    
}
