//
//  PaymentListViewController.swift
//  IOSThreedPrinting
//
//  Created by Majd Ajaj on 3/13/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import UIKit

class PaymentListViewController: UIViewController {

    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBOutlet weak var editButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func addButtonPressed(_ sender: Any) {
        
    }
    @IBAction func editButtonPressed(_ sender: Any) {
        setEditing(!isEditing, animated: true)
    }
    
    var dataSource = ["Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6", "Title 7", "Title 8", "Title 9", "Title 10", "Title 11", "Title 12", "Title 13", "Title 14", "Title 15", "Title 16", "Title 17", "Title 18", "Title 19", "Title 20"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: PaymentListTableViewCell.nibName, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: PaymentListTableViewCell.nibName)
        tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .none)
        tableView.rowHeight = UITableViewAutomaticDimension
        editButton.title = "EDIT_BUTTON".localized()
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        addButton.isEnabled = !editing
        if editing {
            editButton.title = "DONE_BUTTON".localized()
        }
        else {
            editButton.title = "EDIT_BUTTON".localized()
        }
        super.setEditing(editing, animated: animated)
        tableView.setEditing(editing, animated: animated)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .default
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension PaymentListViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: PaymentListTableViewCell.nibName) as? PaymentListTableViewCell{
            cell.delegate = self
            cell.indexPath = indexPath
            cell.contentLabel.text = "****6589\nMohammed\nExpires 22/2022"
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        if tableView.isEditing{
            return .delete
        }
        return .none
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            dataSource.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
}

extension PaymentListViewController: PaymentListTableViewCellDelegate{
    
    func didTapEdit(_ indexPath: IndexPath) {
        print(indexPath.row)
    }
    
}
