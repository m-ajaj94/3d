//
//  AddAddressViewController.swift
//  IOSThreedPrinting
//
//  Created by Majd Ajaj on 3/13/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import UIKit
import DropDown

class AddAddressViewController: UIViewController {

    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabelContainerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contactNameTextField: UITextField!
    @IBOutlet weak var areaCodeTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var countryDropDownTextField: DropDownTextField!
    @IBOutlet weak var stateDropDownTextField: DropDownTextField!
    @IBOutlet weak var aptTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var zipTextField: UITextField!
    @IBOutlet weak var securityAccessCodeTextField: UITextField!
    @IBOutlet weak var dashLabel: UILabel!
    @IBOutlet weak var defaultShippingAddressLabel: UILabel!
    @IBOutlet weak var defaultShippingAddressSwitch: UISwitch!
    @IBOutlet weak var defaultBillingAddressLabel: UILabel!
    @IBOutlet weak var defaultBillingAddressSwitch: UISwitch!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    @IBAction func defaultShippingAddressSwitchChanged(_ sender: Any) {
        
    }
    @IBAction func defaultBillingAddressSwitchChanged(_ sender: Any) {
        
    }
    @IBAction func saveButtonPressed(_ sender: Any) {
        
    }
    @IBAction func didPressCancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    var selectedTextField: UIView?
    var userData : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cancelButton.title = "CANCEL_BUTTON_TITLE".localized()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        setupView()
    }
    
    func setupView(){
        userData.fill("", 10)
        hidesKeyboardOnTap()
        saveButton.layer.cornerRadius = 5
        setupTextFields()
        titleLabel.text = "ADD_ADDRESS_TITLE_HELP_LABEL".localized()
        defaultShippingAddressLabel.text = "ADD_ADDRESS_DEFAULT_SHIPPING_ADDRESS".localized()
        defaultBillingAddressLabel.text = "ADD_ADDRESS_DEFAULT_BILLING_ADDRESS".localized()
        saveButton.setTitle("ADD_ADDRESS_SAVE_BUTTON_TITLE".localized(), for: .normal)
    }
    
    func setupTextFields(){
        countryDropDownTextField.dropDownDelegate = self
        stateDropDownTextField.dropDownDelegate = self
        contactNameTextField.placeholder = "ADDRESS_NAME_PLACEHOLDER".localized()
        areaCodeTextField.placeholder = "+"
        phoneNumberTextField.placeholder = "ADDRESS_MOBILE_PLACEHOLDER".localized()
        addressTextField.placeholder = "ADDRESS_ADDRESS_PLACEHOLDER".localized()
        countryDropDownTextField.placeholder = "ADDRESS_COUNTRY_PLACEHOLDER".localized()
        stateDropDownTextField.placeholder = "ADDRESS_STATE_PLACEHOLDER".localized()
        aptTextField.placeholder = "ADDRESS_APT_PLACEHOLDER".localized()
        cityTextField.placeholder = "ADDRESS_CITY_PLACEHOLDER".localized()
        zipTextField.placeholder = "ADDRESS_ZIP_PLACEHOLDER".localized()
        securityAccessCodeTextField.placeholder = "ADDRESS_SECURITY_PLACEHOLDER".localized()
        contactNameTextField.tag = 1
        areaCodeTextField.tag = 2
        phoneNumberTextField.tag = 3
        addressTextField.tag = 4
        countryDropDownTextField.tag = 10
        stateDropDownTextField.tag = 6
        aptTextField.tag = 7
        cityTextField.tag = 8
        zipTextField.tag = 9
        securityAccessCodeTextField.tag = 10
        
        areaCodeTextField.textAlignment = .center
        phoneNumberTextField.keyboardType = .phonePad
        areaCodeTextField.keyboardType = .phonePad
        zipTextField.keyboardType = .numberPad
        
        contactNameTextField.delegate = self
        areaCodeTextField.delegate = self
        phoneNumberTextField.delegate = self
        addressTextField.delegate = self
        aptTextField.delegate = self
        cityTextField.delegate = self
        zipTextField.delegate = self
        securityAccessCodeTextField.delegate = self
        
        //MARK: Data Source
        countryDropDownTextField.dataSource = ["Syria", "Lebanon", "Saudi Arabia"]
        stateDropDownTextField.dataSource = ["Damascus", "Homs", "Aleppo"]
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.bottomConstraint.constant = 32
            } else {
                if self.bottomConstraint.constant == 32{
                    if selectedTextField == nil{
                        scrollView.contentOffset = CGPoint(x:0, y:scrollView.contentOffset.y + endFrame!.size.height)
                    }
                    else{
                        let topSpace = navigationController!.navigationBar.frame.height + UIApplication.shared.statusBarFrame.height
                        scrollView.contentOffset = CGPoint(x: 0, y:selectedTextField!.frame.origin.y - topSpace - 32)
                    }
                }
                self.bottomConstraint.constant = endFrame!.size.height + 32
            }
            UIView.animate(withDuration: duration,delay: TimeInterval(0),options: animationCurve,animations: {
                self.view.layoutIfNeeded()
            },completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension AddAddressViewController: DropDownTextFieldDelegate, UITextFieldDelegate{
    
    func didChooseItemAtIndex(_ textField: DropDownTextField, _ index: Int) {
        userData[textField.tag - 1] = textField.dataSource[index]
        switch textField {
        case countryDropDownTextField:
            print("country select: \(index)")
            break
        case stateDropDownTextField:
            print("state select: \(index)")
            break
        default:
            break
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        userData[textField.tag - 1] = textField.text!
        selectedTextField = nil
    }
    
}
