//
//  CustomAnnotation.swift
//  IOSThreedPrinting
//
//  Created by Majd Ajaj on 3/21/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import UIKit
import MapKit

class CustomAnnotation: MKMarkerAnnotationView {

    internal override var annotation: MKAnnotation? { willSet { newValue.flatMap(configure(with:))}}
        
    func configure(with annotation: MKAnnotation) {
        guard annotation is MKPointAnnotation else { fatalError("Unexpected annotation type: \(annotation)") }
        clusteringIdentifier = String(describing: ClusterAnnotation.self)
    }

}
