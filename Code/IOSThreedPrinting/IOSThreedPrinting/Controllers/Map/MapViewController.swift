//
//  MapViewController.swift
//  IOSThreedPrinting
//
//  Created by Majd Ajaj on 3/21/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    
    var currentLocation: CLLocationCoordinate2D!
    var locations : [MKAnnotation]!
    
    @IBAction func locationButtonPressed(_ sender: Any) {
        setToInitial()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.register(CustomAnnotation.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        mapView.register(ClusterAnnotation.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultClusterAnnotationViewReuseIdentifier)
        mapView.showsUserLocation = true
        mapView.addAnnotations(locations)
        setToInitial()
    }
    
    func setToInitial(){
        //TODO:
        mapView.setRegion(MKCoordinateRegion(center: currentLocation, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)), animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension MapViewController: MKMapViewDelegate{
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
    }
    
}
