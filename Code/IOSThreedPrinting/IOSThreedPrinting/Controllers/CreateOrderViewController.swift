//
//  CreateOrderViewController.swift
//  IOSThreedPrinting
//
//  Created by Majd Ajaj on 3/20/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import UIKit
import MapKit

class CreateOrderViewController: UIViewController {

    @IBOutlet weak var button: UIButton!
    
    @IBAction func buttonPressed(_ sender: Any) {
        getLocation()
    }
    
    let locationManager = CLLocationManager()
    var currentLocation: CLLocationCoordinate2D?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowMap"{
            let controller = segue.destination as! MapViewController
            controller.currentLocation = currentLocation
            controller.locations = dummyAnnotations()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension CreateOrderViewController: CLLocationManagerDelegate{
    
    func getLocation(){
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        if CLLocationManager.locationServicesEnabled(){
            switch CLLocationManager.authorizationStatus(){
            case .authorizedWhenInUse, .authorizedAlways:
                locationManager.startUpdatingLocation()
                break
            case .denied, .restricted:
                let alert = UIAlertController(title: "LOCATION_ERROR_TITLE".localized(), message: "LOCATION_ERROR_MESSAGE".localized(), preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "OKAY".localized(), style: .default, handler: nil)
                alert.addAction(cancelAction)
                present(alert, animated: true, completion: nil)
                break
            case .notDetermined:
                locationManager.requestWhenInUseAuthorization()
            }
        }
        else{
            let alert = UIAlertController(title: "LOCATION_ERROR_TITLE".localized(), message: "LOCATION_ERROR_MESSAGE".localized(), preferredStyle: .alert)
            let settingsAction = UIAlertAction(title: "SETTINGS".localized(), style: .default, handler: { (action) in
                if let url = URL(string: "App-prefs:root=LOCATION_SERVICES") {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            })
            let cancelAction = UIAlertAction(title: "CANCEL".localized(), style: .default, handler: nil)
            alert.addAction(settingsAction)
            alert.addAction(cancelAction)
            present(alert, animated: true, completion: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations[0].coordinate
        locationManager.stopUpdatingLocation()
        performSegue(withIdentifier: "ShowMap", sender: self)
    }
    
    func dummyAnnotations() -> [MKAnnotation]{
        var array: [MKAnnotation] = []
        for _ in 0..<100{
            let id = arc4random().quotientAndRemainder(dividingBy: 2).remainder
            let x = (id == 0 ? 1 : -1) * Double(arc4random().quotientAndRemainder(dividingBy: 100000).remainder) * 0.00001 + currentLocation!.latitude
            let id2 = arc4random().quotientAndRemainder(dividingBy: 2).remainder
            let y = (id2 == 0 ? 1 : -1) * Double(arc4random().quotientAndRemainder(dividingBy: 100000).remainder) * 0.00001 + currentLocation!.longitude
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: x, longitude: y)
            let custom = CustomAnnotation()
            custom.annotation = annotation
            array.append(custom.annotation!)
        }
        return array
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        getLocation()
    }
    
}
