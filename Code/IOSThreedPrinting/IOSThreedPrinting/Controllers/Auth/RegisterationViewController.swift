//
//  RegisterationViewController.swift
//  IOSThreedPrinting
//
//  Created by Alaa Dergham on 3/10/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import UIKit

class RegisterationViewController: UIViewController {
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var chooseImageButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var linearView: UIView!
    @IBOutlet weak var usernameContainerView: UIView!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var usernameImageView: UIImageView!
    @IBOutlet weak var emailContainerView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailImageView: UIImageView!
    @IBOutlet weak var passwordContainerView: UIView!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordImageView: UIImageView!
    @IBOutlet weak var firstNameContainerView: UIView!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var firstNameImageView: UIImageView!
    @IBOutlet weak var lastNameContainerView: UIView!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var lastNameImageView: UIImageView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        navigationController!.popViewController(animated: true)
    }
    @IBAction func chooseImageButtonPressed(_ sender: Any) {
        shouldShowPickerModeAlert()
    }
    @IBAction func submitButtonPressed(_ sender: Any) {
        
    }
    
    var pickedProfileImage : UIImage?{
        didSet{
            imageView.image = pickedProfileImage
            chooseImageButton.setTitle("REGISTRATION_CHANGE_PHOTO".localized(), for: .normal)
        }
    }
    var userInput : [String] = ["", "", "", "", ""]
    var imagePickerController = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        navigationController!.interactivePopGestureRecognizer!.delegate = self
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        setupView()
    }
    
    func setupView(){
        //TEMP
        imageView.backgroundColor = .gray
        usernameImageView.backgroundColor = .gray
        emailImageView.backgroundColor = .gray
        passwordImageView.backgroundColor = .gray
        firstNameImageView.backgroundColor = .gray
        lastNameImageView.backgroundColor = .gray
        //
        hidesKeyboardOnTap()
        setupTextFields()
        chooseImageButton.setTitle("REGISTRATION_CHOOSE_PHOTO".localized(), for: .normal)
        submitButton.setTitle("REGISTRATION_SUBMIT_BUTTON_TITLE".localized(), for: .normal)
        submitButton.layer.cornerRadius = 8
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imageView.layer.cornerRadius = imageView.frame.width/2
        imageView.clipsToBounds = true
    }
    
    func setupTextFields(){
        usernameTextField.placeholder = "REGISRTRATION_USERNAME_PLACEHOLDER".localized()
        emailTextField.placeholder = "REGISRTRATION_EMAIL_PLACEHOLDER".localized()
        passwordTextField.placeholder = "REGISRTRATION_PASSWORD_PLACEHOLDER".localized()
        firstNameTextField.placeholder = "REGISRTRATION_FIRSTNAME_PLACEHOLDER".localized()
        lastNameTextField.placeholder = "REGISRTRATION_LASTNAME_PLACEHOLDER".localized()
        usernameTextField.delegate = self
        emailTextField.delegate = self
        passwordTextField.delegate = self
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        usernameTextField.tag = 1
        emailTextField.tag = 2
        passwordTextField.tag = 3
        firstNameTextField.tag = 4
        lastNameTextField.tag = 5
        passwordTextField.isSecureTextEntry = true
        emailTextField.keyboardType = .emailAddress
    }
    
    //MARK: Keyboard Notification
    @objc func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.bottomConstraint.constant = 16
            } else {
                if self.bottomConstraint.constant == 16{
                    scrollView.contentOffset = CGPoint(x:0, y:scrollView.contentOffset.y + endFrame!.size.height)
                }
                self.bottomConstraint.constant = endFrame!.size.height + 16
            }
            UIView.animate(withDuration: duration,delay: TimeInterval(0),options: animationCurve,animations: {
                self.view.layoutIfNeeded()
            },completion: nil)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .default
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension RegisterationViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            pickedProfileImage = pickedImage
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func shouldShowPickerModeAlert(){
        let alert = UIAlertController(title: "REGISTRATION_PICKER_MODE_TITLE".localized(), message: "REGISTRATION_PICKER_MODE_MESSAGE".localized(), preferredStyle: .actionSheet)
        let galleryAction = UIAlertAction(title: "REGISTRATION_CHOOSE_GALLERY_ACTION".localized(), style: .default) { (action) in
            self.imagePickerController.sourceType = .photoLibrary
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        let cameraAction = UIAlertAction(title: "REGISTRATION_CHOOSE_CAMERA_ACTION".localized(), style: .default) { (action) in
            self.imagePickerController.sourceType = .camera
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "REGISTRATION_CHOOSE_GALLERY_ACTION".localized(), style: .cancel)
        alert.addAction(galleryAction)
        alert.addAction(cameraAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
}

extension RegisterationViewController : UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        userInput[textField.tag - 1] = textField.text!
    }
    
}

extension RegisterationViewController : UIGestureRecognizerDelegate{
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return navigationController!.viewControllers.count > 1
    }
}

