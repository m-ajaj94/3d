//
//  SigninViewController.swift
//  IOSThreedPrinting
//
//  Created by Majd Ajaj on 3/12/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import UIKit

class SigninViewController: UIViewController {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var secondaryLabel: UILabel!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signinButton: UIButton!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    @IBAction func signinButtonPressed(_ sender: Any) {
        
    }
    @IBAction func signupButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "ShowSignup", sender: self)
    }
    @IBAction func forgotPasswordButtonPressed(_ sender: Any) {
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        setupView()
    }
    
    override func viewDidLayoutSubviews() {
        topConstraint.constant = (view.frame.height - 306)/2
        view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupTextFields()
    }
    
    func setupView(){
        hidesKeyboardOnTap()
        signinButton.layer.cornerRadius = 5
        signinButton.setTitle("SIGNIN_SIGNIN_BUTTON_TITLE".localized(), for: .normal)
        signupButton.setTitle("SIGNIN_SIGNUP_BUTTON_TITLE".localized(), for: .normal)
        forgotPasswordButton.setTitle("SIGNIN_FORGOT_PASSWORD_BUTTON_TITLE".localized(), for: .normal)
    }
    
    func setupTextFields(){
        usernameTextField.setPlaceHolder("SIGNIN_USERNAME_PLACEHOLDER".localized())
        passwordTextField.setPlaceHolder("SIGNIN_PASSWORD_PLACEHOLDER".localized())
        usernameTextField.tag = 1
        passwordTextField.tag = 2
        passwordTextField.isSecureTextEntry = true
    }
    
    
    //MARK: Keyboard Notification
    @objc func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.bottomConstraint.constant = 32
            } else {
                if self.bottomConstraint.constant == 32{
                    scrollView.contentOffset = CGPoint(x:0, y:scrollView.contentOffset.y + endFrame!.size.height)
                }
                self.bottomConstraint.constant = endFrame!.size.height + 32
            }
            UIView.animate(withDuration: duration,delay: TimeInterval(0),options: animationCurve,animations: {
                self.view.layoutIfNeeded()
            },completion: nil)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

