//
//  POSDetailsViewController.swift
//  IOSThreedPrinting
//
//  Created by Majd Ajaj on 4/11/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import UIKit

class POSDetailsViewController: UIViewController {

    @IBOutlet weak var imagePagerContainerView: UIView!
    @IBOutlet weak var ownerInfoContainerView: UIView!
    @IBOutlet weak var addressContainerView: UIView!
    @IBOutlet weak var openingHoursContainerView: UIView!
    @IBOutlet weak var servicesContainerView: UIView!
    @IBOutlet weak var contactInfoContainerView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userStatusView: UIView!
    @IBOutlet weak var addressTitleLabel: UILabel!
    @IBOutlet weak var addressImageView: UIImageView!{
        didSet{
            addressImageView.layer.cornerRadius = 3
            addressImageView.layer.shadowColor = UIColor.black.cgColor
            addressImageView.layer.shadowOffset = CGSize.zero
            addressImageView.layer.shadowRadius = 5
            addressImageView.layer.shadowOpacity = 0.1
        }
    }
    @IBOutlet weak var openingHoursTitleLabel: UILabel!
    @IBOutlet weak var openingHoursLabel: UILabel!
    @IBOutlet weak var serviceTitleLabel: UILabel!
    @IBOutlet weak var servicesLabel: UILabel!
    @IBOutlet weak var contactInfoTitleLabel: UILabel!
    @IBOutlet weak var contactInfoTableView: UITableView!{
        didSet{
            if contactInfoTableView != nil{
                contactInfoTableView.isScrollEnabled = false
                contactInfoTableView.register(UINib(nibName: String(describing: POSContactInfoTableViewCell.self), bundle: nil), forCellReuseIdentifier: "cell")
                contactInfoTableView.delegate = self
                contactInfoTableView.dataSource = self
                contactInfoTableView.separatorStyle = .none
            }
            
        }
    }
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    var imagePagerView: ImageSliderView!{
        didSet{
            if imagePagerView != nil{
                imagePagerContainerView.addSubview(imagePagerView)
            }
        }
    }
    
    var contactInfo: CGFloat = 3
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePagerView = ImageSliderView.instanciateFromNib()
        imagePagerView.dataSource = ["https://upload.wikimedia.org/wikipedia/commons/e/e5/HTTPS_icon.png", "https://upload.wikimedia.org/wikipedia/commons/e/e5/HTTPS_icon.png", "https://upload.wikimedia.org/wikipedia/commons/e/e5/HTTPS_icon.png", "https://upload.wikimedia.org/wikipedia/commons/e/e5/HTTPS_icon.png"]
        contactInfoTableView.reloadData()
        tableViewHeightConstraint.constant = contactInfo * 60
        view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        userImageView.layer.cornerRadius = userImageView.frame.size.width / 2
        userStatusView.layer.cornerRadius = userStatusView.frame.size.width / 2
        imagePagerView.frame.origin = CGPoint.zero
        imagePagerView.frame.size = imagePagerContainerView.frame.size
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension POSDetailsViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? POSContactInfoTableViewCell{
            return cell
        }
        return UITableViewCell()
    }
    
}
