//
//  HomeImageSliderView.swift
//  Market360
//
//  Created by Majd Ajaj on 3/27/18.
//  Copyright © 2018 Majd Ajaj. All rights reserved.
//

import UIKit
import Kingfisher

class ImageSliderView: UIView {

    @IBOutlet weak var pager: UIPageControl!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var dataSource: [String]!{
        didSet{
            loadData()
        }
    }
    var timer: Timer!{
        willSet{
            if newValue == nil && timer != nil{
                timer.invalidate()
            }
        }
    }
    
    static func instanciateFromNib() -> ImageSliderView{
        return Bundle.main.loadNibNamed(String(describing: self), owner: self, options: nil)![0] as! ImageSliderView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        scrollView.isPagingEnabled = true
        scrollView.delegate = self
    }
    
    func loadData(){
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(scrollToNext), userInfo: nil, repeats: true)
        scrollView.contentSize = CGSize(width: CGFloat(dataSource.count) * frame.size.width, height: scrollView.frame.size.height)
        pager.numberOfPages = dataSource.count
        pager.currentPage = 0
        for i in 0..<dataSource.count{
            let imageView = UIImageView(frame: CGRect(x: frame.size.width * CGFloat(i), y: 0, width: frame.size.width, height: frame.size.height))
            imageView.contentMode = .scaleAspectFit
            imageView.kf.setImage(with: URL(string: dataSource[i])!)
            scrollView.addSubview(imageView)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        scrollView.contentSize = CGSize(width: CGFloat(dataSource.count) * frame.size.width, height: scrollView.frame.size.height)
        layoutIfNeeded()
    }

}

extension ImageSliderView: UIScrollViewDelegate{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pager.currentPage = currentIndex
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        timer = nil
        debugPrint(self.scrollView)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(scrollToNext), userInfo: nil, repeats: true)
    }
    
    @objc func scrollToNext(){
        if currentIndex == dataSource.count - 1{
            UIView.animate(withDuration: 0.8, delay: 0, options: .curveEaseOut, animations: {
                self.scrollView.contentOffset = CGPoint.zero
            }, completion: nil)
        }
        else{
            UIView.animate(withDuration: 0.8, delay: 0, options: .curveEaseInOut, animations: {
                self.scrollView.contentOffset = CGPoint(x: CGFloat(self.currentIndex + 1) * self.scrollView.frame.size.width, y: 0)
            }, completion: nil)
        }
    }
    
    var currentIndex: Int{
        return Int(scrollView.contentOffset.x / scrollView.frame.size.width)
    }
}
