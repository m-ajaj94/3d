//
//  POSContactInfoTableViewCell.swift
//  IOSThreedPrinting
//
//  Created by Majd Ajaj on 4/12/18.
//  Copyright © 2018 Alaa Dergham. All rights reserved.
//

import UIKit

class POSContactInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
