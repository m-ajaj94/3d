//
//  FileListViewController.swift
//  FileBrowser
//
//  Created by Roy Marmelstein on 12/02/2016.
//  Copyright © 2016 Roy Marmelstein. All rights reserved.
//

import Foundation

class FileListViewController: UIViewController {
    
    // TableView
    @IBOutlet weak var tableView: UITableView!
    let collation = UILocalizedIndexedCollation.current()
    
    /// Data
    var didSelectFile: ((FBFile) -> ())?
    var didSelectPath: ((URL) -> ())?
    var mode: FileBrowserMode!
    var files = [FBFile]()
    var initialPath: URL?
    let parser = FileParser.sharedInstance
    let previewManager = PreviewManager()
    var sections: [[FBFile]] = []
    var allowEditing: Bool = false
    var selectedPathForFile: URL?
    
    // Search controller
    var filteredFiles = [FBFile]()
    let searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.searchBarStyle = .minimal
        searchController.searchBar.backgroundColor = UIColor.white
        searchController.dimsBackgroundDuringPresentation = false
        return searchController
    }()
    
    
    //MARK: Lifecycle
    convenience init (initialPath: URL) {
        self.init(initialPath: initialPath, showCancelButton: true, mode: .select)
    }
    
    convenience init (initialPath: URL, showCancelButton: Bool, mode: FileBrowserMode) {
        self.init(nibName: "FileBrowser", bundle: Bundle(for: FileListViewController.self))
        self.edgesForExtendedLayout = UIRectEdge()
        
        // Set initial path
        self.initialPath = initialPath
        self.title = initialPath.lastPathComponent
        self.mode = mode
        // Set search controller delegates
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.delegate = self
        
        if mode == .select {
            // Add dismiss button
            let dismissButton = UIBarButtonItem(image: UIImage(named: "CloseIcon"), style: .plain, target: self, action: #selector(FileListViewController.dismiss(button:)))
            let newButton = UIBarButtonItem(image: UIImage(named: "NewFolderIcon"), style: .plain, target: self, action: #selector(newFolder))
            self.navigationItem.rightBarButtonItems = [dismissButton, newButton]
        }
        else{
            let dismissButton = UIBarButtonItem(image: UIImage(named: "CloseIcon"), style: .plain, target: self, action: #selector(FileListViewController.dismiss(button:)))
            let newButton = UIBarButtonItem(image: UIImage(named: "NewFolderIcon"), style: .plain, target: self, action: #selector(newFolder))
            let saveButton = UIBarButtonItem(image: UIImage(named: "SaveIcon"), style: .plain, target: self, action: #selector(saveFile))
            self.navigationItem.rightBarButtonItems = [dismissButton, newButton, saveButton]
        }
    }
    
    @objc func saveFile(){
        self.dismiss(animated: true) {
            if self.didSelectPath != nil{
                self.didSelectPath!(self.initialPath!)
            }
        }
    }
    
    @objc func newFolder(){
        let alert = UIAlertController(title: "New Folder name", message: nil, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let createAction = UIAlertAction(title: "Create", style: .default) { (action) in
            let text = alert.textFields![0].text!
            if text == ""{
                let errorAlert = UIAlertController(title: "Error", message: "Folder name can't be empty", preferredStyle: .alert)
                let okayAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
                errorAlert.addAction(okayAction)
                self.present(errorAlert, animated: true, completion: nil)
            }
            else{
                let filemgr = FileManager.default
                let docsDirURL = self.initialPath!
                let url = docsDirURL.appendingPathComponent(text)
                if !filemgr.fileExists(atPath: url.path) {
                    do {
                        try filemgr.createDirectory(at: url, withIntermediateDirectories: false, attributes: nil)
                        print("Directory created at: \(url)")
                        self.prepareData()
                        self.tableView.reloadData()
                    } catch let error as NSError {
                        NSLog("Unable to create directory \(error.debugDescription)")
                        let errorAlert = UIAlertController(title: "Error", message: "Something went wrong", preferredStyle: .alert)
                        let okayAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
                        errorAlert.addAction(okayAction)
                        self.present(errorAlert, animated: true, completion: nil)
                    }
                }
                else{
                    let errorAlert = UIAlertController(title: "Error", message: "Folder already exists", preferredStyle: .alert)
                    let okayAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
                    errorAlert.addAction(okayAction)
                    self.present(errorAlert, animated: true, completion: nil)
                }
            }
        }
        alert.addAction(cancelAction)
        alert.addAction(createAction)
        alert.addTextField { (textField) in
            textField.clearButtonMode = .always
        }
        present(alert, animated: true, completion: nil)
    }
    
    deinit{
        if #available(iOS 9.0, *) {
            searchController.loadViewIfNeeded()
        } else {
            searchController.loadView()
        }
    }
    
    func prepareData() {
        // Prepare data
        if let initialPath = initialPath {
            files = parser.filesForDirectory(initialPath)
            indexFiles()
        }
    }
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        
        prepareData()
        
        // Set search bar
        tableView.tableHeaderView = searchController.searchBar
        
        // Register for 3D touch
        self.registerFor3DTouch()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Scroll to hide search bar
        self.tableView.contentOffset = CGPoint(x: 0, y: searchController.searchBar.frame.size.height)
        
        // Make sure navigation bar is visible
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @objc func dismiss(button: UIBarButtonItem = UIBarButtonItem()) {
        selectedPathForFile = nil
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Data
    
    func indexFiles() {
        let selector: Selector = #selector(getter: FBFile.displayName)
        sections = Array(repeating: [], count: collation.sectionTitles.count)
        if let sortedObjects = collation.sortedArray(from: files, collationStringSelector: selector) as? [FBFile]{
            for object in sortedObjects {
                let sectionNumber = collation.section(for: object, collationStringSelector: selector)
                sections[sectionNumber].append(object)
            }
        }
    }
    
    func fileForIndexPath(_ indexPath: IndexPath) -> FBFile {
        var file: FBFile
        if searchController.isActive {
            file = filteredFiles[(indexPath as NSIndexPath).row]
        }
        else {
            file = sections[(indexPath as NSIndexPath).section][(indexPath as NSIndexPath).row]
        }
        return file
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filteredFiles = files.filter({ (file: FBFile) -> Bool in
            return file.displayName.lowercased().contains(searchText.lowercased())
        })
        tableView.reloadData()
    }
    
}


