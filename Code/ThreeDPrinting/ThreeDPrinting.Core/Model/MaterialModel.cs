﻿namespace ThreeDPrinting.core
{
    public class MaterialModel
    {
        public int MaterialId { get; set; }
        public string MaterialName { get; set; }
        public CurrencyType Currency { get; set; }
        public int PricePerGram { get; set; }
        //public List<int> Colors { get; set; }
    }


}