﻿

namespace ThreeDPrinting.Core.Model
{
  public  class StreetAddressModel
    {
        public string StreetInfo { get; set; }
        public string ApartmentInfo { get; set; }
    }
}
