﻿using System;
using System.Collections.Generic;
using System.Linq;
using MvvmCross.Core.Platform;

namespace ThreeDPrinting.core
{
    public class ModifyCustomerModel
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public byte[] Photo { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Password;

        public ModifyCustomerModel(int customerID, string firstName, string lastName,  byte[] photo)
        {
            CustomerId = customerID;
            FirstName = firstName;
            LastName = lastName;
         
            Photo = photo.ToArray();
        }
    }
}