﻿using System;

namespace ThreeDPrinting.core
{
    public class PaymentMethod
    {

        public Guid CustomerID { get; set; }
        public Guid CardId { get; set; }
        public string CardNumber { get; set; }
        public string FirstName { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string CardCompany { get; set; }
        public bool IsSelected { get; set; }
        public string SecurityCode { get; set; }
        public string LastName { get; set; }

    }
}