using System;


namespace ThreeDPrinting.Core.Model
{
    public class CustomerAddressModel
    {
        public string ContactName { get; set; }
        public string CountryCode { get; set; }
        public string Mobile { get; set; }
       // public StreetAddressModel StreetAddress { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string SecurityAccessCode { get; set; }
        public bool DefaultShippingAddress { get; set; }
        public bool DefaultBillingAddress { get; set; }
        public string StreetAddress { get;  set; }
        public string StreetAddressNumberPOBox { get;  set; }
        public string ApartmentSuiteUnitBuildingFloor { get;  set; }
        public int AddressId { get; set; }
        public int CustomerId { get; set; }
        public bool IsSelected { get; set; }

      
    }


}
