﻿using System;

namespace ThreeDPrinting.core
{
    public class AddPaymentMethodModel
    {
        public Guid CustomerId { get; set; }

        public string CardNumber { get; set; }
        public string FirstName { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string CardCompany { get; set; }
        public string SecurityCode { get; set; }
        public string LastName { get; set; }

        public AddPaymentMethodModel(Guid customerId, string cardNumber, string name, DateTime expiryDate, string cardCompany, string securityCode, string lastName)
        {
            CustomerId = customerId;
            CardNumber = cardNumber;
            FirstName = name;
            ExpiryDate = expiryDate;
            CardCompany = cardCompany;
            SecurityCode = securityCode;
            LastName = lastName;

        }


    }
}