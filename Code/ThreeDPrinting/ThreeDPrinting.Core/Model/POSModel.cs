﻿namespace ThreeDPrinting.core
{
    public class POSModel
    {
        public int POSId { get; set; }
        public string POSName { get; set; }
        public Location POSLocation { get; set; }

    }


}