﻿using System;

namespace ThreeDPrinting.core
{
    public class DeletePaymentMethod
    {

        public Guid CustomerId { get; set; }
        public Guid CardId { get; set; }

        public DeletePaymentMethod(Guid customerId, Guid cardId)
        {
            CustomerId = customerId;
            CardId = cardId;
        }


    }
}