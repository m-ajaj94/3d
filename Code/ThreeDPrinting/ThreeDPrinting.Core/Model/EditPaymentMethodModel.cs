﻿using System;

namespace ThreeDPrinting.core
{
    public class EditPaymentMethodModel
    {

        public Guid CustomerID { get; set; }
        public Guid CardId { get; set; }
        public string CardNumber { get; set; }
        public string FirstName { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string CardCompany { get; set; }
       
        public string LastName { get; set; }



        public EditPaymentMethodModel(Guid cardId, Guid customerId, string cardNumber, string name, DateTime expiryDate, string cardCompany,  string lastName)
        {
            CardId = cardId;
            CardNumber = cardNumber;
            FirstName = name;
            ExpiryDate = expiryDate;
            CardCompany = cardCompany;
            LastName = lastName;
            CustomerID = customerId;
        }

        
    }
}