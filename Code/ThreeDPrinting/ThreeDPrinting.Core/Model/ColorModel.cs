﻿namespace ThreeDPrinting.core
{
    public class ColorModel
    {
        public int ColorId { get; set; }
        public string ColorName { get; set; }
        public int R { get; set; }
        public int G { get; set; }
        public int B { get; set; }
        public bool IsSelected { get; set; }
    }
}