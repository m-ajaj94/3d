﻿using System.Linq;

namespace ThreeDPrinting.core
{
    public class AddCustomerModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public byte[] Photo { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public AddCustomerModel( string firstName, string lastName, string email, string password,string userName, byte[] photo)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Password = password;
            UserName = userName;
            Photo = photo.ToArray();
        }

    }
}