using System;
using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;
using MvvmCross.Core.Navigation;
using ThreeDPrinting.core;
using ThreeDPrinting.Core.Services;
using System.IO;
using MvvmCross.Plugins.PictureChooser;

namespace ThreeDPrinting.Core.ViewModels
{
    public class ModifyCustomerViewModel : MvxViewModel<int, bool>
    {
        private readonly IMvxNavigationService _navigationService;
        private readonly ICustomerService _customerService;
        private string _firstName;
        private string _lastName;

        private int _customerId;
        private byte[] _customerPhoto;
        public IMvxAsyncCommand ChoosePictureCommand { get; }

        public IMvxAsyncCommand SaveCommand { get; }
        private IMvxPictureChooserTask _pictureChooserTask;



        public string FirstName
        {
            get => _firstName;
            set => SetProperty(ref _firstName, value);
        }

        public string LastName
        {
            get => _lastName;
            set => SetProperty(ref _lastName, value);

        }



        public byte[] CustomerPhoto
        {
            get => _customerPhoto;
            set { _customerPhoto = value; RaisePropertyChanged(() => CustomerPhoto); }

        }


        public ModifyCustomerViewModel(IMvxNavigationService navigationService, ICustomerService customerService, IMvxPictureChooserTask pictureChooserTask)
        {
            _navigationService = navigationService;
            _customerService = customerService;
            _pictureChooserTask = pictureChooserTask;
            ChoosePictureCommand = new MvxAsyncCommand(DoChoosePicture);
            SaveCommand = new MvxAsyncCommand(Save);
        }

        public override Task Initialize()
        {
            return Task.WhenAll(base.Initialize(), LoadData());
        }

        public override void Prepare(int parameter)
        {
            _customerId = parameter;
        }

        private async Task LoadData()
        {
            var customer = await _customerService.GetCustomerAsync(_customerId);
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            CustomerPhoto = customer.Photo;
        }
        private async Task Save()
        {
            try
            {
                var modifyCustomerModel = new ModifyCustomerModel(_customerId, FirstName, LastName, CustomerPhoto);
                await _customerService.UpdateCustomerAsync(modifyCustomerModel);
                await _navigationService.Close(this, true);
            }
            catch (Exception e)
            {
                var exceptionString = e.Message;
            }

        }

        public async Task DoChoosePicture()
        {
            try
            {
                _pictureChooserTask.ChoosePictureFromLibrary(400, 95, OnPicture, () => { });

            }
            catch (Exception e)
            {
                var exceptionString = e.Message;
            }
        }
        public void OnPicture(Stream pictureStream)
        {
            try
            {
                var memoryStream = new MemoryStream();
                pictureStream.CopyTo(memoryStream);
                CustomerPhoto = memoryStream.ToArray();
            }
            catch (Exception e)
            {
                var exceptionString = e.Message;
            }
        }
    }
}