using System;
using System.Threading.Tasks;
using MvvmCross.Binding.Combiners;
using MvvmCross.Binding.Views;
using MvvmCross.Core.ViewModels;
using MvvmCross.Core.Navigation;
using MvvmCross.Platform.Platform;
using ThreeDPrinting.core;
using ThreeDPrinting.Core.Services;
using System.IO;


namespace ThreeDPrinting.Core.ViewModels
{
    public class CreateOrderforPrint3DDrawingServiceViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService _navigationService;
        private readonly IOrderService _orderService;
        
        

        public MvxObservableCollection<MaterialModel> Matelials { get; }
        public MvxObservableCollection<ColorModel> Colors { get; }
        public MvxObservableCollection<POSModel> POSs { get; }

        public IMvxAsyncCommand SubmitCommand { get; }
        public IMvxAsyncCommand ShowNearbyPOSsCommand { get; }

        private MaterialModel _selectedMaterial;
        public MaterialModel SelectedMaterial
        {
            get => _selectedMaterial; 
            set { _selectedMaterial = value; RaisePropertyChanged(() => SelectedMaterial); }
        }

        private int _minimumCharge;
        public int MinimumCharge
        {
            get => _minimumCharge; 
            set { _minimumCharge = value; RaisePropertyChanged(() => MinimumCharge); }
        }

        private int _miles;
        public int Miles
        {
            get =>  _miles; 
            set { _miles = value; RaisePropertyChanged(() => Miles); }
        }

        private string _comment;
        public string Comment
        {
            get => _comment;
            set { _comment = value; RaisePropertyChanged(() => Comment); }
        }

        private MvxObservableCollection<ColorModel> _selectedColors { get; }
        public MvxObservableCollection<ColorModel> SelectedColors
        {
            get => _selectedColors; 
            set {  RaisePropertyChanged(() => SelectedColors); }
        }


        public CreateOrderforPrint3DDrawingServiceViewModel(IMvxNavigationService navigationService, IOrderService orderService)
        {
            _navigationService = navigationService;
            _orderService = orderService;
            Matelials = new MvxObservableCollection<MaterialModel>();
            Colors = new MvxObservableCollection<ColorModel>();
            POSs = new MvxObservableCollection<POSModel>();
            // SubmitCommand = new MvxAsyncCommand();
            // ShowNearbyPOSsCommand = new MvxAsyncCommand();
        }

        public override Task Initialize()
        {
            //TODO: Add starting logic here

            return Task.WhenAll(base.Initialize(), LoadData());
        }

        

        private async Task LoadData()
        {
            var colors = await _orderService.GetAllColorsAsync();
            Colors.Clear();
            Colors.AddRange(colors);

            var materials = await _orderService.GetAllMaterialsAsync();
            Matelials.Clear();
            Matelials.AddRange(materials);

        }


    }
}