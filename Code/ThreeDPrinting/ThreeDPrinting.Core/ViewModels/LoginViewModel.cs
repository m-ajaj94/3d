using System;
using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;
using MvvmCross.Core.Navigation;
using ThreeDPrinting.core;
using ThreeDPrinting.Core.Services;
using System.IO;
using MvvmCross.Plugins.PictureChooser;

namespace ThreeDPrinting.Core.ViewModels
{
    public class LoginViewModel  : MvxViewModel
    {
        private readonly IMvxNavigationService _navigationService;
        private readonly ICustomerService _customerService;
        private string _userName;
        private string _password;
   
      
      
        public IMvxAsyncCommand LoginCommand { get; }


 

        public string UserName
        {
            get => _userName;
            set => SetProperty(ref _userName, value);
        }

        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
            
        }
    



        public LoginViewModel(IMvxNavigationService navigationService, ICustomerService customerService)
        {
            _navigationService = navigationService;
            _customerService = customerService;
           LoginCommand = new MvxAsyncCommand(Login);
        }
        
        public override Task Initialize()
        {
            return Task.WhenAll(base.Initialize(), LoadData());
        }



        private async Task LoadData()
        {

        }
        private async Task Login()
        {
            try
            {
                await _customerService.LoginAsync(UserName,Password);
                await _navigationService.Close(this);
            }
            catch (Exception e)
            {
                var exceptionString = e.Message;
            }

        }

    }
}