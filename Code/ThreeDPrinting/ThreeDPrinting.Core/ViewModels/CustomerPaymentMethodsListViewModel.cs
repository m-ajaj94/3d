using System;
using System.Linq;
using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;
using MvvmCross.Core.Navigation;
using ThreeDPrinting.core;
using ThreeDPrinting.Core.Services;

namespace ThreeDPrinting.Core.ViewModels
{
    public class CustomerPaymentMethodsListViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService _navigationService;
       
        private readonly ICustomerService _customerService;
        public MvxObservableCollection<PaymentMethod> PaymentMethods { get; }
        
        private bool _isEdit;

        public IMvxAsyncCommand<PaymentMethod> EditPaymentMethodCommand { get; }
        public IMvxAsyncCommand AddPaymentMethodCommand { get; }
        public IMvxAsyncCommand<DeletePaymentMethod> DeletePaymentMethodCommand { get; }
        public IMvxCommand ToggleCommand { get; }
        public bool IsEdit
        {
            get { return _isEdit; }
            set { SetProperty(ref _isEdit, value); }
        }


        public CustomerPaymentMethodsListViewModel(IMvxNavigationService navigationService, ICustomerService customerService)
        {
            _navigationService = navigationService;
            _customerService = customerService;
            PaymentMethods = new MvxObservableCollection<PaymentMethod>();
            EditPaymentMethodCommand = new MvxAsyncCommand<PaymentMethod>(EditPaymentMethod);
            AddPaymentMethodCommand = new MvxAsyncCommand(AddPaymentMethod);
            DeletePaymentMethodCommand = new MvxAsyncCommand<DeletePaymentMethod>(DeletePaymentMethod);
            ToggleCommand = new MvxCommand(Toggle);
        }
        
        public override Task Initialize()
        {
            return Task.WhenAll(base.Initialize(), LoadData());
        }
        
        private async Task LoadData()
        {
            var paymentMethods = await _customerService.GetAllPaymentMethodsAsync();
            PaymentMethods.Clear();
            PaymentMethods.AddRange(paymentMethods);
        }

        private void Toggle()
        {
            IsEdit = !IsEdit;
            var paymentMethods = PaymentMethods.ToList();
            PaymentMethods.Clear();
            PaymentMethods.AddRange(paymentMethods);
        }

        private async Task EditPaymentMethod(PaymentMethod paymentMethod)
        {
            var result = await _navigationService.Navigate<AddEditPaymentMethodViewModel, Guid?, bool>(paymentMethod.CardId);
            if (result)
                await LoadData();
        }
        private async Task AddPaymentMethod()
        {
            var result = await _navigationService.Navigate<AddEditPaymentMethodViewModel, bool>();
            if (result)
                await LoadData();
        }

        private async Task DeletePaymentMethod(DeletePaymentMethod model)
        {
        
            foreach (PaymentMethod paymentMethod in PaymentMethods)
            {
                if (paymentMethod.IsSelected)
                  
                    await _customerService.DeletePaymentMethodAsync(new DeletePaymentMethod(paymentMethod.CustomerID, paymentMethod.CardId));
            }
            
                await LoadData();
        }
    }
}