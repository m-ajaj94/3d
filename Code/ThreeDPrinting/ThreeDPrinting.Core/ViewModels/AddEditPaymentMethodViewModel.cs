using System;
using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;
using MvvmCross.Core.Navigation;
using ThreeDPrinting.core;
using ThreeDPrinting.Core.Services;
using System.IO;
using MvvmValidation;
using Plugin.Connectivity;

namespace ThreeDPrinting.Core.ViewModels
{
    public class AddEditPaymentMethodViewModel : MvxViewModel<Guid?, bool>
    {
        private readonly IMvxNavigationService _navigationService;
        private readonly ICustomerService _customerService;
        private string _cardNumber;
        private string _firstName;
        private DateTime _expiryDate;
        private string _cardCompany;
        private Guid? _cardId;
        private bool _isSelected;
        private bool _isEdit;
        private string _lastName;
        private string _securityCode;
        private Guid _customerId;
        private ValidationHelper _validator;


        public IMvxAsyncCommand SaveCommand { get; }
        
    

 

        public string CardNumber
        {
            get => _cardNumber;
            set
            {
                SetProperty(ref _cardNumber, value);
                RaisePropertyChanged(nameof(CanSave));
            }
        }

        public string FirstName
        {
            get => _firstName;
            set
            {
                SetProperty(ref _firstName, value);
                RaisePropertyChanged(nameof(CanSave));
            }

        }
        public string LastName
        {
            get => _lastName;
            set
            {
                SetProperty(ref _lastName, value);
                RaisePropertyChanged(nameof(CanSave));
            }

        }

        public string CardCompany
        {
            get => _cardCompany;
            set
            {
                SetProperty(ref _cardCompany, value);
                RaisePropertyChanged(nameof(CanSave));
            }

        }
        public DateTime ExpiryDate
        {
            get => _expiryDate;
            set
            {
                SetProperty(ref _expiryDate, value);
                RaisePropertyChanged(nameof(CanSave));
            }

        }
        public bool IsSelected
        {
            get => _isSelected;
            set => SetProperty(ref _isSelected, value);

        }

        public string SecurityCode
        {
            get => _securityCode;
            set
            {
                SetProperty(ref _securityCode, value);
                RaisePropertyChanged(nameof(CanSave));
            }

        }
        public bool IsEdit
        {
            get { return _isEdit; }
            set { SetProperty(ref _isEdit, value); }
        }




        public AddEditPaymentMethodViewModel(IMvxNavigationService navigationService, ICustomerService customerService)
        {
            _navigationService = navigationService;
            _customerService = customerService;
            SaveCommand = new MvxAsyncCommand(Save);
          
        }
        
        public override Task Initialize()
        {
            return Task.WhenAll(base.Initialize(), LoadData());
        }

        public override void Prepare(Guid? parameter)
        {
            _cardId = parameter;
            IsEdit = _cardId.HasValue;
        }

        private async Task LoadData()
        {
            if (IsEdit)
            {
                var paymentMethod = await _customerService.GetPaymentMethodAsync(_cardId.Value);
                CardNumber = paymentMethod.CardNumber;
                FirstName = paymentMethod.FirstName;
                LastName = paymentMethod.LastName;
                CardCompany = paymentMethod.CardCompany;
                ExpiryDate = paymentMethod.ExpiryDate;
                
            }
        }
        private async Task Save()
        {
            try
            {
                if (IsEdit)
                {
                    var editPaymentMethodModel = new EditPaymentMethodModel(_cardId.Value,_customerId, CardNumber,FirstName,ExpiryDate,CardCompany,LastName);
                    await _customerService.EditPaymentMethodAsync(editPaymentMethodModel);
                }
                else
                {
                    var addPaymentMethodModel = new AddPaymentMethodModel(_customerId, CardNumber, FirstName, ExpiryDate, CardCompany, SecurityCode,LastName);
                    await _customerService.AddPaymentMethodAsync(addPaymentMethodModel);
                }

                await _navigationService.Close(this, true);
            }
            catch (Exception e)
            {

            }

        }

        private ObservableDictionary<string, string> _errors;
        public ObservableDictionary<string, string> Errors
        {
            get => _errors;
            set
            {
                _errors = value;
                RaisePropertyChanged(() => Errors);
            }
        }

        public object CanSave => Validate();

        private bool Validate()
        {
            _validator = new ValidationHelper();
            _validator.AddRequiredRule(() => CardNumber, "Card Number is required.");
            _validator.AddRequiredRule(() => SecurityCode, "Security Code is required.");
            _validator.AddRequiredRule(() => FirstName, "First Name  is required.");
            _validator.AddRequiredRule(() => LastName, "Last Name is required.");
            _validator.AddRequiredRule(() => CardCompany, "Company is required.");
            _validator.AddRequiredRule(() => ExpiryDate, "Expiry Date is required.");

            var result = _validator.ValidateAll();
            Errors = result.AsObservableDictionary();
            return result.IsValid;
        }

        //private bool Connected()
        //{
        //    if (CrossConnectivity.Current.IsConnected)
        //        return true;
        //    return false;
            
        //}


    }
}