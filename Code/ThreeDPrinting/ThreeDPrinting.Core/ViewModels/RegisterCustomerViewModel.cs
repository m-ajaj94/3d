using System;
using System.IO;
using System.Threading.Tasks;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.PictureChooser;
using ThreeDPrinting.core;
using ThreeDPrinting.Core.Services;

namespace ThreeDPrinting.Core.ViewModels
{
    public class RegisterCustomerViewModel : MvxViewModel
    {

        private readonly IMvxNavigationService _navigationService;
        private readonly ICustomerService _customerService;

        private string _firstName;
        private string _lastName;
        private string _userName;
        private string _password;
        private string _email;
        private byte[] _customerPhoto;
        public IMvxAsyncCommand ChoosePictureCommand { get; }

        public IMvxAsyncCommand SaveCommand { get; }
        private IMvxPictureChooserTask _pictureChooserTask;

        public string FirstName
        {
            get => _firstName;
            set => SetProperty(ref _firstName, value);
        }

        public string LastName
        {
            get => _lastName;
            set => SetProperty(ref _lastName, value);

        }
        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }
        public string UserName
        {
            get => _userName;
            set => SetProperty(ref _userName, value);
        }


        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }


        public byte[] CustomerPhoto
        {
            get => _customerPhoto;
            set { _customerPhoto = value; RaisePropertyChanged(() => CustomerPhoto); }

        }

        public RegisterCustomerViewModel(IMvxNavigationService navigationService, ICustomerService customerService, IMvxPictureChooserTask pictureChooserTask)
        {
            _navigationService = navigationService;
            _customerService = customerService;
            _pictureChooserTask = pictureChooserTask;
            ChoosePictureCommand = new MvxAsyncCommand(DoChoosePicture);
            SaveCommand = new MvxAsyncCommand(Save);
        }
        public override Task Initialize()
        {
            return Task.WhenAll(base.Initialize());
        }

        public override void Prepare()
        {
            //_customerId = parameter;
        }


        private async Task Save()
        {
            try
            {
                var addCustomerModel = new AddCustomerModel( FirstName, LastName,Email,Password,UserName, CustomerPhoto);
                await _customerService.RegisterCustomerAsync(addCustomerModel);
                await _navigationService.Close(this);
            }
            catch (Exception e)
            {
                var exceptionString = e.Message;
            }

        }

        public async Task DoChoosePicture()
        {
            try
            {
                _pictureChooserTask.ChoosePictureFromLibrary(400, 95, OnPicture, () => { });

            }
            catch (Exception e)
            {
                var exceptionString = e.Message;
            }
        }
        public void OnPicture(Stream pictureStream)
        {
            try
            {
                var memoryStream = new MemoryStream();
                pictureStream.CopyTo(memoryStream);
                CustomerPhoto = memoryStream.ToArray();
            }
            catch (Exception e)
            {
                var exceptionString = e.Message;
            }
        }



    }
}