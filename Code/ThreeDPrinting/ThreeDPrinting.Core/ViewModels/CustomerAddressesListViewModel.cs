using System;
using System.Threading.Tasks;
using MvvmCross.Binding.Combiners;
using MvvmCross.Binding.Views;
using MvvmCross.Core.ViewModels;
using MvvmCross.Core.Navigation;
using MvvmCross.Platform.Platform;
using ThreeDPrinting.core;
using ThreeDPrinting.Core.Services;
using System.IO;
using MvvmCross.Plugins.PictureChooser;
using ThreeDPrinting.Core.Model;

namespace ThreeDPrinting.Core.ViewModels
{
    public class CustomerAddressesListViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService _navigationService;

        private readonly ICustomerService _customerService;
        public MvxObservableCollection<CustomerAddressModel> Addresses { get; }
        public IMvxAsyncCommand DeleteCommand { get; }

        public CustomerAddressesListViewModel(IMvxNavigationService navigationService, ICustomerService customerService)
        {
            _navigationService = navigationService;
            _customerService = customerService;
            Addresses = new MvxObservableCollection<CustomerAddressModel>();
            DeleteCommand = new MvxAsyncCommand(DeleteAddress);
        }

        public override Task Initialize()
        {
            return Task.WhenAll(base.Initialize(), LoadData());
        }

        private async Task LoadData()
        {
            var addresses = await _customerService.GetAllAddressesAsync();
            Addresses.Clear();
            Addresses.AddRange(addresses);
        }

        private async Task DeleteAddress()
        {
           
            foreach (CustomerAddressModel address in Addresses)
            {
                if(address.IsSelected)
                 await  _customerService.DeleteAddressAsync(address.AddressId);
            }

            await LoadData();
        }


    }
}