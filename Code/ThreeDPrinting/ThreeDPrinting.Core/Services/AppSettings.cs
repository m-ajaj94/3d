﻿namespace ThreeDPrinting.Core.Services
{
    public class AppSettings
    {
        public AppSettings(string apiServiceBaseUri)
        {
            ApiServiceBaseUri = apiServiceBaseUri;
        }
        public string ApiServiceBaseUri { get; set; }
    }
}