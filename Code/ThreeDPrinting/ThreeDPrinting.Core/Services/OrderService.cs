﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using MvvmCross.Plugins.Network.Rest;
using ThreeDPrinting;
using ThreeDPrinting.core;


namespace ThreeDPrinting.Core.Services
{

 

    public class OrderService : IOrderService
    {


        private readonly IMvxJsonRestClient _mvxJsonRestClient;
        private readonly AppSettings _appSettings;
        private readonly string _baseUri;
     

        public OrderService(IMvxJsonRestClient mvxJsonRestClient, AppSettings appSettings)
        {
            _mvxJsonRestClient = mvxJsonRestClient;
            _appSettings = appSettings;
             _baseUri = _appSettings.ApiServiceBaseUri + "/api";
        }



        public async Task<List<ColorModel>> GetAllColorsAsync()
        {
            try
            {
                var request = new MvxRestRequest($"{_baseUri}/Color/GetAllColors/");
                var response = await _mvxJsonRestClient.MakeRequestForAsync<List<ColorModel>>(request);
                return response.Result;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<List<MaterialModel>> GetAllMaterialsAsync()
        {
            try
            {
                var request = new MvxRestRequest($"{_baseUri}/Material/GetAllMaterials/");
                var response = await _mvxJsonRestClient.MakeRequestForAsync<List<MaterialModel>>(request);
                return response.Result;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<List<POSModel>> GetPOSsInRange(int miles)
        {
            try
            {
                var request = new MvxRestRequest($"{_baseUri}/POS/GetPOSsInRange/{miles}");
                var response = await _mvxJsonRestClient.MakeRequestForAsync<List<POSModel>>(request);
                return response.Result;
            }
            catch (Exception e)
            {
                throw;
            }
        }

    }

    public interface IOrderService
    {

        Task<List<ColorModel>> GetAllColorsAsync();
        Task<List<MaterialModel>> GetAllMaterialsAsync();
        Task<List<POSModel>> GetPOSsInRange(int miles);

    }
}