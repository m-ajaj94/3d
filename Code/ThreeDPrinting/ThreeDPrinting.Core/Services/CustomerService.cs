﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using MvvmCross.Plugins.Network.Rest;
using ThreeDPrinting;
using ThreeDPrinting.core;
using ThreeDPrinting.Core.Model;

namespace ThreeDPrinting.Core.Services
{



    public class CustomerService : ICustomerService
    {


        private readonly IMvxJsonRestClient _mvxJsonRestClient;
        private readonly AppSettings _appSettings;
        private readonly string _baseUri;




        public CustomerService(IMvxJsonRestClient mvxJsonRestClient, AppSettings appSettings)
        {
            _mvxJsonRestClient = mvxJsonRestClient;
            _appSettings = appSettings;
            _baseUri = _appSettings.ApiServiceBaseUri + "/api/Customer";
        }


        public async Task<CustomerModel> GetCustomerAsync(int customerId)
        {
            var request = new MvxRestRequest($"{_baseUri}/Customer/{customerId}");
            var response = await _mvxJsonRestClient.MakeRequestForAsync<CustomerModel>(request);
            return response.Result;
        }



        public async Task UpdateCustomerAsync(ModifyCustomerModel model)
        {
            var request = new MvxJsonRestRequest<ModifyCustomerModel>($"{_baseUri}/Customer/ModifyCustomer/", "PUT") { Body = model };
            var res = await _mvxJsonRestClient.MakeRequestForAsync<object>(request);
            if (res.StatusCode != HttpStatusCode.NoContent)
            {

            }
        }

        public async Task RegisterCustomerAsync(AddCustomerModel model)
        {
            try
            {
                var request = new MvxJsonRestRequest<AddCustomerModel>($"{_baseUri}/RegisterCustomer/", "PUT") { Body = model };
                var res = await _mvxJsonRestClient.MakeRequestForAsync<object>(request);
                if (res.StatusCode != HttpStatusCode.NoContent)
                {

                }
            }
            catch (Exception e)
            {
                var msg = e.Message;
            }

        }

        public async Task<List<CustomerAddressModel>> GetAllAddressesAsync()
        {

            var request = new MvxRestRequest($"{_baseUri}/GetCustomerAddresses/");
            var response = await _mvxJsonRestClient.MakeRequestForAsync<List<CustomerAddressModel>>(request);
            return response.Result;

        }

        public async Task<List<PaymentMethod>> GetAllPaymentMethodsAsync()
        {
            try
            {
                var request = new MvxRestRequest($"{_baseUri}/payment/");
                var response = await _mvxJsonRestClient.MakeRequestForAsync<List<PaymentMethod>>(request);
                return response.Result;
            }
            catch (Exception e)
            {
                throw;
            }

        }

        public async Task AddPaymentMethodAsync(AddPaymentMethodModel model)
        {
            var request = new MvxJsonRestRequest<AddPaymentMethodModel>($"{_baseUri}", "PUT") { Body = model };
            var res = await _mvxJsonRestClient.MakeRequestForAsync<object>(request);
            if (res.StatusCode != HttpStatusCode.NoContent)
            {

            }
        }

        public async Task EditPaymentMethodAsync(EditPaymentMethodModel model)
        {
            var request = new MvxJsonRestRequest<EditPaymentMethodModel>($"{_baseUri}", "PUT") { Body = model };
            var res = await _mvxJsonRestClient.MakeRequestForAsync<object>(request);
            if (res.StatusCode != HttpStatusCode.NoContent)
            {

            }
        }

        public async Task<PaymentMethod> GetPaymentMethodAsync(Guid cardId)
        {
            var request = new MvxRestRequest($"{_baseUri}/GetPaymentMethod/{cardId}");
            var response = await _mvxJsonRestClient.MakeRequestForAsync<PaymentMethod>(request);
            return response.Result;
        }
        public async Task DeleteAddressAsync(int addressId)
        {

            var request = new MvxRestRequest($"{_baseUri}/DeleteAddress/{addressId}", "DELETE");
            var res = await _mvxJsonRestClient.MakeRequestForAsync<object>(request);
            if (res.StatusCode != HttpStatusCode.OK)
            {

            }
        }

        public async Task LoginAsync(string userName, string password)
        {

            var request = new MvxRestRequest($"{_baseUri}/User/Login/{userName}/{password}");
            var res = await _mvxJsonRestClient.MakeRequestForAsync<object>(request);
            if (res.StatusCode != HttpStatusCode.NoContent)
            {

            }

        }
        public async Task DeletePaymentMethodAsync(DeletePaymentMethod deletePaymentMethod)
        {
            var request = new MvxJsonRestRequest<DeletePaymentMethod>($"{_baseUri}", "DELETE") { Body = deletePaymentMethod };
            var res = await _mvxJsonRestClient.MakeRequestForAsync<object>(request);
            if (res.StatusCode != HttpStatusCode.NoContent)
            {

            }
        }

    }

    public interface ICustomerService
    {

        Task<CustomerModel> GetCustomerAsync(int customerId);
        Task UpdateCustomerAsync(ModifyCustomerModel model);

        Task RegisterCustomerAsync(AddCustomerModel model);
        Task<List<CustomerAddressModel>> GetAllAddressesAsync();
        Task LoginAsync(string userName, string password);
        Task<List<PaymentMethod>> GetAllPaymentMethodsAsync();
        Task AddPaymentMethodAsync(AddPaymentMethodModel model);
        Task EditPaymentMethodAsync(EditPaymentMethodModel model);
        Task<PaymentMethod> GetPaymentMethodAsync(Guid cardId);
        Task DeletePaymentMethodAsync(DeletePaymentMethod deletePaymentMethod);
        Task DeleteAddressAsync(int addressId);
    }
}