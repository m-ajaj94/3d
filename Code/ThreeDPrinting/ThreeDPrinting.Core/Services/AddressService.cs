﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using MvvmCross.Plugins.Network.Rest;
using ThreeDPrinting;
using ThreeDPrinting.core;
using ThreeDPrinting.Core.Model;

namespace ThreeDPrinting.Core.Services
{

 

    public class AddressService : IAddressService
    {


        private readonly IMvxJsonRestClient _mvxJsonRestClient;
        private readonly AppSettings _appSettings;
        private readonly string _baseUri;



     

        public AddressService(IMvxJsonRestClient mvxJsonRestClient, AppSettings appSettings)
        {
            _mvxJsonRestClient = mvxJsonRestClient;
            _appSettings = appSettings;
             _baseUri = _appSettings.ApiServiceBaseUri + "/api/address/";
        }




        public async Task<List<CustomerAddressModel>> GetAllIAddressesAsync()
        {
            try
            {
                var request = new MvxRestRequest(_baseUri);
                var response = await _mvxJsonRestClient.MakeRequestForAsync<List<CustomerAddressModel>>(request);
                return response.Result;
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }

    public interface IAddressService
    {
        Task<List<CustomerAddressModel>> GetAllIAddressesAsync();
   
    }
}