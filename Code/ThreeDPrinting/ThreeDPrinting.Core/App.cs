using MvvmCross.Platform;
using MvvmCross.Platform.IoC;
using ThreeDPrinting.Core.Services;

namespace ThreeDPrinting.Core
{
    public class App : MvvmCross.Core.ViewModels.MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();
            Mvx.RegisterSingleton<AppSettings>(new AppSettings("http://192.168.1.10:7991"));

            RegisterAppStart<ViewModels.AddEditPaymentMethodViewModel>();
        }
    }
}
