// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace ThreeDPrinting.iOS.Views
{
    [Register ("AddNewAddressView")]
    partial class AddNewAddressView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField apt { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField choose_country { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField choose_state { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField city { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField code { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField Contact_Name { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch isDefault_payment { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch isDefault_shipping { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField Mobile { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextView security { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField Street_addresses { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField zip_code { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (apt != null) {
                apt.Dispose ();
                apt = null;
            }

            if (choose_country != null) {
                choose_country.Dispose ();
                choose_country = null;
            }

            if (choose_state != null) {
                choose_state.Dispose ();
                choose_state = null;
            }

            if (city != null) {
                city.Dispose ();
                city = null;
            }

            if (code != null) {
                code.Dispose ();
                code = null;
            }

            if (Contact_Name != null) {
                Contact_Name.Dispose ();
                Contact_Name = null;
            }

            if (isDefault_payment != null) {
                isDefault_payment.Dispose ();
                isDefault_payment = null;
            }

            if (isDefault_shipping != null) {
                isDefault_shipping.Dispose ();
                isDefault_shipping = null;
            }

            if (Mobile != null) {
                Mobile.Dispose ();
                Mobile = null;
            }

            if (security != null) {
                security.Dispose ();
                security = null;
            }

            if (Street_addresses != null) {
                Street_addresses.Dispose ();
                Street_addresses = null;
            }

            if (zip_code != null) {
                zip_code.Dispose ();
                zip_code = null;
            }
        }
    }
}