using System;
using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using MvvmCross.iOS.Views.Presenters.Attributes;
using UIKit;
using Foundation;  

namespace ThreeDPrinting.iOS.Views
{
    [MvxFromStoryboard("Main")]
    public partial class RegisterCustomerView : MvxViewController
    {

        public RegisterCustomerView() : base("RegisterCustomerView", null)
        {
        }

        public RegisterCustomerView(IntPtr handle) : base (handle)
        {
        }
        UIImagePickerController picker; 
       
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            profile_image.Layer.BorderWidth = 2;
            profile_image.Layer.MasksToBounds = false;

            profile_image.Layer.BorderColor = UIColor.Gray.CGColor;
            profile_image.Layer.CornerRadius = profile_image.Frame.Size.Width / 2;
            profile_image.ClipsToBounds = true;


            var set = this.CreateBindingSet<RegisterCustomerView, Core.ViewModels.RegisterCustomerViewModel>();
            set.Bind(user_name).To(vm => vm.UserName);
            set.Bind(first_name).To(vm => vm.FirstName);
            set.Bind(last_name).To(vm => vm.LastName);
            set.Bind(email).To(vm => vm.Email);
            set.Bind(password).To(vm => vm.Password);
            set.Bind(add_btn).To(vm => vm.SaveCommand);
            set.Apply();

            //add_btn.TouchUpInside += delegate {

            //    //add_btn.SetTitle ("aad", UIControlState.Normal);
            //};

            chooseImage.TouchUpInside += delegate {

                picker = new UIImagePickerController();  
            picker.SourceType = UIImagePickerControllerSourceType.PhotoLibrary;  
            picker.MediaTypes = UIImagePickerController.AvailableMediaTypes(UIImagePickerControllerSourceType.PhotoLibrary);  
            picker.FinishedPickingMedia += Finished;  
            picker.Canceled += Canceled;  
            PresentViewController(picker, animated: true, completionHandler: null); 
            };
        }

    
         
        public void Finished(object sender, UIImagePickerMediaPickedEventArgs e) {  
            bool isImage = false;  
            switch (e.Info[UIImagePickerController.MediaType].ToString()) {  
                case "public.image":  
                    isImage = true;  
                    break;  
                case "public.video":  
                    break;  
            }  
            NSUrl referenceURL = e.Info[new NSString("UIImagePickerControllerReferenceUrl")] as NSUrl;  
            if (referenceURL != null) Console.WriteLine("Url:" + referenceURL.ToString());  
            if (isImage) {  
                UIImage originalImage = e.Info[UIImagePickerController.OriginalImage] as UIImage;  
                if (originalImage != null) {  
                    profile_image.Image = originalImage;  
                }  
            } else {  
                NSUrl mediaURL = e.Info[UIImagePickerController.MediaURL] as NSUrl;  
                if (mediaURL != null) {  
                    Console.WriteLine(mediaURL.ToString());  
                }  
            }  
            picker.DismissModalViewController(true);  
        }  
        void Canceled(object sender, EventArgs e) {  
            picker.DismissModalViewController(true);  
        }  
    }
}
