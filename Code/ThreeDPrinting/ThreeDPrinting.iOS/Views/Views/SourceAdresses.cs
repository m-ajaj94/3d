﻿using System;
using System.Collections.Generic;
using Foundation;
using ThreeDPrinting.Core.Model;
using UIKit;

namespace ThreeDPrinting.iOS.Views
{
    internal class SourceAdresses : UITableViewSource
    {
        private List<CustomerAddressModel> addresses;

        public SourceAdresses(List<CustomerAddressModel> addresses)
        {
            this.addresses = addresses;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = (DefaultAddressCell) tableView.DequeueReusableCell("AddressCell", indexPath);

            cell.Layer.BorderColor = UIColor.FromRGB(236, 236, 236).CGColor;
            cell.Layer.BorderWidth = 1.8f;
            cell.Layer.MasksToBounds = true;

            cell.Layer.CornerRadius = 5;
            //cell.Layer.ShadowColor = UIColor.FromRGB(236, 236, 236).CGColor;
            //cell.Layer.ShadowOpacity = 1.0f;
            //cell.Layer.ShadowRadius = 4.0f;
            //cell.Layer.ShadowOffset = new System.Drawing.SizeF(0f, 3f);
            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return addresses.Count;
        }
    }
}