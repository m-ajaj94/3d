﻿using System;

using UIKit;
using Foundation;
using MvvmCross.iOS.Views;
using MvvmCross.Binding.BindingContext;

namespace ThreeDPrinting.iOS.Views
{
    [MvxFromStoryboard("Main")]
    public partial class ModifyCustomerView : MvxViewController
    {
        public ModifyCustomerView() : base("ModifyCustomerView", null)
        {
        }
        public ModifyCustomerView(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            profile_image.Layer.BorderWidth = 2;
            profile_image.Layer.MasksToBounds = false;
            profile_image.Layer.BorderColor = UIColor.Gray.CGColor;
            profile_image.Layer.CornerRadius = profile_image.Frame.Size.Width / 2;
            profile_image.ClipsToBounds = true;

            var set = this.CreateBindingSet<ModifyCustomerView, Core.ViewModels.ModifyCustomerViewModel>();

            set.Bind(first_name).To(vm => vm.FirstName);
            set.Bind(last_name).To(vm => vm.LastName);
            set.Bind(profile_image.Image).To(vm => vm.CustomerPhoto);

            //   set.Bind(password).To(vm => vm.);
            set.Bind(chooseImage).To(vm => vm.ChoosePictureCommand);

            set.Bind(add_btn).To(vm => vm.SaveCommand);
            set.Apply();

        }


        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}

