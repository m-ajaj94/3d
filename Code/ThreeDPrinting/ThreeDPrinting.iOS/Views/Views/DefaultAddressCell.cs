﻿using System;

using Foundation;
using UIKit;

namespace ThreeDPrinting.iOS.Views
{
    public partial class DefaultAddressCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("DefaultAddressCell");
        public static readonly UINib Nib;

        static DefaultAddressCell()
        {
            Nib = UINib.FromName("DefaultAddressCell", NSBundle.MainBundle);
        }

        protected DefaultAddressCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }
    }
}
