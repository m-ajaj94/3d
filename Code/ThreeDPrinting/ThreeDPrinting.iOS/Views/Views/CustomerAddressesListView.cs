﻿using System;
using System.Collections.Generic;
using MvvmCross.iOS.Views;
using UIKit;

namespace ThreeDPrinting.iOS.Views
{
    [MvxFromStoryboard("Main")]
    public partial class CustomerAddressesListView : MvxViewController
    {
        public CustomerAddressesListView() : base("CustomerAddressesListView", null)
        {
        }
        public CustomerAddressesListView(IntPtr handle) : base (handle)

        {
            Title = "Shipping Address "; 
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            this.NavigationController.NavigationBar.BarTintColor = UIColor.FromRGBA(220, 33, 42, 1);
          //  UINavigationBar.Appearance.TintColor = UIColor.White;
           // this.NavigationController.NavigationBar.TintColor = UIColor.White;
           
            this.NavigationController.NavigationBar.TitleTextAttributes = new UIStringAttributes
            {
                ForegroundColor = UIColor.White
            };

            var Addresses = new List<Core.Model.CustomerAddressModel>();
            Addresses.Add(new Core.Model.CustomerAddressModel());
            Addresses.Add(new Core.Model.CustomerAddressModel());
            Addresses.Add(new Core.Model.CustomerAddressModel());
            Addresses.Add(new Core.Model.CustomerAddressModel());

            AddressesTableView.Source = new SourceAdresses(Addresses);
            AddressesTableView.RowHeight = 195f;
            AddressesTableView.EstimatedRowHeight = 195f;
            AddressesTableView.ReloadData();

        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}

