﻿using System;
using CoreAnimation;
using MvvmCross.iOS.Views;
using UIKit;

namespace ThreeDPrinting.iOS.Views
{
    //[MvxFromStoryboard("Main")]
    public partial class AddNewAddressView : MvxViewController
    {
        public AddNewAddressView() : base("AddNewAddressView", null)
        {
        }
        public AddNewAddressView(IntPtr handle) : base (handle)
        {
            Title = "Add New Address "; 
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            this.NavigationController.NavigationBar.BarTintColor = UIColor.FromRGB(220f, 33f, 42f);

         //   this.NavigationController.NavigationBar.TintColor = UIColor.White;
            add_border(Contact_Name);
            add_border(Mobile);
            add_border(choose_country);
            add_border(choose_state);
            add_border(Street_addresses);
            add_border(apt);
            add_border(city);
            add_border(zip_code);
            add_border(code);

            var borderLayer = new CALayer();
            var width = 1.0f;
            borderLayer.BorderColor = UIColor.LightGray.CGColor;
            borderLayer.MasksToBounds = true;
            borderLayer.Frame = new CoreGraphics.CGRect(0f, security.Frame.Height - width, security.Frame.Width, security.Frame.Height);
            borderLayer.BorderWidth = width;

            security.Layer.AddSublayer(borderLayer);
       
            isDefault_shipping.Transform = CoreGraphics.CGAffineTransform.MakeScale(0.65f, 0.65f);
            isDefault_payment.Transform = CoreGraphics.CGAffineTransform.MakeScale(0.65f, 0.65f);

        }

        private void add_border(UITextField  field){
            var borderLayer = new CALayer();
            var width = 1.0f;
            borderLayer.BorderColor = UIColor.LightGray.CGColor;
            borderLayer.MasksToBounds = true;
            borderLayer.Frame = new CoreGraphics.CGRect(0f, field.Frame.Height - width, field.Frame.Width, field.Frame.Height);
            borderLayer.BorderWidth = width;

            field.Layer.AddSublayer(borderLayer);

        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}

