// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace ThreeDPrinting.iOS.Views
{
    [Register ("CustomerAddressesListView")]
    partial class CustomerAddressesListView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton Add_new_address { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView AddressesTableView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (Add_new_address != null) {
                Add_new_address.Dispose ();
                Add_new_address = null;
            }

            if (AddressesTableView != null) {
                AddressesTableView.Dispose ();
                AddressesTableView = null;
            }
        }
    }
}