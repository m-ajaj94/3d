using MvvmCross.Platform.Plugins;

namespace ThreeDPrinting.iOS.Bootstrap
{
    public class PictureChooserPluginBootstrap
        : MvxLoaderPluginBootstrapAction<MvvmCross.Plugins.PictureChooser.PluginLoader, MvvmCross.Plugins.PictureChooser.iOS.Plugin>
    {
    }
}