// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace ThreeDPrinting.iOS
{
    [Register ("ModifyCustomerView")]
    partial class ModifyCustomerView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton add_btn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton chooseImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField first_name { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField last_name { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField password { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView profile_image { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (add_btn != null) {
                add_btn.Dispose ();
                add_btn = null;
            }

            if (chooseImage != null) {
                chooseImage.Dispose ();
                chooseImage = null;
            }

            if (first_name != null) {
                first_name.Dispose ();
                first_name = null;
            }

            if (last_name != null) {
                last_name.Dispose ();
                last_name = null;
            }

            if (password != null) {
                password.Dispose ();
                password = null;
            }

            if (profile_image != null) {
                profile_image.Dispose ();
                profile_image = null;
            }
        }
    }
}