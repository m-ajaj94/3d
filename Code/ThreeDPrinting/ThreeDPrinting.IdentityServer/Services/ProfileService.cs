﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Services;
using ThreeDPrinting.Repository.IdentityManagers;

namespace ThreeDPrinting.IdentityServer.Services
{
    public class ProfileService : IProfileService
    {
        private ISystemUserManager _systemUserManager;

        public ProfileService(ISystemUserManager systemUserManager)
        {
            _systemUserManager = systemUserManager;
        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var user = await _systemUserManager.GetUserAsync(context.Subject);
            var claims = new List<Claim>()
            {
                new Claim(JwtClaimTypes.PreferredUserName, user.UserName)
            };
            foreach (var role in user.UserRoles)
            {
                claims.Add(new Claim(JwtClaimTypes.Role, role.Role.NormalizedName));
            }
            context.IssuedClaims.AddRange(claims);
        }

        public Task IsActiveAsync(IsActiveContext context)
        {
            var user = _systemUserManager.GetUserAsync(context.Subject).Result;
            context.IsActive = user != null;
            return Task.FromResult(0);
        }
    }
}