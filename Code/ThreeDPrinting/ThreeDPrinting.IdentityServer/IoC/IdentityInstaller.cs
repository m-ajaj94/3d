﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Microsoft.AspNetCore.Identity;
using ThreeDPrinting.Model.Identity;
using ThreeDPrinting.Repository.IdentityManagers;
using ThreeDPrinting.Repository.IdentityStores;

namespace ThreeDPrinting.IdentityServer.IoC
{
    public class IdentityInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IUserStore<SystemUser>>().ImplementedBy<SystemUserStore>().LifestyleTransient(),
                Component.For<IRoleStore<SystemRole>>().ImplementedBy<SystemRoleStore>().LifestyleTransient(),
                Component.For<ISystemUserManager>().ImplementedBy<SystemUserManager>().LifestyleTransient(),
                Component.For<ISystemRoleManager>().ImplementedBy<SystemRoleManager>().LifestyleTransient()
                );
        }
    }
}