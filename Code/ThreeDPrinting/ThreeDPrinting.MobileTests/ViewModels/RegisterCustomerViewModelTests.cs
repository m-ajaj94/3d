﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using ThreeDPrinting.Core.ViewModels;
using Moq;
using System.Threading.Tasks;
using FizzWare.NBuilder;
using ThreeDPrinting.core;
using ThreeDPrinting.Core.Services;
using MvvmCross.Core;
using MvvmCross.Core.Navigation;
using MvvmCross.Plugins.PictureChooser;
using System.IO;

//using MvvmCross.Core.Navigation;


namespace ThreeDPrinting.MobileTests
{
    [TestFixture]
    public class RegisterCustomerViewModelTests : TestBaseClass
    {

        protected RegisterCustomerViewModel ViewModel;
        protected Mock<IMvxNavigationService> MockedNavigationService;
        protected Mock<ICustomerService> MockedCustomerService;
        protected Mock<IMvxPictureChooserTask> MockedPictureChooserTask;


        public override void BecauseOf()
        {
            base.BecauseOf();
            MockedNavigationService = new Mock<IMvxNavigationService>();
            MockedCustomerService = new Mock<ICustomerService>();
            MockedPictureChooserTask = new Mock<IMvxPictureChooserTask>();
            ViewModel = new RegisterCustomerViewModel(MockedNavigationService.Object, MockedCustomerService.Object, MockedPictureChooserTask.Object);
        }




       

        public class SaveActionTests : RegisterCustomerViewModelTests
        {
            private AddCustomerModel _actualModel;
            public override void BecauseOf()
            {
                base.BecauseOf();
                ViewModel.FirstName = "First Name";
                ViewModel.LastName = "Last Name";
                ViewModel.Email = "Email";
                ViewModel.Password = "Password";
                ViewModel.UserName = "UserName";
                ViewModel.CustomerPhoto = new byte[] { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };
                ViewModel.Prepare();
                MockedCustomerService.Setup(x => x.RegisterCustomerAsync(It.IsAny<AddCustomerModel>())).Returns(Task.FromResult((object)null)).Callback(
                    (AddCustomerModel model) =>
                    {
                        _actualModel = model;
                    });
                ViewModel.SaveCommand.ExecuteAsync().Wait();
                SetupActuals();
            }



            [Test]
            public void TestSaveActionOnRegisterCustomerViewModel()
            {
             MockedNavigationService.Verify(x => x.Close(ViewModel), "View was not closed.");
               MockedCustomerService.Verify(x => x.RegisterCustomerAsync(It.IsAny<AddCustomerModel>()), "Customer is not registered.");
                Assert.That(_actualModel.FirstName, Is.EqualTo(ViewModel.FirstName), "First name is not correct");
                Assert.That(_actualModel.LastName, Is.EqualTo(ViewModel.LastName), "Last name is not correct");
                Assert.That(_actualModel.Password, Is.EqualTo(ViewModel.Password), "Password is not correct");
                Assert.That(_actualModel.Email, Is.EqualTo(ViewModel.Email), "Email is not correct");
                Assert.That(_actualModel.UserName, Is.EqualTo(ViewModel.UserName), "UserName is not correct");
                Assert.That(_actualModel.Photo, Is.EqualTo(ViewModel.CustomerPhoto), "Photo is not correct");
            }
        }

        public class ChoosePhotoTest : ModifyCustomerViewModelTests
        {
            private CustomerModel _actualModel;
            private byte[] _photo;

            MemoryStream memoryStream = new MemoryStream(0x10000);
           


            public override void BecauseOf()
            {
                base.BecauseOf();
               
                try
                {
                    
                    ViewModel.ChoosePictureCommand.ExecuteAsync().Wait();
                    ViewModel.OnPicture(memoryStream);
                    _photo = ViewModel.CustomerPhoto.ToArray();
                    SetupActuals();
                }
                catch (Exception e)
                {
                    var exceptionString = e.Message;
                }

            }

            protected override void SetupActuals()
            {
                base.SetupActuals();
                _actualModel = Builder<CustomerModel>.CreateNew().Build();
                _actualModel.Photo = new byte[] { };

            }

            [Test]
            public void TestChoosePictureOnRegisterCustomerViewModel()
            {
              
              Assert.That(_actualModel.Photo, Is.EqualTo(ViewModel.CustomerPhoto), "Command doesnt return photo");


            }

        }

    }
}
