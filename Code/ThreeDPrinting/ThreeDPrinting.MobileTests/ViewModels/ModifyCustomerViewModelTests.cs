﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using ThreeDPrinting.Core.ViewModels;
using Moq;
using System.Threading.Tasks;
using FizzWare.NBuilder;
using ThreeDPrinting.core;
using ThreeDPrinting.Core.Services;
using MvvmCross.Core;
using MvvmCross.Core.Navigation;
using MvvmCross.Plugins.PictureChooser;
using System.IO;

//using MvvmCross.Core.Navigation;


namespace ThreeDPrinting.MobileTests
{
    [TestFixture]
    public class ModifyCustomerViewModelTests : TestBaseClass
    {

        protected ModifyCustomerViewModel ViewModel;
        protected Mock<IMvxNavigationService> MockedNavigationService;
        protected Mock<ICustomerService> MockedCustomerService;
        protected Mock<IMvxPictureChooserTask> MockedPictureChooserTask;


        public override void BecauseOf()
        {
            base.BecauseOf();
            MockedNavigationService = new Mock<IMvxNavigationService>();
            MockedCustomerService = new Mock<ICustomerService>();
            MockedPictureChooserTask = new Mock<IMvxPictureChooserTask>();
            ViewModel = new ModifyCustomerViewModel(MockedNavigationService.Object, MockedCustomerService.Object, MockedPictureChooserTask.Object);
        }




        public class InitializeViewModelTests : ModifyCustomerViewModelTests
        {
            private CustomerModel _customer;
            private string _actualFirstName;
            private string _actualLastName;
            private byte[] _photo;

            public override void BecauseOf()
            {
                base.BecauseOf();
                _customer = Builder<CustomerModel>.CreateNew().Build();
                _customer.Photo = new byte[] { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };
                MockedCustomerService.Setup(x => x.GetCustomerAsync(_customer.CustomerId)).ReturnsAsync(_customer);
                ViewModel.Prepare(_customer.CustomerId);
                ViewModel.Initialize().Wait();
                SetupActuals();
            }

            protected override void SetupActuals()
            {
                base.SetupActuals();
                _actualFirstName = ViewModel.FirstName;
                _actualLastName = ViewModel.LastName;
                _photo = ViewModel.CustomerPhoto.ToArray();

            }

           // [Test]
            public void TestInitializeModifyCustomerViewModel()
            {
                MockedCustomerService.Verify(x => x.GetCustomerAsync(_customer.CustomerId), "Customer was not grabbed from Api.");
                Assert.That(_actualFirstName, Is.EquivalentTo(_customer.FirstName), "First name was not set.");
                Assert.That(_actualLastName, Is.EquivalentTo(_customer.LastName), "Last name was not set.");
                Assert.That(_photo, Is.EquivalentTo(_customer.Photo.ToArray()), "Photo was not set.");

            }
        }

        public class SaveActionTests : ModifyCustomerViewModelTests
        {
            private ModifyCustomerModel _actualModel;
            public override void BecauseOf()
            {
                base.BecauseOf();
                ViewModel.FirstName = "First Name";
                ViewModel.LastName = "Last Name";
                ViewModel.CustomerPhoto = new byte[] { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };
                ViewModel.Prepare(1);
                MockedCustomerService.Setup(x => x.UpdateCustomerAsync(It.IsAny<ModifyCustomerModel>())).Returns(Task.FromResult((object)null)).Callback(
                    (ModifyCustomerModel model) =>
                    {
                        _actualModel = model;
                    });
                ViewModel.SaveCommand.ExecuteAsync().Wait();
                SetupActuals();
            }



            [Test]
            public void TestSaveActionOnModifyCustomerViewModel()
            {
              MockedNavigationService.Verify(x => x.Close(ViewModel, true), "View was not closed.");
               MockedCustomerService.Verify(x => x.UpdateCustomerAsync(It.IsAny<ModifyCustomerModel>()), "Customer is not updated.");
                Assert.That(_actualModel.CustomerId, Is.EqualTo(1), "Customer id is not correct");
                Assert.That(_actualModel.FirstName, Is.EqualTo(ViewModel.FirstName), "First name is not correct");
                Assert.That(_actualModel.LastName, Is.EqualTo(ViewModel.LastName), "Last name is not correct");
                Assert.That(_actualModel.Photo, Is.EqualTo(ViewModel.CustomerPhoto), "Photo is not correct");
            }
        }

        public class ChoosePhotoTest : ModifyCustomerViewModelTests
        {
            private CustomerModel _actualModel;
            private byte[] _photo;

            MemoryStream memoryStream = new MemoryStream(0x10000);
           


            public override void BecauseOf()
            {
                base.BecauseOf();
               
                try
                {
                    
                    ViewModel.ChoosePictureCommand.ExecuteAsync().Wait();
                    ViewModel.OnPicture(memoryStream);
                    _photo = ViewModel.CustomerPhoto.ToArray();
                    SetupActuals();
                }
                catch (Exception e)
                {
                    var exceptionString = e.Message;
                }

            }

            protected override void SetupActuals()
            {
                base.SetupActuals();
                _actualModel = Builder<CustomerModel>.CreateNew().Build();
                _actualModel.Photo = new byte[] { };

            }

            [Test]
            public void TestChoosePictureOnModifyCustomerViewModel()
            {
              
              Assert.That(_actualModel.Photo, Is.EqualTo(ViewModel.CustomerPhoto), "Command doesnt return photo");


            }

        }

    }
}
