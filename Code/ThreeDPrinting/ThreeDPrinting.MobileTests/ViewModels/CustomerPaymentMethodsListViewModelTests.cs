﻿using System;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework;
using ThreeDPrinting.Core.ViewModels;
using Moq;
using FizzWare.NBuilder;
using ThreeDPrinting.Core.Services;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using ThreeDPrinting.Core.Model;
using ThreeDPrinting.core;

//using MvvmCross.Core.Navigation;


namespace ThreeDPrinting.MobileTests
{
    [TestFixture]
    public class CustomerPaymentMethodsListViewModelTests : TestBaseClass
    {

        protected CustomerPaymentMethodsListViewModel ViewModel;
        protected Mock<IMvxNavigationService> MockedNavigationService;
        protected Mock<ICustomerService> MockedCustomerService;
    


        public override void BecauseOf()
        {
            base.BecauseOf();
            MockedNavigationService = new Mock<IMvxNavigationService>();
            MockedCustomerService = new Mock<ICustomerService>();
            ViewModel = new CustomerPaymentMethodsListViewModel(MockedNavigationService.Object, MockedCustomerService.Object);
        }




        public class InitializeViewModelTests : CustomerPaymentMethodsListViewModelTests
        {

            private IList<PaymentMethod> _paymentMethods;
            private IList<PaymentMethod> _actualPaymentMethods;

            public override void BecauseOf()
            {
                base.BecauseOf();
                _paymentMethods = Builder<PaymentMethod>.CreateListOfSize(100).Build();
                MockedCustomerService.Setup(x => x.GetAllPaymentMethodsAsync()).ReturnsAsync((List<PaymentMethod>)_paymentMethods);
                ViewModel.Initialize().Wait();
                SetupActuals();
            }

            protected override void SetupActuals()
            {
                base.SetupActuals();
                _actualPaymentMethods = ViewModel.PaymentMethods;
            }

            [Test]
            public void TestInitializeItemsViewModel()
            {
                MockedCustomerService.Verify(x => x.GetAllPaymentMethodsAsync(), "Payment Methods not grabbed.");
                Assert.That(_actualPaymentMethods, Is.EquivalentTo(_paymentMethods), "Payment Methods  not loaded into list.");
            }
        }


        public class NavigationToEditPaymentMethodTests : CustomerPaymentMethodsListViewModelTests
        {
            protected PaymentMethod PaymentMethod;
            public override void BecauseOf()
            {
                base.BecauseOf();
                PaymentMethod = Builder<PaymentMethod>.CreateNew().Build();
                MockedNavigationService.Setup(x => x.Navigate<AddEditPaymentMethodViewModel, Guid?, bool>(PaymentMethod.CardId, null, default(CancellationToken))).ReturnsAsync(true);
                SetupActuals();
            }

            protected override void SetupActuals()
            {
                base.SetupActuals();
                ViewModel.EditPaymentMethodCommand.ExecuteAsync(PaymentMethod);
            }

            [Test]
            public void TestNavigationtoEditPaymentMethod()
             {
                MockedNavigationService.Verify(x => x.Navigate<AddEditPaymentMethodViewModel,Guid?, bool>(PaymentMethod.CardId, null, default(CancellationToken)), Times.Once, "Didn't navigate to Edit Payment ViewModel.");
                // MockedItemService.Verify(x => x.GetAllItemsAsync(), Times.Once, "Items not refreshed after save.");
                 
            }
            
        }

        public class NavigationToAddPaymentMethodTests : CustomerPaymentMethodsListViewModelTests
        {
            protected PaymentMethod PaymentMethod;
            public override void BecauseOf()
            {
                base.BecauseOf();
                PaymentMethod = Builder<PaymentMethod>.CreateNew().Build();
               //MockedNavigationService.Setup(x => x.Navigate<AddEditPaymentMethodViewModel>(null));
               MockedNavigationService.Setup(x => x.Navigate<AddEditPaymentMethodViewModel, bool>((IMvxBundle)null, It.IsAny<CancellationToken>()));

                SetupActuals();
            }

            protected override void SetupActuals()
            {
                base.SetupActuals();
                ViewModel.AddPaymentMethodCommand.ExecuteAsync();
            }

            [Test]
            public void TestNavigationtoAddPaymentMethod()
            {
                MockedNavigationService.Verify(x => x.Navigate<AddEditPaymentMethodViewModel, bool>((IMvxBundle)null, It.IsAny<CancellationToken>()), Times.Once, "Didn't navigate to Add Payment ViewModel.");
               
            }

        }
    }
}
