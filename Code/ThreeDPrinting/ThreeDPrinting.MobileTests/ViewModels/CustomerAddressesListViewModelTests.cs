﻿using System.Collections.Generic;
using NUnit.Framework;
using ThreeDPrinting.Core.ViewModels;
using Moq;
using FizzWare.NBuilder;
using ThreeDPrinting.Core.Services;
using MvvmCross.Core.Navigation;
using ThreeDPrinting.Core.Model;

//using MvvmCross.Core.Navigation;


namespace ThreeDPrinting.MobileTests
{
    [TestFixture]
    public class CustomerAddressesListViewModelTests : TestBaseClass
    {

        protected CustomerAddressesListViewModel ViewModel;
        protected Mock<IMvxNavigationService> MockedNavigationService;
        protected Mock<ICustomerService> MockedCustomerService;
        protected IList<CustomerAddressModel> _addresses;


        public override void BecauseOf()
        {
            base.BecauseOf();
            MockedNavigationService = new Mock<IMvxNavigationService>();
            MockedCustomerService = new Mock<ICustomerService>();
            ViewModel = new CustomerAddressesListViewModel(MockedNavigationService.Object, MockedCustomerService.Object);
            _addresses = Builder<CustomerAddressModel>.CreateListOfSize(10).Build();
        }




        public class InitializeViewModelTests : CustomerAddressesListViewModelTests
        {

         
            private IList<CustomerAddressModel> _actualAddresses;

            public override void BecauseOf()
            {
                base.BecauseOf();
               
                MockedCustomerService.Setup(x => x.GetAllAddressesAsync()).ReturnsAsync((List<CustomerAddressModel>)_addresses);
                ViewModel.Initialize().Wait();
                SetupActuals();
            }

            protected override void SetupActuals()
            {
                base.SetupActuals();
                _actualAddresses = ViewModel.Addresses;
            }

            [Test]
            public void TestInitializeItemsViewModel()
            {
                MockedCustomerService.Verify(x => x.GetAllAddressesAsync(), "Addresses not grabbed.");
                Assert.That(_actualAddresses, Is.EquivalentTo(_addresses), "Addresses not loaded into list.");
            }
        }


        public class DeleteAddressesTests : CustomerAddressesListViewModelTests
        {
            private CustomerAddressModel _address;
            public override void BecauseOf()
            {
                base.BecauseOf();
                _address = Builder<CustomerAddressModel>.CreateNew().Build();
                MockedCustomerService.Setup(x => x.DeleteAddressAsync(_address.AddressId));
                //int index = 0;
                //foreach (CustomerAddressModel address in _addresses)
                //{
               
                //    address.AddressId = index;
                    
                //    if (index % 2 == 1)
                //    {
                //        _addresses.ElementAt(index).IsSelected = true;
                //    }
                //    index++;
                //}

                //foreach (CustomerAddressModel address in _addresses)
                //{
                //    if (address.IsSelected)
                //        MockedCustomerService.Setup(x => x.DeleteAddressAsync(_address.AddressId));
                //}
                SetupActuals();
            }

            protected override void SetupActuals()
            {
                base.SetupActuals();
                ViewModel.DeleteCommand.ExecuteAsync();
            }

            [Test]
            public void TestDeleteAddresses()
            {
                
              // MockedCustomerService.Verify(x => x.DeleteAddressAsync(_address.AddressId), Times.Once(), "Not all selected addresses are deleted.");
                MockedCustomerService.Verify(x => x.GetAllAddressesAsync(), Times.Once, "Addresses not refreshed after delete.");
            }
        }




    }
}
