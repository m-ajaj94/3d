﻿using System;
using System.Threading.Tasks;
using NUnit.Framework;
using ThreeDPrinting.Core.ViewModels;
using Moq;
using FizzWare.NBuilder;
using ThreeDPrinting.core;
using ThreeDPrinting.Core.Services;
using MvvmCross.Core.Navigation;
using MvvmCross.Plugins.PictureChooser;


namespace ThreeDPrinting.MobileTests
{
    [TestFixture]
    public class AddEditPaymentMethodViewModelTests : TestBaseClass
    {

        protected AddEditPaymentMethodViewModel ViewModel;
        protected Mock<IMvxNavigationService> MockedNavigationService;
        protected Mock<ICustomerService> MockedCustomerService;
        protected Mock<IMvxPictureChooserTask> MockedPictureChooserTask;


        public override void BecauseOf()
        {
            base.BecauseOf();
            MockedNavigationService = new Mock<IMvxNavigationService>();
            MockedCustomerService = new Mock<ICustomerService>();
            MockedPictureChooserTask = new Mock<IMvxPictureChooserTask>();
            ViewModel = new AddEditPaymentMethodViewModel(MockedNavigationService.Object, MockedCustomerService.Object);
        }


        public class EditPaymentMethodViewModelTest : AddEditPaymentMethodViewModelTests
        {
            private PaymentMethod _paymentMethod;
            private string _actualCardNumber;
            private string _actualName;
            private DateTime _actualExpiryDate;
            private string _actualCompanyName;
            public override void BecauseOf()
            {
                base.BecauseOf();
                _paymentMethod = Builder<PaymentMethod>.CreateNew().Build();
              
                MockedCustomerService.Setup(x => x.GetPaymentMethodAsync(_paymentMethod.CardId)).ReturnsAsync(_paymentMethod);
                ViewModel.Prepare(_paymentMethod.CardId);
                ViewModel.Initialize().Wait();
                SetupActuals();
            }

            protected override void SetupActuals()
            {
                base.SetupActuals();
                _actualCardNumber = ViewModel.CardNumber;
                _actualName = ViewModel.FirstName;
                _actualExpiryDate = ViewModel.ExpiryDate;
                _actualCompanyName = ViewModel.CardCompany;

            }

            [Test]
            public void TestInitializeEditPaymentViewModel()
            {
                MockedCustomerService.Verify(x => x.GetPaymentMethodAsync(_paymentMethod.CardId), "Payment Method was not grabbed from Api.");
                Assert.That(_actualCardNumber, Is.EquivalentTo(_paymentMethod.CardNumber), "Card Number was not set.");
                Assert.That(_actualName, Is.EquivalentTo(_paymentMethod.FirstName), "Name was not set.");
                Assert.That(_actualExpiryDate, Is.EqualTo(_paymentMethod.ExpiryDate), "Expiry Date was not set.");
                Assert.That(_actualCompanyName, Is.EquivalentTo(_paymentMethod.CardCompany), "Card Company was not set.");
                
            }
        }

        public class SaveActionAfterEditTests : AddEditPaymentMethodViewModelTests
        {
            private EditPaymentMethodModel _actualModel;
            public override void BecauseOf()
            {
                base.BecauseOf();
                ViewModel.CardNumber = "123456789";
                ViewModel.CardCompany = "Visa";
                ViewModel.FirstName = "First Name";
                ViewModel.ExpiryDate = DateTime.Parse("01-01-2018");
                ViewModel.Prepare(Guid.Parse("1"));
                MockedCustomerService.Setup(x => x.EditPaymentMethodAsync(It.IsAny<EditPaymentMethodModel>())).Returns(Task.FromResult((object)null)).Callback(
                    (EditPaymentMethodModel model) =>
                    {
                        _actualModel = model;
                    });
                ViewModel.SaveCommand.ExecuteAsync().Wait();
                SetupActuals();
            }



            [Test]
            public void TestSaveActionOnEditPaymentMethodViewModel()
            {
                MockedNavigationService.Verify(x => x.Close(ViewModel, true), "View was not closed.");
                MockedCustomerService.Verify(x => x.EditPaymentMethodAsync(It.IsAny<EditPaymentMethodModel>()), "Payment Method is not updated.");
                Assert.That(_actualModel.CardId, Is.EqualTo(1), "Card id is not correct");
                Assert.That(_actualModel.CardCompany, Is.EqualTo(ViewModel.CardCompany), "Card Company is not correct");
                Assert.That(_actualModel.FirstName, Is.EqualTo(ViewModel.FirstName), "Name is not correct");
                Assert.That(_actualModel.CardNumber, Is.EqualTo(ViewModel.CardNumber), "Card Number is not correct");
                Assert.That(_actualModel.ExpiryDate, Is.EqualTo(ViewModel.ExpiryDate), "Expiry Date is not correct");
            }
        }



    }
}
