﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using ThreeDPrinting.Core.ViewModels;
using Moq;
using System.Threading.Tasks;
using FizzWare.NBuilder;
using ThreeDPrinting.core;
using ThreeDPrinting.Core.Services;
using MvvmCross.Core;
using MvvmCross.Core.Navigation;
using MvvmCross.Plugins.PictureChooser;
using System.IO;

//using MvvmCross.Core.Navigation;


namespace ThreeDPrinting.MobileTests
{
    [TestFixture]
    public class LoginViewModelTests : TestBaseClass
    {

        protected LoginViewModel ViewModel;
        protected Mock<IMvxNavigationService> MockedNavigationService;
        protected Mock<ICustomerService> MockedCustomerService;
     


        public override void BecauseOf()
        {
            base.BecauseOf();
            MockedNavigationService = new Mock<IMvxNavigationService>();
            MockedCustomerService = new Mock<ICustomerService>();
            
            ViewModel = new LoginViewModel(MockedNavigationService.Object, MockedCustomerService.Object);
        }




     

        public class LoginTests : LoginViewModelTests
        {
            private string _userName;
            private string _password;
            public override void BecauseOf()
            {
                base.BecauseOf();
                
                MockedCustomerService.Setup(x => x.LoginAsync(It.IsAny<string>(),It.IsAny<string>())).Returns(Task.FromResult((object)null));
                ViewModel.LoginCommand.ExecuteAsync().Wait();
                SetupActuals();
            }



            [Test]
            public void TestLoginAction()
            {
              MockedNavigationService.Verify(x => x.Close(ViewModel), "View was not closed.");
               MockedCustomerService.Verify(x => x.LoginAsync(It.IsAny<string>(), It.IsAny<string>()), "Customer cannot login.");

            }
        }

     

    }
}
