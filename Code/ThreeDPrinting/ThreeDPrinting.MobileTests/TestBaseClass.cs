﻿using NUnit.Framework;

namespace ThreeDPrinting.MobileTests
{
    [TestFixture]
    public class TestBaseClass
    {

        [SetUp]
        public virtual void BecauseOf()
        {

        }

        protected virtual void SetupExpected()
        {

        }

        protected virtual void SetupActuals()
        {

        }
    }
}