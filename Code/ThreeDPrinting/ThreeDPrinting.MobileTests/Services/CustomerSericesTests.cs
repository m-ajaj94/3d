﻿using System.Threading;
using Moq;
using MvvmCross.Plugins.Network.Rest;
using ThreeDPrinting.core;
using ThreeDPrinting.Core.Services;
using FizzWare.NBuilder;
using NUnit.Framework;


namespace ThreeDPrinting.MobileTests.Services
{
    public class CustomerSericesTests :TestBaseClass
    {
        protected CustomerService Service;
        protected AppSettings AppSettings;
        protected Mock<IMvxJsonRestClient> MockedClient;

        public override void BecauseOf()
        {
            base.BecauseOf();
            AppSettings = new AppSettings("http://localhost:23213");
            MockedClient = new Mock<IMvxJsonRestClient>();
            Service = new CustomerService(MockedClient.Object, AppSettings);
        }



        public class GetCustomerAsyncTests : CustomerSericesTests
        {
            private MvxRestRequest _actualRequest;
            private string _expectedUrl;
            private string _expectedVerb;
            private CustomerModel _customer;
            private CustomerModel _actualCustomer;
            public override void BecauseOf()
            {
                base.BecauseOf();
                _customer = Builder<CustomerModel>.CreateNew().Build();
                MockedClient
                    .Setup(x => x.MakeRequestForAsync<CustomerModel>(It.IsAny<MvxRestRequest>(),
                        default(CancellationToken))).Callback(
                        (MvxRestRequest req, CancellationToken token) =>
                        {
                            _actualRequest = req;
                        }).ReturnsAsync(new MvxDecodedRestResponse<CustomerModel>()
                    {
                        Result = _customer
                        });
                SetupExpected();
                SetupActuals();
            }

            protected override void SetupExpected()
            {
                base.SetupExpected();
                _expectedUrl = "http://localhost:23213/api/" + _customer.CustomerId;
                _expectedVerb = "GET";
            }

            protected override void SetupActuals()
            {
                base.SetupActuals();
                _actualCustomer = Service.GetCustomerAsync(_customer.CustomerId).Result;
            }
            [Test]
            public void TestGetCustomerAsync()
            {
               // MockedClient.Verify(x => x.MakeRequestForAsync<CustomerModel>(It.IsAny<MvxRestRequest>(), default(CancellationToken)), Times.Once, "Request not called.");
              //  Assert.That(_actualRequest.Uri.ToString(), Is.EqualTo(_expectedUrl), "Wrong request url.");
               // Assert.That(_actualRequest.Verb, Is.EqualTo(_expectedVerb), "Wrong request verb.");
                Assert.That(_actualCustomer, Is.EqualTo(_customer), "Customer not returned.");
            }
        }


    }
}