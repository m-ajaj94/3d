﻿using System;
using System.Threading;
using Moq;
using MvvmCross.Plugins.Network.Rest;
using ThreeDPrinting.core;
using ThreeDPrinting.Core.Services;
using FizzWare.NBuilder;
using NUnit.Framework;
using ThreeDPrinting.Core.Model;
using System.Collections.Generic;
using System.Linq;
using MvvmCross.Core.Navigation;
using ThreeDPrinting.Core.ViewModels;

namespace ThreeDPrinting.MobileTests.Services
{
    public class CustomerSerivcesTests : TestBaseClass
    {
        protected CustomerService Service;
        protected AppSettings AppSettings;
        protected Mock<IMvxJsonRestClient> MockedClient;

        public override void BecauseOf()
        {
            base.BecauseOf();
            AppSettings = new AppSettings("http://localhost:23213");
            MockedClient = new Mock<IMvxJsonRestClient>();
            Service = new CustomerService(MockedClient.Object, AppSettings);
        }



        public class GetCustomerAsyncTests : CustomerSerivcesTests
        {
            private MvxRestRequest _actualRequest;
            private string _expectedUrl;
            private string _expectedVerb;
            private CustomerModel _customer;
            private CustomerModel _actualCustomer;

            public override void BecauseOf()
            {
                base.BecauseOf();
                _customer = Builder<CustomerModel>.CreateNew().Build();
                MockedClient
                    .Setup(x => x.MakeRequestForAsync<CustomerModel>(It.IsAny<MvxRestRequest>(),
                        default(CancellationToken))).Callback(
                        (MvxRestRequest req, CancellationToken token) =>
                        {
                            _actualRequest = req;
                        }).ReturnsAsync(new MvxDecodedRestResponse<CustomerModel>()
                        {
                            Result = _customer
                        });
                SetupExpected();
                SetupActuals();
            }

            protected override void SetupExpected()
            {
                base.SetupExpected();
                _expectedUrl = "http://localhost:23213/api/Customer/" + _customer.CustomerId;
                _expectedVerb = "GET";
            }

            protected override void SetupActuals()
            {
                base.SetupActuals();
                _actualCustomer = Service.GetCustomerAsync(_customer.CustomerId).Result;
            }

            [Test]
            public void TestGetCustomerAsync()
            {
                MockedClient.Verify(
                    x => x.MakeRequestForAsync<CustomerModel>(It.IsAny<MvxRestRequest>(), default(CancellationToken)),
                    Times.Once, "Request not called.");
                Assert.That(_actualRequest.Uri.ToString(), Is.EqualTo(_expectedUrl), "Wrong request url.");
                Assert.That(_actualRequest.Verb, Is.EqualTo(_expectedVerb), "Wrong request verb.");
                Assert.That(_actualCustomer, Is.EqualTo(_customer), "Customer not returned.");
            }
        }

        public class GetAllAddressesTests : CustomerSerivcesTests
        {
            private MvxRestRequest _actualRequest;
            private string _expectedUrl;
            private string _expectedVerb;
            private IList<CustomerAddressModel> _addresses;
            private IList<CustomerAddressModel> _actualAddresses;

            public override void BecauseOf()
            {
                base.BecauseOf();
                _addresses = Builder<CustomerAddressModel>.CreateListOfSize(100).Build();
                MockedClient
                    .Setup(x => x.MakeRequestForAsync<List<CustomerAddressModel>>(It.IsAny<MvxRestRequest>(),
                        default(CancellationToken))).Callback(
                        (MvxRestRequest req, CancellationToken token) =>
                        {
                            _actualRequest = req;
                        }).ReturnsAsync(new MvxDecodedRestResponse<List<CustomerAddressModel>>()
                        {
                            Result = _addresses.ToList()
                        });
                SetupExpected();
                SetupActuals();
            }

            protected override void SetupExpected()
            {
                base.SetupExpected();
                _expectedUrl = "http://localhost:23213/api/Customer/GetCustomerAddresses/";
                _expectedVerb = "GET";
            }

            protected override void SetupActuals()
            {
                base.SetupActuals();
                _actualAddresses = Service.GetAllAddressesAsync().Result;
            }

            [Test]
            public void TestGetAllAddresses()
            {
                MockedClient.Verify(
                    x => x.MakeRequestForAsync<List<CustomerAddressModel>>(It.IsAny<MvxRestRequest>(),
                        default(CancellationToken)), Times.Once, "Request not called.");
                Assert.That(_actualRequest.Uri.ToString(), Is.EqualTo(_expectedUrl), "Wrong request url.");
                Assert.That(_actualRequest.Verb, Is.EqualTo(_expectedVerb), "Wrong request verb.");
                Assert.That(_actualAddresses, Is.EquivalentTo(_addresses), "Addresses not returned.");
            }
        }


        public class GetPaymentMethodsTests : CustomerSerivcesTests
        {
            private MvxRestRequest _actualRequest;
            private string _expectedUrl;
            private string _expectedVerb;
            private IList<PaymentMethod> _paymentMethods;
            private IList<PaymentMethod> _actualPaymentMethods;

            public override void BecauseOf()
            {
                base.BecauseOf();
                _paymentMethods = Builder<PaymentMethod>.CreateListOfSize(100).Build();
                MockedClient
                    .Setup(x => x.MakeRequestForAsync<List<PaymentMethod>>(It.IsAny<MvxRestRequest>(),
                        default(CancellationToken))).Callback(
                        (MvxRestRequest req, CancellationToken token) =>
                        {
                            _actualRequest = req;
                        }).ReturnsAsync(new MvxDecodedRestResponse<List<PaymentMethod>>()
                        {
                            Result = _paymentMethods.ToList()
                        });
                SetupExpected();
                SetupActuals();
            }

            protected override void SetupExpected()
            {
                base.SetupExpected();
                _expectedUrl = "http://localhost:23213/api/Customer/payment/";
                _expectedVerb = "GET";
            }

            protected override void SetupActuals()
            {
                base.SetupActuals();
                _actualPaymentMethods = Service.GetAllPaymentMethodsAsync().Result;
            }

            [Test]
            public void TestGetAllPaymentMethods()
            {
                MockedClient.Verify(
                    x => x.MakeRequestForAsync<List<PaymentMethod>>(It.IsAny<MvxRestRequest>(),
                        default(CancellationToken)), Times.Once, "Request not called.");
                Assert.That(_actualRequest.Uri.ToString(), Is.EqualTo(_expectedUrl), "Wrong request url.");
                Assert.That(_actualRequest.Verb, Is.EqualTo(_expectedVerb), "Wrong request verb.");
                Assert.That(_actualPaymentMethods, Is.EquivalentTo(_paymentMethods), "Addresses not returned.");
            }
        }

        public class UpdateCustomerAsyncTests : CustomerSerivcesTests
        {
            private MvxJsonRestRequest<ModifyCustomerModel> _actualRequest;
            private string _expectedUrl;
            private string _expectedVerb;
            private ModifyCustomerModel _editCustomerModel;

            public override void BecauseOf()
            {
                base.BecauseOf();
                _editCustomerModel = new ModifyCustomerModel(1, "First Name", "Last Name",
                    new byte[] { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 });
                MockedClient
                    .Setup(x => x.MakeRequestForAsync<object>(It.IsAny<MvxJsonRestRequest<ModifyCustomerModel>>(),
                        default(CancellationToken))).Callback(
                        (MvxRestRequest req, CancellationToken token) =>
                        {
                            _actualRequest = (MvxJsonRestRequest<ModifyCustomerModel>)req;
                        }).ReturnsAsync(new MvxDecodedRestResponse<object>()
                        {
                            Result = null
                        });
                Service.UpdateCustomerAsync(_editCustomerModel).Wait();
                SetupExpected();
            }

            protected override void SetupExpected()
            {
                base.SetupExpected();
                _expectedUrl = "http://localhost:23213/api/Customer/ModifyCustomer/";
                _expectedVerb = "PUT";
            }

            [Test]
            public void TestUpdateCustomerAsync()
            {
                MockedClient.Verify(
                    x => x.MakeRequestForAsync<object>(It.IsAny<MvxJsonRestRequest<ModifyCustomerModel>>(),
                        default(CancellationToken)), Times.Once, "Request not called.");
                Assert.That(_actualRequest.Uri.ToString(), Is.EqualTo(_expectedUrl), "Wrong request url.");
                Assert.That(_actualRequest.Verb, Is.EqualTo(_expectedVerb), "Wrong request verb.");
                Assert.That(_actualRequest.Body, Is.EqualTo(_editCustomerModel), "Edit Customer Model not passed.");
            }
        }

        public class RegisterCustomerTests : CustomerSerivcesTests
        {
            private MvxJsonRestRequest<AddCustomerModel> _actualRequest;
            private string _expectedUrl;
            private string _expectedVerb;
            private AddCustomerModel _addCustomerModel;

            public override void BecauseOf()
            {
                base.BecauseOf();
                _addCustomerModel = new AddCustomerModel("First Name", "Last Name", "Email", "Password", "UserName",
                    new byte[] { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 });
                MockedClient
                    .Setup(x => x.MakeRequestForAsync<object>(It.IsAny<MvxJsonRestRequest<AddCustomerModel>>(),
                        default(CancellationToken))).Callback(
                        (MvxRestRequest req, CancellationToken token) =>
                        {
                            _actualRequest = (MvxJsonRestRequest<AddCustomerModel>)req;
                        }).ReturnsAsync(new MvxDecodedRestResponse<object>()
                        {
                            Result = null
                        });
                Service.RegisterCustomerAsync(_addCustomerModel).Wait();
                SetupExpected();
            }
        }

        public class DeleteAddressTests : CustomerSerivcesTests
        {
            private MvxRestRequest _actualRequest;
            private string _expectedUrl;
            private string _expectedVerb;
            private CustomerAddressModel _address;

            public override void BecauseOf()
            {
                base.BecauseOf();

                _address = Builder<CustomerAddressModel>.CreateNew().Build();
                _address.AddressId = 1;
                MockedClient
                     .Setup(x => x.MakeRequestForAsync<object>(It.IsAny<MvxRestRequest>(),
                      default(CancellationToken))).Callback(
                       (MvxRestRequest req, CancellationToken token) =>
                        {
                            _actualRequest = (MvxRestRequest)req;
                        }).ReturnsAsync(new MvxDecodedRestResponse<object>()
                        {
                            Result = null
                        });
                Service.DeleteAddressAsync(_address.AddressId).Wait();
                SetupExpected();
                SetupActuals();
            }
            protected override void SetupExpected()
            {
                base.SetupExpected();
                _expectedUrl = "http://localhost:23213/api/Customer/DeleteAddress/1";
                _expectedVerb = "DELETE";
            }
            [Test]
            public void TestDeleteAddress()
            {
                MockedClient.Verify(x => x.MakeRequestForAsync<object>(It.IsAny<MvxRestRequest>(), default(CancellationToken)), Times.Once, "Request not called.");
                Assert.That(_actualRequest.Uri.ToString(), Is.EqualTo(_expectedUrl), "Wrong request url.");
                Assert.That(_actualRequest.Verb, Is.EqualTo(_expectedVerb), "Wrong request verb.");

            }
        }

        public class GetPaymentMethodAsyncTests : CustomerSerivcesTests
        {
            private MvxRestRequest _actualRequest;
            private string _expectedUrl;
            private string _expectedVerb;
            private PaymentMethod _paymentMethod;
            private PaymentMethod _actualPaymentMethod;
            public override void BecauseOf()
            {
                base.BecauseOf();
                _paymentMethod = Builder<PaymentMethod>.CreateNew().Build();
                MockedClient
                    .Setup(x => x.MakeRequestForAsync<PaymentMethod>(It.IsAny<MvxRestRequest>(),
                        default(CancellationToken))).Callback(
                        (MvxRestRequest req, CancellationToken token) =>
                        {
                            _actualRequest = req;
                        }).ReturnsAsync(new MvxDecodedRestResponse<PaymentMethod>()
                        {
                            Result = _paymentMethod
                        });
                SetupExpected();
                SetupActuals();
            }

            protected override void SetupExpected()
            {
                base.SetupExpected();
                _expectedUrl = "http://localhost:23213/api/Customer/GetPaymentMethod/" + _paymentMethod.CardId;
                _expectedVerb = "GET";
            }

            protected override void SetupActuals()
            {
                base.SetupActuals();
                _actualPaymentMethod = Service.GetPaymentMethodAsync(_paymentMethod.CardId).Result;
            }
            [Test]
            public void TestGetPaymentMethodAsync()
            {
                MockedClient.Verify(x => x.MakeRequestForAsync<PaymentMethod>(It.IsAny<MvxRestRequest>(), default(CancellationToken)), Times.Once, "Request not called.");
                Assert.That(_actualRequest.Uri.ToString(), Is.EqualTo(_expectedUrl), "Wrong request url.");
                Assert.That(_actualRequest.Verb, Is.EqualTo(_expectedVerb), "Wrong request verb.");
                Assert.That(_actualPaymentMethod, Is.EqualTo(_paymentMethod), "Payment Method not returned.");
            }
        }

        public class EditPaymentMethodAsyncTests : CustomerSerivcesTests
        {
            private MvxJsonRestRequest<EditPaymentMethodModel> _actualRequest;
            private string _expectedUrl;
            private string _expectedVerb;
            private EditPaymentMethodModel _editPaymentMethodModel;
            public override void BecauseOf()
            {
                base.BecauseOf();

                _editPaymentMethodModel = new EditPaymentMethodModel(Guid.Parse("1"), Guid.Parse("1"), "123456789", "first Name",
                    DateTime.Parse("01-01-2018"), "visa", "Last name");
                MockedClient
                    .Setup(x => x.MakeRequestForAsync<object>(It.IsAny<MvxJsonRestRequest<EditPaymentMethodModel>>(),
                        default(CancellationToken))).Callback(
                        (MvxRestRequest req, CancellationToken token) =>
                        {
                            _actualRequest = (MvxJsonRestRequest<EditPaymentMethodModel>)req;
                        }).ReturnsAsync(new MvxDecodedRestResponse<object>()
                        {
                            Result = null
                        });
                Service.EditPaymentMethodAsync(_editPaymentMethodModel).Wait();
                SetupExpected();
            }

            protected override void SetupExpected()
            {
                base.SetupExpected();
                _expectedUrl = "http://localhost:23213/api/Customer";
                _expectedVerb = "PUT";
            }



            [Test]
            public void TestUpdateCustomerAsync()
            {
                MockedClient.Verify(x => x.MakeRequestForAsync<object>(It.IsAny<MvxJsonRestRequest<EditPaymentMethodModel>>(), default(CancellationToken)), Times.Once, "Request not called.");
                Assert.That(_actualRequest.Uri.ToString(), Is.EqualTo(_expectedUrl), "Wrong request url.");
                Assert.That(_actualRequest.Verb, Is.EqualTo(_expectedVerb), "Wrong request verb.");
                Assert.That(_actualRequest.Body, Is.EqualTo(_editPaymentMethodModel), "Edit payment Method Model not passed.");
            }
        }

        public class AddPaymentMethodTests : CustomerSerivcesTests
        {
            private MvxJsonRestRequest<AddPaymentMethodModel> _actualRequest;
            private string _expectedUrl;
            private string _expectedVerb;
            private AddPaymentMethodModel _addPaymentMethodModel;
            public override void BecauseOf()
            {
                base.BecauseOf();
                _addPaymentMethodModel = new AddPaymentMethodModel(Guid.Parse("1"), "123456789", "First Name",
                    DateTime.Parse("01-01-2018"), "Visa", "123456789", "Last Name");
                MockedClient
                    .Setup(x => x.MakeRequestForAsync<object>(It.IsAny<MvxJsonRestRequest<AddPaymentMethodModel>>(),
                        default(CancellationToken))).Callback(
                        (MvxRestRequest req, CancellationToken token) =>
                        {
                            _actualRequest = (MvxJsonRestRequest<AddPaymentMethodModel>)req;
                        }).ReturnsAsync(new MvxDecodedRestResponse<object>()
                        {
                            Result = null
                        });
                Service.AddPaymentMethodAsync(_addPaymentMethodModel).Wait();
                SetupExpected();
            }

            protected override void SetupExpected()
            {
                base.SetupExpected();
                _expectedUrl = "http://localhost:23213/api/Customer";
                _expectedVerb = "PUT";
            }
            protected override void SetupActuals()
            {
                base.SetupActuals();

            }
            [Test]
            public void TestRegisterCustomerAsync()
            {
                MockedClient.Verify(x => x.MakeRequestForAsync<object>(It.IsAny<MvxJsonRestRequest<AddPaymentMethodModel>>(), default(CancellationToken)), Times.Once, "Request not called.");
                Assert.That(_actualRequest.Uri.ToString(), Is.EqualTo(_expectedUrl), "Wrong request url.");
                Assert.That(_actualRequest.Verb, Is.EqualTo(_expectedVerb), "Wrong request verb.");
                Assert.That(_actualRequest.Body, Is.EqualTo(_addPaymentMethodModel), "Add Payment Model not passed.");
            }
        }


        public class LoginAsyncTests : CustomerSerivcesTests
        {
            private MvxRestRequest _actualRequest;
            private string _expectedUrl;
            private string _expectedVerb;
            private string _userName;
            private string _password;

            public override void BecauseOf()
            {
                base.BecauseOf();
              
            MockedClient
                    .Setup(x => x.MakeRequestForAsync<object>(It.IsAny<MvxRestRequest>(),
                        default(CancellationToken))).Callback(
                        (MvxRestRequest req, CancellationToken token) =>
                        {
                            _actualRequest = (MvxRestRequest)req;
                        }).ReturnsAsync(new MvxDecodedRestResponse<object>()
                        {
                            Result = null
                        });
                _userName = "userName";
                _password = "Password";
                Service.LoginAsync(_userName, _password).Wait();
                SetupExpected();
            }

            protected override void SetupExpected()
            {
                base.SetupExpected();
                _expectedUrl = "http://localhost:23213/api/User/Login/userName/Password";
                _expectedVerb = "GET";
            }

            [Test]
            public void TestLoginAsync()
            {
                MockedClient.Verify(x => x.MakeRequestForAsync<object>(It.IsAny<MvxRestRequest>(), default(CancellationToken)), Times.Once, "Request not called.");
                Assert.That(_actualRequest.Uri.ToString(), Is.EqualTo(_expectedUrl), "Wrong request url.");
                Assert.That(_actualRequest.Verb, Is.EqualTo(_expectedVerb), "Wrong request verb.");
               
            }
        }

    }
}
