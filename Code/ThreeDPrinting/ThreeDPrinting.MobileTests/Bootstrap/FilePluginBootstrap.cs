using MvvmCross.Platform.Plugins;

namespace ThreeDPrinting.MobileTests.Bootstrap
{
    public class FilePluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.File.PluginLoader>
    {
    }
}