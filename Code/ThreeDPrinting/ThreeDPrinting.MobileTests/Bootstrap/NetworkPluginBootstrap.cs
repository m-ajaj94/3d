using MvvmCross.Platform.Plugins;

namespace ThreeDPrinting.MobileTests.Bootstrap
{
    public class NetworkPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Network.PluginLoader>
    {
    }
}