using MvvmCross.Platform.Plugins;

namespace ThreeDPrinting.MobileTests.Bootstrap
{
    public class PictureChooserPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.PictureChooser.PluginLoader>
    {
    }
}