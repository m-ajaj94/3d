﻿using System;

namespace ThreeDPrinting.Model.DTO
{
    public class ColorDTO
    {
        public Guid ColorId { get; set; }
        public string ColorName { get; set; }
        public int R { get; set; }
        public int G { get; set; }
        public int B { get; set; }
    }
}