﻿using System;

namespace ThreeDPrinting.Model.DTO
{
    public class CustomerDTO
    {
        public Guid CutomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public byte[] Photo { get; set; }
    }
}