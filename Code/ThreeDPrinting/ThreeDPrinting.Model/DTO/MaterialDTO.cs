﻿using System;
using System.Collections.Generic;
using ThreeDPrinting.Model.Enums;

namespace ThreeDPrinting.Model.DTO
{
    public class MaterialDTO
    {
        public Guid MaterialId { get; set; }
        public string MaterialName { get;  set; }
        public CurrencyType Currency { get;  set; }
        public int PricePerGram { get;  set; }
        public List<ColorDTO> Colors { get; set; }
    }
}