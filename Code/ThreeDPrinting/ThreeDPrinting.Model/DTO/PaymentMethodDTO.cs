﻿using System;

namespace ThreeDPrinting.Model.DTO
{
    public class PaymentMethodDTO
    {
        public Guid CustomerId { get; set; }
        public Guid PaymentMethodId { get; set; }
        public string CardNumber { get; set; }
        public string Name { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string CardCompany { get; set; }
    }
}