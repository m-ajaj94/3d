﻿using System;
using System.Collections.Generic;
using GardarFramework.Core.Domain;
using ThreeDPrinting.Model.Entities;
using ThreeDPrinting.Model.Identity;

namespace ThreeDPrinting.Model.AggregateRoot
{
    public class POS:AggregateRoot<Guid>
    {
        public string Name { get; set; }
        public SystemUser POSUser { get; set; }
        public virtual IList<POSService> POSServices { get; set; }
        public virtual IList<POSMaterial> POSMaterials { get; set; }
        public virtual IList<POSCNCMaterial> POSCNCMaterials { get; set; }
        public bool HasPickUpService { get; set; }
        public int? WorkingHoursPrice { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public bool HasCNCService { get; set; }
        public double PercentageFromPrice { get; set; }
        public byte [] Image1 { get; set; }
        public byte [] Image2 { get; set; }
        public byte [] Image3 { get; set; }
        public byte [] Image4 { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public TimeSpan StartingHour { get; set; }
        public TimeSpan ClosingHour { get; set; }
    }
}