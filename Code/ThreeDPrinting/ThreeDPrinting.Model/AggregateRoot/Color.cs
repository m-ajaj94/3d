﻿using GardarFramework.Core.Domain;
using System;
using System.Collections.Generic;
using ThreeDPrinting.Model.Entities;

namespace ThreeDPrinting.Model.AggregateRoot
{
    public class Color:AggregateRoot<Guid>
    {
        public string ColorName { get; private set; }
        public int R { get; private set; }
        public int G { get; private set; }
        public int B { get; private set; }
        public virtual IList<MaterialColor> MaterialColors { get; set; }
        public virtual IList<CustomerOrderColor> OrderColors { get; set; }
        public Color(string colorName, int r, int g, int b)
        {
            Id=Guid.NewGuid();
            ColorName = colorName;
            R = r;
            G = g;
            B = b;
        }

        public Color()
        {
            
        }

        public void ModifyColor(string colorName, int r, int g, int b)
        {
            ColorName = colorName;
            R = r;
            G = g;
            B = b;
        }
    }
}