﻿using System;
using System.Collections.Generic;
using GardarFramework.Core.Domain;
using ThreeDPrinting.Model.Entities;
using ThreeDPrinting.Model.Identity;

namespace ThreeDPrinting.Model.AggregateRoot
{
    public class Customer:AggregateRoot<Guid>
    {
        public SystemUser User { get; set; }
        public Guid UserId { get; set; }
        public virtual IList<Address> Addresses { get; set; }
        public virtual IList<PaymentMethod> PaymentMethods { get; set; }
        public virtual IList<CustomerOrder> Orders { get; set; }
        
    }
}