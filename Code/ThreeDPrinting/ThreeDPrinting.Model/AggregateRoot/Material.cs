﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using GardarFramework.Core.Domain;
using ThreeDPrinting.Model.Entities;
using ThreeDPrinting.Model.Enums;

namespace ThreeDPrinting.Model.AggregateRoot
{
    public class Material:AggregateRoot<Guid>
    {
        public string MaterialName { get; private set; }
        public CurrencyType Currency { get; private set; }
        public int PricePerGram { get; private set; }
        public virtual IList<MaterialColor> MaterialColors { get; set; }
        public virtual IList<POSMaterial> POSMaterials { get; set; }

        public Material()
        {
            
        }

        public Material(string materialName, CurrencyType currency, int pricePerGram)
        {
            Id=Guid.NewGuid();
            MaterialName = materialName;
            Currency = currency;
            PricePerGram = pricePerGram;
        }

        public void ModifyMaterial(string materialName, CurrencyType currency, int pricePerGram)
        {
            MaterialName = materialName;
            Currency = currency;
            PricePerGram = pricePerGram;
        }
    }
}