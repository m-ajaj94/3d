﻿using System;
using System.Collections.Generic;
using GardarFramework.Core.Domain;
using ThreeDPrinting.Model.Entities;

namespace ThreeDPrinting.Model.AggregateRoot
{
    public class CustomerOrder : AggregateRoot<Guid>
    {
        public Guid CustomerId { get; set; }
        public Guid MaterialId { get; set; }
        public virtual IList<CustomerOrderColor> OrderColors { get; set; }
        public Customer Customer { get; set; }
        public Material Material { get; set; }
        public string OrderNote { get; set; }
        public Service Service { get; set; }
        public Guid ServiceId { get; set; }
        public IList<AttachedFile> AttachedFiles { get; set; }


        public CustomerOrder()
        {

        }

        public CustomerOrder(Customer customer, Service service, Material material, string orderNote)
        {
            Id = Guid.NewGuid();
            Customer = customer;
            Service = service;
            Material = material;
            OrderNote = orderNote;
            CustomerId = customer.Id;
            ServiceId = service.Id;
            MaterialId = material.Id;
        }
    }
}