﻿using System;
using System.Collections.Generic;
using GardarFramework.Core.Domain;
using ThreeDPrinting.Model.Entities;

namespace ThreeDPrinting.Model.AggregateRoot
{
    public class Service:AggregateRoot<Guid>
    {
        public string Name { get; set; }
        public bool HasMaterial { get; set; }
        public int? MaximumCharge { get; set; }
        public bool IsAttachedRequired { get; set; }
        public bool HasMaximumCharge { get; set; }
        public virtual IList<POSService> POSServices { get; set; }
    }
}