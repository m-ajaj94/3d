﻿using System;
using System.Collections.Generic;
using GardarFramework.Core.Domain;
using ThreeDPrinting.Model.Entities;

namespace ThreeDPrinting.Model.AggregateRoot
{
    public class CNCMaterial:AggregateRoot<Guid>
    {
        public string MaterialName { get; set; }
        public WorkType WorkType { get; set; }
        public Guid WorkTypeId { get; set; }
        public virtual IList<POSCNCMaterial> POSCNCMaterials { get; set; }
    }
}