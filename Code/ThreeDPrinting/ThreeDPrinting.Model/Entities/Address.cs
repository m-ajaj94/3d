﻿using System;
using GardarFramework.Core.Domain;
using ThreeDPrinting.Model.AggregateRoot;
using ThreeDPrinting.Model.Identity;

namespace ThreeDPrinting.Model.Entities
{
    public class Address:Entity<Guid>
    {
        public string ContactName { get; private set; }
        public string CountryCode { get; private set; }
        public string Mobile { get; private set; }
        public string StreetAddress { get; private set; }
        public string StreetAddressNumberPOBox { get; private set; }
        public string ApartmentSuiteUnitBuildingFloor { get; private set; }
        public string Country { get; private set; }
        public string StateProvinceCounty { get; private set; }
        public string City { get; private set; }
        public string ZipPostalCode { get; private set; }
        public string SecurityAccessCode { get; private set; }
        public bool IsDefaultShipping { get; private set; }
        public bool IsDefaultBilling { get; set; }
        public Customer Customer { get; set; }
        public Guid CustomerId { get; private set; }

        public Address(string contactName, string countryCode, string mobile, string streetAddress, string streetAddressNumberPOBox, string apartmentSuiteUnitBuildingFloor, string country, string stateProvinceCounty, string city, string zipPostalCode, string securityAccessCode, bool isDefaultShipping, bool isDefaultBilling, Guid customerId)
        {
            Id = Guid.NewGuid();
            ContactName = contactName;
            CountryCode = countryCode;
            Mobile = mobile;
            StreetAddress = streetAddress;
            StreetAddressNumberPOBox = streetAddressNumberPOBox;
            ApartmentSuiteUnitBuildingFloor = apartmentSuiteUnitBuildingFloor;
            Country = country;
            StateProvinceCounty = stateProvinceCounty;
            City = city;
            ZipPostalCode = zipPostalCode;
            SecurityAccessCode = securityAccessCode;
            IsDefaultShipping = isDefaultShipping;
            CustomerId = customerId;
            IsDefaultBilling = IsDefaultBilling;
        }

        public void EditAddress(string contactName, string countryCode, string mobile, string streetAddress, string streetAddressNumberPOBox, string apartmentSuiteUnitBuildingFloor, string country, string stateProvinceCounty, string city, string zipPostalCode, string securityAccessCode, bool isDefaultShipping, bool isDefaultBilling)
        {
            ContactName = contactName;
            CountryCode = countryCode;
            Mobile = mobile;
            StreetAddress = streetAddress;
            StreetAddressNumberPOBox = streetAddressNumberPOBox;
            ApartmentSuiteUnitBuildingFloor = apartmentSuiteUnitBuildingFloor;
            Country = country;
            StateProvinceCounty = stateProvinceCounty;
            City = city;
            ZipPostalCode = zipPostalCode;
            SecurityAccessCode = securityAccessCode;
            IsDefaultShipping = isDefaultShipping;
            IsDefaultBilling = isDefaultBilling;
        }
        public Address()
        {

        }

        public void SetNotDefaultShipping()
        {
            IsDefaultShipping = false;
        }
    }
}