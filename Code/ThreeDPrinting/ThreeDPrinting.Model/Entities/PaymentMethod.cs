﻿using System;
using GardarFramework.Core.Domain;
using ThreeDPrinting.Model.AggregateRoot;

namespace ThreeDPrinting.Model.Entities
{
    public class PaymentMethod:Entity<Guid>
    {
        public string CardNumber { get; private set; }
        public string Name { get; private set; }
        public DateTime ExpiryDate { get; private set; }
        public string CardCompany { get; private set; }
        public Guid CustomerId { get; private set; }
        public virtual Customer Customer { get; set; }

        public PaymentMethod(string cardNumber, string name, DateTime expiryDate, string cardCompany, Guid customerId)
        {
            Id=Guid.NewGuid();
            CardNumber = cardNumber;
            Name = name;
            ExpiryDate = expiryDate;
            CardCompany = cardCompany;
            CustomerId = customerId;
        }

        public PaymentMethod()
        {
            
        }

        public void EditPaymentMethod(string cardNumber, string name, DateTime expiryDate, string cardCompany)
        {
            Name = name;
            CardNumber = cardNumber;
            CardCompany = cardCompany;
            ExpiryDate = expiryDate;
        }
    }
}