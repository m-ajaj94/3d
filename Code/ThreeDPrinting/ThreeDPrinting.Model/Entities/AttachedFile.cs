using System;
using GardarFramework.Core.Domain;
using ThreeDPrinting.Model.AggregateRoot;

namespace ThreeDPrinting.Model.Entities
{
    public class AttachedFile:Entity<Guid>
    {
        public byte[] FileData { get; set; }
        public string Name { get; set; }
        public CustomerOrder Order { get; set; }
        public Guid CustomerOrderId { get; set; }
    }
}