﻿using System;
using ThreeDPrinting.Model.AggregateRoot;

namespace ThreeDPrinting.Model.Entities
{
    public class POSCNCMaterial
    {
        public Guid POSId { get; set; }
        public POS POS { get; set; }
        public Guid CNCMaterialId { get; set; }
        public CNCMaterial CNCMaterial { get; set; }
    }
}