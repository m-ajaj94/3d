﻿using System;
using ThreeDPrinting.Model.AggregateRoot;

namespace ThreeDPrinting.Model.Entities
{
    public class POSMaterial
    {
        public Guid POSId { get; set; }
        public POS POS { get; set; }
        public Guid MaterialId { get; set; }
        public Material Material { get; set; }
    }
}