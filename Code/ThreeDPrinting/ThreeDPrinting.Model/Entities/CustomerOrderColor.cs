﻿using System;
using ThreeDPrinting.Model.AggregateRoot;

namespace ThreeDPrinting.Model.Entities
{
    public class CustomerOrderColor
    {
        public CustomerOrder CustomerOrder { get; set; }
        public Guid CustomerOrderId { get; set; }
        public Color Color { get; set; }
        public Guid ColorId { get; set; }
    }
}