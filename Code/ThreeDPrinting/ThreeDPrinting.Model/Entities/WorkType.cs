﻿using System;
using GardarFramework.Core.Domain;

namespace ThreeDPrinting.Model.Entities
{
    public class WorkType:Entity<Guid>
    {
        public string Name { get; set; }
        public Criteria Criteria { get; set; }
        public Guid CriteriaId { get; set; }
    }
}