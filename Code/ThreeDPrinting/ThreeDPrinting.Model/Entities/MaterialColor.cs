﻿using System;
using ThreeDPrinting.Model.AggregateRoot;

namespace ThreeDPrinting.Model.Entities
{
    public class MaterialColor
    {
        public Material Material { get; set; }
        public Guid MaterialId { get; set; }
        public Color Color { get; set; }
        public Guid ColorId { get; set; }

        public MaterialColor()
        {
            
        }
    }
}