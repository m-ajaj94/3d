﻿using System;
using ThreeDPrinting.Model.AggregateRoot;

namespace ThreeDPrinting.Model.Entities
{
    public class POSService
    {
        public Guid POSId { get; set; }
        public POS POS { get; set; }
        public Guid ServiceId { get; set; }
        public Service Service { get; set; }
    }
}