﻿namespace ThreeDPrinting.Model.Enums
{
    public enum CurrencyType
    {
        USDollar,
        Euro
    }
}