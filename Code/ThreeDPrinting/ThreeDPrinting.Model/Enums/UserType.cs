﻿namespace ThreeDPrinting.Model.Enums
{
    public enum UserType
    {
        Customer,
        POS,
        Normal
    }
}