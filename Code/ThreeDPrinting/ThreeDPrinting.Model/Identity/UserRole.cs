﻿using System;

namespace ThreeDPrinting.Model.Identity
{
    public class UserRole
    {
        public SystemUser User { get; set; }
        public Guid UserId { get; set; }
        public SystemRole Role { get; set; }
        public Guid RoleId { get; set; }
    }
}