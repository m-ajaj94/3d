﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace ThreeDPrinting.Model.Identity
{
    public class SystemRole : IdentityRole<Guid>
    {
        public List<UserRole> UserRoles { get; set; }
    }
}