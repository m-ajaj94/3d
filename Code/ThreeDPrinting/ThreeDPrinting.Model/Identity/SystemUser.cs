﻿using System;
using System.Collections.Generic;
using GardarFramework.Core.Domain;
using Microsoft.AspNetCore.Identity;
using ThreeDPrinting.Model.Enums;

namespace ThreeDPrinting.Model.Identity
{
    public class SystemUser
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string NormalizedEmail { get; set; }
        public string UserName { get; set; }
        public string NormalizedUserName { get; set; }
        public string Password { get; set; }
        public byte[] Photo { get; set; }
        public bool LockoutEnabled { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string SecurityStamp { get; set; }
        public DateTimeOffset? LockoutEnd { get; set; }
        public int AccessFailedCount { get; set; }
        public UserType UserType { get; set; }
        public List<UserRole> UserRoles { get; set; }

    }
}