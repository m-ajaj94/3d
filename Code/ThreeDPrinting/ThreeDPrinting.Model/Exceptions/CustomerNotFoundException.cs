﻿using System;

namespace ThreeDPrinting.Model.Exceptions
{
    public class CustomerNotFoundException:Exception
    {
        public CustomerNotFoundException(string message) : base(message)
        {
        }
    }
}