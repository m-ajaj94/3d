﻿using System;

namespace ThreeDPrinting.Model.Exceptions
{
    public class MaterialNotFoundException : Exception
    {
        public MaterialNotFoundException(string message) : base(message)
        {
        }
    }
}