﻿using System;

namespace ThreeDPrinting.Model.Exceptions
{
    public class AddressNotFoundException : Exception
    {
        public AddressNotFoundException(string message) : base(message)
        {
        }
    }
}