﻿using System;

namespace ThreeDPrinting.Model.Exceptions
{
    public class CustomerAlreadyExistException:Exception
    {
        public CustomerAlreadyExistException(string message) : base(message)
        {
        }
    }
}