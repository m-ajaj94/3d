﻿using System;

namespace ThreeDPrinting.Model.Exceptions
{
    public class PaymentMethodNotFoundException : Exception
    {
        public PaymentMethodNotFoundException(string message) : base(message)
        {
        }
    }
}