﻿using System;

namespace ThreeDPrinting.Model.Exceptions
{
    public class ServiceNotFoundException : Exception
    {
        public ServiceNotFoundException(string message) : base(message)
        {
        }
    }
}