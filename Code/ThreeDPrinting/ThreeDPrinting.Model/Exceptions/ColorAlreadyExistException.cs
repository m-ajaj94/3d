﻿using System;

namespace ThreeDPrinting.Model.Exceptions
{
    public class ColorAlreadyExistException : Exception
    {
        public ColorAlreadyExistException(string message) : base(message)
        {
        }
    }
}