﻿using System;

namespace ThreeDPrinting.Model.Exceptions
{
    public class MaterialAlreadyExistException : Exception
    {
        public MaterialAlreadyExistException(string message) : base(message)
        {
        }
    }
}