﻿using System;

namespace ThreeDPrinting.Model.Exceptions
{
    public class ColorNotFoundException : Exception
    {
        public ColorNotFoundException(string message) : base(message)
        {
        }
    }
}