using MvvmCross.Platform.Plugins;

namespace ThreeDPrinting.Droid.Bootstrap
{
    public class NetworkPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Network.PluginLoader>
    {
    }
}