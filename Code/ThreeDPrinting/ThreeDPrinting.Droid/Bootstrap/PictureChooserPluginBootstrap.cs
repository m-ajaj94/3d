using MvvmCross.Platform.Plugins;

namespace ThreeDPrinting.Droid.Bootstrap
{
    public class PictureChooserPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.PictureChooser.PluginLoader>
    {
    }
}