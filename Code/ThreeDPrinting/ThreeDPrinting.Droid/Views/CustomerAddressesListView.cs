using Android.App;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Widget;
using MvvmCross.Droid.Views;
using System;

namespace ThreeDPrinting.Droid.Views
{
    [Activity(Label = "Modify Customer Info")]
    public class CustomerAddressesListView : MvxActivity
    {
        ImageButton boton_1;
        ImageButton boton_2;
        CheckBox DeleteCheckBox;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.CustomerAddressesListView);



            //


            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            toolbar.SetTitle(Resource.String.shipping_address_list);
            toolbar.SetTitleTextColor(Color.White);
            SetActionBar(toolbar);

            //for edit address

            boton_1 = FindViewById<ImageButton>(Resource.Id.ib_edit);
            boton_2 = FindViewById<ImageButton>(Resource.Id.ib_delete);
            DeleteCheckBox = FindViewById<CheckBox>(Resource.Id.cb_delete);
            boton_1.Click += boton1_Click;
            boton_2.Click += boton2_Click;


            //


            Drawable upArrow =Resources.GetDrawable(Resource.Drawable.abc_ic_ab_back_material);
            upArrow.SetColorFilter(Resources.GetColor(Resource.Color.white), PorterDuff.Mode.SrcAtop);

            ActionBar.SetHomeAsUpIndicator(upArrow);

            ActionBar.SetDisplayHomeAsUpEnabled(true);
          
        }

        private void boton1_Click(object sender, EventArgs e)
        {


            boton_1.Visibility = Android.Views.ViewStates.Gone;
            boton_2.Visibility = Android.Views.ViewStates.Visible;
            DeleteCheckBox.Visibility = Android.Views.ViewStates.Visible;
        }

        private void boton2_Click(object sender, EventArgs e)
        {


            boton_1.Visibility = Android.Views.ViewStates.Visible;
            boton_2.Visibility = Android.Views.ViewStates.Gone;
            DeleteCheckBox.Visibility = Android.Views.ViewStates.Gone;
        }
    }
}
