using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;

namespace ThreeDPrinting.Droid.Views
{
    [Activity(Label = "Customer Registration")]
    public class RegisterCustomerView : MvxActivity
    {
        protected override void OnCreate(Bundle bundle)
        {

           
            base.OnCreate(bundle);
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
            {
                Window.AddFlags(global::Android.Views.WindowManagerFlags.DrawsSystemBarBackgrounds);
                Window.SetStatusBarColor(Resources.GetColor(Resource.Color.my_primarydark));
            }
            SetContentView(Resource.Layout.RegisterCustomerView);
       
        }
    }
}
