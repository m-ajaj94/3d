using Android.App;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Widget;
using MvvmCross.Droid.Views;
using System;

namespace ThreeDPrinting.Droid.Views
{
    [Activity(Label = "Edit Payment Method")]
    public class EditPaymentMethodView : MvxActivity
    {
        
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.EditPaymentMethodView);



            //


            var toolbar = FindViewById<Toolbar>(Resource.Id.AddEditPaymentMethodViewToolBar);
            toolbar.SetTitle(Resource.String.shipping_address_list);
            toolbar.SetTitleTextColor(Color.White);
            SetActionBar(toolbar);

            //for edit address

           


            Drawable upArrow =Resources.GetDrawable(Resource.Drawable.abc_ic_ab_back_material);
            upArrow.SetColorFilter(Resources.GetColor(Resource.Color.white), PorterDuff.Mode.SrcAtop);

            ActionBar.SetHomeAsUpIndicator(upArrow);

            ActionBar.SetDisplayHomeAsUpEnabled(true);
          
        }

    }
}
