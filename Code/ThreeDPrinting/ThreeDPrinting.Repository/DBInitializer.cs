﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ThreeDPrinting.Model.Identity;

namespace ThreeDPrinting.Repository
{
    public class DBInitializer
    {
        private readonly DBContext _dbContext;

        public DBInitializer(DBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Seed()
        {
            _dbContext.Database.EnsureCreated();
            if (_dbContext.SystemUsers.Any())
            {
                return;   // DB has been seeded
            }
            var users = new List<SystemUser>()
            {
                new SystemUser()
                {
                Id = Guid.NewGuid(),
                UserName = "user1",
                NormalizedUserName = "USER1",
                Password = "pass1",
                FirstName = "Ali",
                LastName = "karem",
                Email = "test@hotmail.com",
                NormalizedEmail = "TEST@HOTMAIL.COM"
                }
            };
            foreach (var systemUser in users)
            {
                _dbContext.SystemUsers.Add(systemUser);
            }
            await _dbContext.SaveChangesAsync();

        }
    }
}