﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ThreeDPrinting.Repository.Migrations
{
    public partial class V11ModifyPOSModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerOrder_Customers_CustomerId",
                table: "CustomerOrder");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomerOrder_Materials_MaterialId",
                table: "CustomerOrder");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomerOrderColor_CustomerOrder_CustomerOrderId",
                table: "CustomerOrderColor");

            migrationBuilder.DropForeignKey(
                name: "FK_POSs_SystemUsers_POSUserId",
                table: "POSs");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CustomerOrder",
                table: "CustomerOrder");

            migrationBuilder.DropColumn(
                name: "DrawingFile",
                table: "CustomerOrder");

            migrationBuilder.RenameTable(
                name: "CustomerOrder",
                newName: "ThreeDOrder");

            migrationBuilder.RenameColumn(
                name: "IsAnnonymous",
                table: "POSs",
                newName: "HasPickUpService");

            migrationBuilder.RenameIndex(
                name: "IX_CustomerOrder_MaterialId",
                table: "ThreeDOrder",
                newName: "IX_ThreeDOrder_MaterialId");

            migrationBuilder.RenameIndex(
                name: "IX_CustomerOrder_CustomerId",
                table: "ThreeDOrder",
                newName: "IX_ThreeDOrder_CustomerId");

            migrationBuilder.AlterColumn<Guid>(
                name: "POSUserId",
                table: "POSs",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "POSs",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<TimeSpan>(
                name: "ClosingHour",
                table: "POSs",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "POSs",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<bool>(
                name: "HasCNCService",
                table: "POSs",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<byte[]>(
                name: "Image1",
                table: "POSs",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "Image2",
                table: "POSs",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "Image3",
                table: "POSs",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "Image4",
                table: "POSs",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Latitude",
                table: "POSs",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Longitude",
                table: "POSs",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "PercentageFromPrice",
                table: "POSs",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<TimeSpan>(
                name: "StartingHour",
                table: "POSs",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<string>(
                name: "Telephone",
                table: "POSs",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "WorkingHoursPrice",
                table: "POSs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FilePath",
                table: "ThreeDOrder",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "OrderNote",
                table: "ThreeDOrder",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ServiceId",
                table: "ThreeDOrder",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_ThreeDOrder",
                table: "ThreeDOrder",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "POSCNCMaterial",
                columns: table => new
                {
                    POSId = table.Column<Guid>(nullable: false),
                    CNCMaterialId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_POSCNCMaterial", x => new { x.POSId, x.CNCMaterialId });
                    table.ForeignKey(
                        name: "FK_POSCNCMaterial_CNCMaterial_CNCMaterialId",
                        column: x => x.CNCMaterialId,
                        principalTable: "CNCMaterial",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_POSCNCMaterial_POSs_POSId",
                        column: x => x.POSId,
                        principalTable: "POSs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "POSMaterial",
                columns: table => new
                {
                    POSId = table.Column<Guid>(nullable: false),
                    MaterialId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_POSMaterial", x => new { x.POSId, x.MaterialId });
                    table.ForeignKey(
                        name: "FK_POSMaterial_Materials_MaterialId",
                        column: x => x.MaterialId,
                        principalTable: "Materials",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_POSMaterial_POSs_POSId",
                        column: x => x.POSId,
                        principalTable: "POSs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ThreeDOrder_ServiceId",
                table: "ThreeDOrder",
                column: "ServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_POSCNCMaterial_CNCMaterialId",
                table: "POSCNCMaterial",
                column: "CNCMaterialId");

            migrationBuilder.CreateIndex(
                name: "IX_POSMaterial_MaterialId",
                table: "POSMaterial",
                column: "MaterialId");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerOrderColor_ThreeDOrder_CustomerOrderId",
                table: "CustomerOrderColor",
                column: "CustomerOrderId",
                principalTable: "ThreeDOrder",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_POSs_SystemUsers_POSUserId",
                table: "POSs",
                column: "POSUserId",
                principalTable: "SystemUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThreeDOrder_Customers_CustomerId",
                table: "ThreeDOrder",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ThreeDOrder_Materials_MaterialId",
                table: "ThreeDOrder",
                column: "MaterialId",
                principalTable: "Materials",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ThreeDOrder_Services_ServiceId",
                table: "ThreeDOrder",
                column: "ServiceId",
                principalTable: "Services",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerOrderColor_ThreeDOrder_CustomerOrderId",
                table: "CustomerOrderColor");

            migrationBuilder.DropForeignKey(
                name: "FK_POSs_SystemUsers_POSUserId",
                table: "POSs");

            migrationBuilder.DropForeignKey(
                name: "FK_ThreeDOrder_Customers_CustomerId",
                table: "ThreeDOrder");

            migrationBuilder.DropForeignKey(
                name: "FK_ThreeDOrder_Materials_MaterialId",
                table: "ThreeDOrder");

            migrationBuilder.DropForeignKey(
                name: "FK_ThreeDOrder_Services_ServiceId",
                table: "ThreeDOrder");

            migrationBuilder.DropTable(
                name: "POSCNCMaterial");

            migrationBuilder.DropTable(
                name: "POSMaterial");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ThreeDOrder",
                table: "ThreeDOrder");

            migrationBuilder.DropIndex(
                name: "IX_ThreeDOrder_ServiceId",
                table: "ThreeDOrder");

            migrationBuilder.DropColumn(
                name: "FilePath",
                table: "ThreeDOrder");

            migrationBuilder.DropColumn(
                name: "OrderNote",
                table: "ThreeDOrder");

            migrationBuilder.DropColumn(
                name: "ServiceId",
                table: "ThreeDOrder");

            migrationBuilder.DropColumn(
                name: "ClosingHour",
                table: "POSs");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "POSs");

            migrationBuilder.DropColumn(
                name: "HasCNCService",
                table: "POSs");

            migrationBuilder.DropColumn(
                name: "Image1",
                table: "POSs");

            migrationBuilder.DropColumn(
                name: "Image2",
                table: "POSs");

            migrationBuilder.DropColumn(
                name: "Image3",
                table: "POSs");

            migrationBuilder.DropColumn(
                name: "Image4",
                table: "POSs");

            migrationBuilder.DropColumn(
                name: "Latitude",
                table: "POSs");

            migrationBuilder.DropColumn(
                name: "Longitude",
                table: "POSs");

            migrationBuilder.DropColumn(
                name: "PercentageFromPrice",
                table: "POSs");

            migrationBuilder.DropColumn(
                name: "StartingHour",
                table: "POSs");

            migrationBuilder.DropColumn(
                name: "Telephone",
                table: "POSs");

            migrationBuilder.DropColumn(
                name: "WorkingHoursPrice",
                table: "POSs");

            migrationBuilder.RenameTable(
                name: "ThreeDOrder",
                newName: "CustomerOrder");

            migrationBuilder.RenameIndex(
                name: "IX_ThreeDOrder_MaterialId",
                table: "CustomerOrder",
                newName: "IX_CustomerOrder_MaterialId");

            migrationBuilder.RenameIndex(
                name: "IX_ThreeDOrder_CustomerId",
                table: "CustomerOrder",
                newName: "IX_CustomerOrder_CustomerId");

            migrationBuilder.RenameColumn(
                name: "HasPickUpService",
                table: "POSs",
                newName: "IsAnnonymous");

            migrationBuilder.AddColumn<byte[]>(
                name: "DrawingFile",
                table: "CustomerOrder",
                nullable: false,
                defaultValue: new byte[] {  });

            migrationBuilder.AlterColumn<Guid>(
                name: "POSUserId",
                table: "POSs",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "POSs",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddPrimaryKey(
                name: "PK_CustomerOrder",
                table: "CustomerOrder",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerOrder_Customers_CustomerId",
                table: "CustomerOrder",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerOrder_Materials_MaterialId",
                table: "CustomerOrder",
                column: "MaterialId",
                principalTable: "Materials",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerOrderColor_CustomerOrder_CustomerOrderId",
                table: "CustomerOrderColor",
                column: "CustomerOrderId",
                principalTable: "CustomerOrder",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_POSs_SystemUsers_POSUserId",
                table: "POSs",
                column: "POSUserId",
                principalTable: "SystemUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
