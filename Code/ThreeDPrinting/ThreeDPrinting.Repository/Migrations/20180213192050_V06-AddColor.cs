﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ThreeDPrinting.Repository.Migrations
{
    public partial class V06AddColor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "SecurityStamp",
                table: "SystemUsers",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "Colors",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    B = table.Column<int>(nullable: false),
                    ColorName = table.Column<string>(nullable: false),
                    G = table.Column<int>(nullable: false),
                    R = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Colors", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Colors");

            migrationBuilder.AlterColumn<string>(
                name: "SecurityStamp",
                table: "SystemUsers",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
