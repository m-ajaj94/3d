﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ThreeDPrinting.Repository.Migrations
{
    public partial class V12UpdateCustomerOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FilePath",
                table: "ThreeDOrder");

            migrationBuilder.CreateTable(
                name: "AttachedFile",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AddedDate = table.Column<DateTime>(nullable: false),
                    CustomerOrderId = table.Column<Guid>(nullable: false),
                    FileData = table.Column<byte[]>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttachedFile", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AttachedFile_ThreeDOrder_CustomerOrderId",
                        column: x => x.CustomerOrderId,
                        principalTable: "ThreeDOrder",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFile_CustomerOrderId",
                table: "AttachedFile",
                column: "CustomerOrderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AttachedFile");

            migrationBuilder.AddColumn<string>(
                name: "FilePath",
                table: "ThreeDOrder",
                nullable: false,
                defaultValue: "");
        }
    }
}
