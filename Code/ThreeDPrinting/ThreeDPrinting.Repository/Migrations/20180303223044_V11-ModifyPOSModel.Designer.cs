﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;
using ThreeDPrinting.Model.Enums;
using ThreeDPrinting.Repository;

namespace ThreeDPrinting.Repository.Migrations
{
    [DbContext(typeof(DBContext))]
    [Migration("20180303223044_V11-ModifyPOSModel")]
    partial class V11ModifyPOSModel
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.1-rtm-125")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ThreeDPrinting.Model.AggregateRoot.CNCMaterial", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("MaterialName")
                        .IsRequired();

                    b.Property<Guid>("WorkTypeId");

                    b.HasKey("Id");

                    b.HasIndex("WorkTypeId");

                    b.ToTable("CNCMaterial");
                });

            modelBuilder.Entity("ThreeDPrinting.Model.AggregateRoot.Color", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("B");

                    b.Property<string>("ColorName")
                        .IsRequired();

                    b.Property<int>("G");

                    b.Property<int>("R");

                    b.HasKey("Id");

                    b.ToTable("Colors");
                });

            modelBuilder.Entity("ThreeDPrinting.Model.AggregateRoot.Customer", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Customers");
                });

            modelBuilder.Entity("ThreeDPrinting.Model.AggregateRoot.CustomerOrder", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("CustomerId");

                    b.Property<string>("FilePath")
                        .IsRequired();

                    b.Property<Guid>("MaterialId");

                    b.Property<string>("OrderNote");

                    b.Property<Guid>("ServiceId");

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.HasIndex("MaterialId");

                    b.HasIndex("ServiceId");

                    b.ToTable("ThreeDOrder");
                });

            modelBuilder.Entity("ThreeDPrinting.Model.AggregateRoot.Material", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Currency");

                    b.Property<string>("MaterialName")
                        .IsRequired();

                    b.Property<int>("PricePerGram");

                    b.HasKey("Id");

                    b.ToTable("Materials");
                });

            modelBuilder.Entity("ThreeDPrinting.Model.AggregateRoot.POS", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<TimeSpan>("ClosingHour");

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<bool>("HasCNCService");

                    b.Property<bool>("HasPickUpService");

                    b.Property<byte[]>("Image1");

                    b.Property<byte[]>("Image2");

                    b.Property<byte[]>("Image3");

                    b.Property<byte[]>("Image4");

                    b.Property<double?>("Latitude");

                    b.Property<double?>("Longitude");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<Guid?>("POSUserId");

                    b.Property<double>("PercentageFromPrice");

                    b.Property<TimeSpan>("StartingHour");

                    b.Property<string>("Telephone")
                        .IsRequired();

                    b.Property<int?>("WorkingHoursPrice");

                    b.HasKey("Id");

                    b.HasIndex("POSUserId");

                    b.ToTable("POSs");
                });

            modelBuilder.Entity("ThreeDPrinting.Model.AggregateRoot.Service", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("HasMaterial");

                    b.Property<bool>("HasMaximumCharge");

                    b.Property<bool>("IsAttachedRequired");

                    b.Property<int?>("MaximumCharge");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Services");
                });

            modelBuilder.Entity("ThreeDPrinting.Model.Entities.Address", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedDate");

                    b.Property<string>("ApartmentSuiteUnitBuildingFloor")
                        .IsRequired();

                    b.Property<string>("City")
                        .IsRequired();

                    b.Property<string>("ContactName")
                        .IsRequired();

                    b.Property<string>("Country")
                        .IsRequired();

                    b.Property<string>("CountryCode")
                        .IsRequired();

                    b.Property<Guid>("CustomerId");

                    b.Property<bool>("IsDefaultBilling");

                    b.Property<bool>("IsDefaultShipping");

                    b.Property<string>("Mobile")
                        .IsRequired();

                    b.Property<DateTime>("ModifiedDate");

                    b.Property<string>("SecurityAccessCode");

                    b.Property<string>("StateProvinceCounty")
                        .IsRequired();

                    b.Property<string>("StreetAddress")
                        .IsRequired();

                    b.Property<string>("StreetAddressNumberPOBox")
                        .IsRequired();

                    b.Property<string>("ZipPostalCode")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.ToTable("Addresses");
                });

            modelBuilder.Entity("ThreeDPrinting.Model.Entities.Criteria", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<int>("Order");

                    b.HasKey("Id");

                    b.ToTable("Criteria");
                });

            modelBuilder.Entity("ThreeDPrinting.Model.Entities.CustomerOrderColor", b =>
                {
                    b.Property<Guid>("CustomerOrderId");

                    b.Property<Guid>("ColorId");

                    b.HasKey("CustomerOrderId", "ColorId");

                    b.HasIndex("ColorId");

                    b.ToTable("CustomerOrderColor");
                });

            modelBuilder.Entity("ThreeDPrinting.Model.Entities.MaterialColor", b =>
                {
                    b.Property<Guid>("MaterialId");

                    b.Property<Guid>("ColorId");

                    b.HasKey("MaterialId", "ColorId");

                    b.HasIndex("ColorId");

                    b.ToTable("MaterialColor");
                });

            modelBuilder.Entity("ThreeDPrinting.Model.Entities.PaymentMethod", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedDate");

                    b.Property<string>("CardCompany")
                        .IsRequired();

                    b.Property<string>("CardNumber")
                        .IsRequired();

                    b.Property<Guid>("CustomerId");

                    b.Property<DateTime>("ExpiryDate");

                    b.Property<DateTime>("ModifiedDate");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.ToTable("PaymentMethod");
                });

            modelBuilder.Entity("ThreeDPrinting.Model.Entities.POSCNCMaterial", b =>
                {
                    b.Property<Guid>("POSId");

                    b.Property<Guid>("CNCMaterialId");

                    b.HasKey("POSId", "CNCMaterialId");

                    b.HasIndex("CNCMaterialId");

                    b.ToTable("POSCNCMaterial");
                });

            modelBuilder.Entity("ThreeDPrinting.Model.Entities.POSMaterial", b =>
                {
                    b.Property<Guid>("POSId");

                    b.Property<Guid>("MaterialId");

                    b.HasKey("POSId", "MaterialId");

                    b.HasIndex("MaterialId");

                    b.ToTable("POSMaterial");
                });

            modelBuilder.Entity("ThreeDPrinting.Model.Entities.POSService", b =>
                {
                    b.Property<Guid>("POSId");

                    b.Property<Guid>("ServiceId");

                    b.HasKey("POSId", "ServiceId");

                    b.HasIndex("ServiceId");

                    b.ToTable("POSService");
                });

            modelBuilder.Entity("ThreeDPrinting.Model.Entities.WorkType", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedDate");

                    b.Property<Guid>("CriteriaId");

                    b.Property<DateTime>("ModifiedDate");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("CriteriaId");

                    b.ToTable("WorkType");
                });

            modelBuilder.Entity("ThreeDPrinting.Model.Identity.SystemRole", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("NormalizedName")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("SystemRoles");
                });

            modelBuilder.Entity("ThreeDPrinting.Model.Identity.SystemUser", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp");

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName")
                        .IsRequired();

                    b.Property<string>("LastName")
                        .IsRequired();

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .IsRequired();

                    b.Property<string>("NormalizedUserName")
                        .IsRequired();

                    b.Property<string>("Password")
                        .IsRequired();

                    b.Property<byte[]>("Photo");

                    b.Property<string>("SecurityStamp")
                        .IsRequired();

                    b.Property<string>("UserName")
                        .IsRequired();

                    b.Property<int>("UserType");

                    b.HasKey("Id");

                    b.ToTable("SystemUsers");
                });

            modelBuilder.Entity("ThreeDPrinting.Model.Identity.UserRole", b =>
                {
                    b.Property<Guid>("UserId");

                    b.Property<Guid>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("UsersRoles");
                });

            modelBuilder.Entity("ThreeDPrinting.Model.AggregateRoot.CNCMaterial", b =>
                {
                    b.HasOne("ThreeDPrinting.Model.Entities.WorkType", "WorkType")
                        .WithMany()
                        .HasForeignKey("WorkTypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ThreeDPrinting.Model.AggregateRoot.Customer", b =>
                {
                    b.HasOne("ThreeDPrinting.Model.Identity.SystemUser", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ThreeDPrinting.Model.AggregateRoot.CustomerOrder", b =>
                {
                    b.HasOne("ThreeDPrinting.Model.AggregateRoot.Customer", "Customer")
                        .WithMany("Orders")
                        .HasForeignKey("CustomerId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ThreeDPrinting.Model.AggregateRoot.Material", "Material")
                        .WithMany()
                        .HasForeignKey("MaterialId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ThreeDPrinting.Model.AggregateRoot.Service", "Service")
                        .WithMany()
                        .HasForeignKey("ServiceId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ThreeDPrinting.Model.AggregateRoot.POS", b =>
                {
                    b.HasOne("ThreeDPrinting.Model.Identity.SystemUser", "POSUser")
                        .WithMany()
                        .HasForeignKey("POSUserId");
                });

            modelBuilder.Entity("ThreeDPrinting.Model.Entities.Address", b =>
                {
                    b.HasOne("ThreeDPrinting.Model.AggregateRoot.Customer", "Customer")
                        .WithMany("Addresses")
                        .HasForeignKey("CustomerId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ThreeDPrinting.Model.Entities.CustomerOrderColor", b =>
                {
                    b.HasOne("ThreeDPrinting.Model.AggregateRoot.Color", "Color")
                        .WithMany("OrderColors")
                        .HasForeignKey("ColorId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ThreeDPrinting.Model.AggregateRoot.CustomerOrder", "CustomerOrder")
                        .WithMany("OrderColors")
                        .HasForeignKey("CustomerOrderId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ThreeDPrinting.Model.Entities.MaterialColor", b =>
                {
                    b.HasOne("ThreeDPrinting.Model.AggregateRoot.Color", "Color")
                        .WithMany("MaterialColors")
                        .HasForeignKey("ColorId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ThreeDPrinting.Model.AggregateRoot.Material", "Material")
                        .WithMany("MaterialColors")
                        .HasForeignKey("MaterialId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ThreeDPrinting.Model.Entities.PaymentMethod", b =>
                {
                    b.HasOne("ThreeDPrinting.Model.AggregateRoot.Customer", "Customer")
                        .WithMany("PaymentMethods")
                        .HasForeignKey("CustomerId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ThreeDPrinting.Model.Entities.POSCNCMaterial", b =>
                {
                    b.HasOne("ThreeDPrinting.Model.AggregateRoot.CNCMaterial", "CNCMaterial")
                        .WithMany("POSCNCMaterials")
                        .HasForeignKey("CNCMaterialId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ThreeDPrinting.Model.AggregateRoot.POS", "POS")
                        .WithMany("POSCNCMaterials")
                        .HasForeignKey("POSId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ThreeDPrinting.Model.Entities.POSMaterial", b =>
                {
                    b.HasOne("ThreeDPrinting.Model.AggregateRoot.Material", "Material")
                        .WithMany("POSMaterials")
                        .HasForeignKey("MaterialId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ThreeDPrinting.Model.AggregateRoot.POS", "POS")
                        .WithMany("POSMaterials")
                        .HasForeignKey("POSId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ThreeDPrinting.Model.Entities.POSService", b =>
                {
                    b.HasOne("ThreeDPrinting.Model.AggregateRoot.POS", "POS")
                        .WithMany("POSServices")
                        .HasForeignKey("POSId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ThreeDPrinting.Model.AggregateRoot.Service", "Service")
                        .WithMany("POSServices")
                        .HasForeignKey("ServiceId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ThreeDPrinting.Model.Entities.WorkType", b =>
                {
                    b.HasOne("ThreeDPrinting.Model.Entities.Criteria", "Criteria")
                        .WithMany()
                        .HasForeignKey("CriteriaId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ThreeDPrinting.Model.Identity.UserRole", b =>
                {
                    b.HasOne("ThreeDPrinting.Model.Identity.SystemRole", "Role")
                        .WithMany("UserRoles")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ThreeDPrinting.Model.Identity.SystemUser", "User")
                        .WithMany("UserRoles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
