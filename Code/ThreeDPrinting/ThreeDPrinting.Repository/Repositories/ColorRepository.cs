﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GardarFramework.Data.Entity;
using GardarFramework.Data.Entity.Repositories;
using Microsoft.EntityFrameworkCore;
using ThreeDPrinting.Model.AggregateRoot;
using ThreeDPrinting.Model.DTO;
using ThreeDPrinting.Model.Entities;
using ThreeDPrinting.Model.Exceptions;
using ThreeDPrinting.Model.Identity;

namespace ThreeDPrinting.Repository.Repositories
{
    public class ColorRepository : IColorRepository
    {
        private readonly DBContext _dbContext;

        public ColorRepository(DBContext dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task AddColor(Color color)
        {
            try
            {
                using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                {
                    var oldColor = await _dbContext.Colors.SingleOrDefaultAsync(x => x.ColorName == color.ColorName);
                    if (oldColor != null)
                        throw new ColorAlreadyExistException($"Color {color.ColorName} Already Exist!!");
                    await _dbContext.Colors.AddAsync(color);
                    await _dbContext.SaveChangesAsync();
                    transaction.Commit();
                }
            }
            catch (ColorAlreadyExistException)
            {
                _dbContext.Database.RollbackTransaction();
                throw;
            }
            catch (Exception e)
            {
                _dbContext.Database.RollbackTransaction();
                throw new Exception("Exception while Adding new Color !!", e);
            }
        }

        public async Task EditColor(Guid colorId, string colorName, int r, int g, int b)
        {
            try
            {
                using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                {
                    var color = await _dbContext.Colors
                        .SingleOrDefaultAsync(x => x.Id == colorId);
                    if (color == null)
                        throw new ColorNotFoundException($"Color with Id: {colorId} Not Found!!");
                    color.ModifyColor(colorName, r, g, b);
                    _dbContext.Colors.Update(color);
                    await _dbContext.SaveChangesAsync();
                    transaction.Commit();
                }
            }
            catch (ColorNotFoundException)
            {
                _dbContext.Database.RollbackTransaction();
                throw;
            }
            catch (Exception e)
            {
                _dbContext.Database.RollbackTransaction();
                throw new Exception($"Exception while Editing Color {colorId} !!", e);
            }
        }

        public async Task DeleteColor(Guid colorId)
        {
            try
            {
                using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                {
                    var color = await _dbContext.Colors
                        .SingleOrDefaultAsync(x => x.Id == colorId);
                    if (color == null)
                        throw new ColorNotFoundException($"Color {colorId} Not Found");
                    _dbContext.Colors.Remove(color);
                    await _dbContext.SaveChangesAsync();
                    transaction.Commit();
                }
            }
            catch (ColorNotFoundException)
            {
                _dbContext.Database.RollbackTransaction();
                throw;
            }
            catch (Exception e)
            {
                _dbContext.Database.RollbackTransaction();
                throw new Exception($"Exception while Deleting a Color {colorId} !!", e);
            }
        }

        public async Task<ColorDTO> GetColor(Guid colorId)
        {
            var color = await _dbContext.Colors
                .SingleOrDefaultAsync(x => x.Id == colorId);
            if (color == null)
                throw new ColorNotFoundException($"Color with Id: {colorId} Not Found!!");
            return new ColorDTO()
            {
                ColorId = color.Id,
                ColorName = color.ColorName,
                R = color.R,
                G = color.G,
                B = color.B
            };
        }

        public Task<List<ColorDTO>> GetAllColors()
        {
            return _dbContext.Colors.Select(x => new ColorDTO()
            {
                ColorId = x.Id,
                ColorName = x.ColorName,
                R = x.R,
                G=x.G,
                B = x.B
            }).ToListAsync();
        }
    }

    public interface IColorRepository
    {
        Task AddColor(Color color);
        Task EditColor(Guid colorId, string colorName, int r, int g, int b);
        Task DeleteColor(Guid colorId);
        Task<ColorDTO> GetColor(Guid colorId);
        Task<List<ColorDTO>> GetAllColors();
    }
}