﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GardarFramework.Data.Entity;
using GardarFramework.Data.Entity.Repositories;
using Microsoft.EntityFrameworkCore;
using ThreeDPrinting.Model.AggregateRoot;
using ThreeDPrinting.Model.DTO;
using ThreeDPrinting.Model.Entities;
using ThreeDPrinting.Model.Enums;
using ThreeDPrinting.Model.Exceptions;
using ThreeDPrinting.Model.Identity;

namespace ThreeDPrinting.Repository.Repositories
{
    public class MaterialRepository : IMaterialRepository
    {
        private readonly DBContext _dbContext;

        public MaterialRepository(DBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddMaterial(Material material, List<Guid> colors)
        {
            try
            {
                using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                {
                    var oldMaterial = await _dbContext.Materials.SingleOrDefaultAsync(x => x.MaterialName == material.MaterialName);
                    if (oldMaterial != null)
                        throw new MaterialAlreadyExistException($"Material {material.MaterialName} Already Exist!!");
                    material.MaterialColors=new List<MaterialColor>();
                    foreach (var colorId in colors)
                    {
                        var color = await _dbContext.Colors.SingleOrDefaultAsync(x => x.Id == colorId);
                        if (color != null)
                        {
                            material.MaterialColors.Add(new MaterialColor()
                            {
                                Color = color,
                                ColorId = colorId,
                                Material = material,
                                MaterialId = material.Id
                            });
                        }
                    }
                    await _dbContext.Materials.AddAsync(material);
                    await _dbContext.SaveChangesAsync();
                    transaction.Commit();
                }
            }
            catch (MaterialAlreadyExistException)
            {
                _dbContext.Database.RollbackTransaction();
                throw;
            }
            catch (Exception e)
            {
                _dbContext.Database.RollbackTransaction();
                throw new Exception("Exception while Adding new Material !!", e);
            }
        }

        public async Task EditMaterial(Guid materialId, string materialName, CurrencyType currency, int pricePerGram, List<Guid> colors)
        {
            try
            {
                using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                {
                    var material = await _dbContext.Materials.Include(x=>x.MaterialColors)
                        .SingleOrDefaultAsync(x => x.Id == materialId);
                    if (material == null)
                        throw new MaterialNotFoundException($"Material with Id: {materialId} Not Found!!");
                    material.ModifyMaterial(materialName, currency, pricePerGram);
                    material.MaterialColors.Clear();
                    _dbContext.Materials.Update(material);
                    await _dbContext.SaveChangesAsync();
                    foreach (var colorId in colors)
                    {
                        var color = await _dbContext.Colors.SingleOrDefaultAsync(x => x.Id == colorId);
                        if (color != null)
                        {
                            material.MaterialColors.Add(new MaterialColor()
                            {
                                Color = color,
                                ColorId = colorId,
                                Material = material,
                                MaterialId = material.Id
                            });
                        }
                    }
                    _dbContext.Materials.Update(material);
                    await _dbContext.SaveChangesAsync();
                    transaction.Commit();
                }
            }
            catch (MaterialNotFoundException)
            {
                _dbContext.Database.RollbackTransaction();
                throw;
            }
            catch (Exception e)
            {
                _dbContext.Database.RollbackTransaction();
                throw new Exception($"Exception while Editing Material {materialId} !!", e);
            }
        }

        public async Task DeleteMaterial(Guid materialId)
        {
            try
            {
                using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                {
                    var material = await _dbContext.Materials
                        .SingleOrDefaultAsync(x => x.Id == materialId);
                    if (material == null)
                        throw new MaterialNotFoundException($"Material {materialId} Not Found");
                    _dbContext.Materials.Remove(material);
                    await _dbContext.SaveChangesAsync();
                    transaction.Commit();
                }
            }
            catch (MaterialNotFoundException)
            {
                _dbContext.Database.RollbackTransaction();
                throw;
            }
            catch (Exception e)
            {
                _dbContext.Database.RollbackTransaction();
                throw new Exception($"Exception while Deleting a Material {materialId} !!", e);
            }
        }

        public async Task<MaterialDTO> GetMaterial(Guid materialId)
        {
            var material = await _dbContext.Materials
                .SingleOrDefaultAsync(x => x.Id == materialId);
            if (material == null)
                throw new MaterialNotFoundException($"Material with Id: {materialId} Not Found!!");
            return new MaterialDTO()
            {
                MaterialId = materialId,
                MaterialName = material.MaterialName,
                Currency = material.Currency,
                PricePerGram = material.PricePerGram,
                Colors = await _dbContext.Materials.Where(x => x.Id == materialId).SelectMany(x => x.MaterialColors)
                     .Select(x => x.Color).Select(x => new ColorDTO()
                     {
                         ColorId = x.Id,
                         ColorName = x.ColorName,
                         R = x.R,
                         G = x.G,
                         B = x.B
                     }).ToListAsync()
            };
        }

        public async Task<List<MaterialDTO>> GetAllMaterials()
        {
            var materials = await _dbContext.Materials.Select(x => new MaterialDTO()
            {
                MaterialId = x.Id,
                MaterialName = x.MaterialName,
                Currency = x.Currency,
                PricePerGram = x.PricePerGram
            }).ToListAsync();
            foreach (var material in materials)
            {
                material.Colors = await _dbContext.Materials.Where(x => x.Id == material.MaterialId)
                    .SelectMany(x => x.MaterialColors)
                    .Select(x => x.Color).Select(x => new ColorDTO()
                    {
                        ColorId = x.Id,
                        ColorName = x.ColorName,
                        R = x.R,
                        G = x.G,
                        B = x.B
                    }).ToListAsync();
            }
            return materials;
        }
    }

    public interface IMaterialRepository
    {
        Task EditMaterial(Guid materialId, string materialName, CurrencyType currency, int pricePerGram, List<Guid> colors);
        Task DeleteMaterial(Guid materialId);
        Task<MaterialDTO> GetMaterial(Guid materialId);
        Task<List<MaterialDTO>> GetAllMaterials();
        Task AddMaterial(Material material, List<Guid> colors);
    }
}