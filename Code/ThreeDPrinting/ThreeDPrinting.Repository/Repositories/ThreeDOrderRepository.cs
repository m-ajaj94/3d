﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using ThreeDPrinting.Model.AggregateRoot;
using ThreeDPrinting.Model.Entities;
using ThreeDPrinting.Model.Exceptions;

namespace ThreeDPrinting.Repository.Repositories
{
    public class ThreeDOrderRepository : IThreeDOrderRepository
    {
        private readonly DBContext _dbContext;

        public ThreeDOrderRepository(DBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task CreateOrder(Guid customerId, Guid serviceId, Guid materialId, List<Guid> colorsId, byte[] fileData, string orderNote)
        {
            try
            {
                using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                {
                    var customer = await _dbContext.Customers.SingleOrDefaultAsync(x => x.Id == customerId);
                    if (customer == null)
                        throw new CustomerNotFoundException($"Customer {customerId} Not Found!!");
                    var service = await _dbContext.Services.SingleOrDefaultAsync(x => x.Id == serviceId);
                    if (service == null)
                        throw new ServiceNotFoundException($"Service {serviceId} Not Found!!");
                    var material = await _dbContext.Materials.SingleOrDefaultAsync(x => x.Id == materialId);
                    if (material == null)
                        throw new MaterialNotFoundException($"Material {materialId} Not Found!!");
                    var customerOrder = new CustomerOrder(customer, service, material, orderNote);
                    foreach (var colorId in colorsId)
                    {
                        var color = await _dbContext.Colors.SingleOrDefaultAsync(x => x.Id == colorId);
                        if (color != null)
                        {
                            customerOrder.OrderColors.Add(new CustomerOrderColor()
                            {
                                Color = color,
                                ColorId = colorId,
                                CustomerOrder = customerOrder,
                                CustomerOrderId = customerOrder.Id
                            });
                        }
                    }
                    customerOrder.AttachedFiles.Add(new AttachedFile()
                    {
                        Id = Guid.NewGuid(),
                        AddedDate = DateTime.Now,
                        FileData = fileData,
                        CustomerOrderId = customerOrder.Id,
                        Order = customerOrder
                    });
                    await _dbContext.ThreeDOrder.AddAsync(customerOrder);
                    await _dbContext.SaveChangesAsync();
                    transaction.Commit();
                }
            }
            catch (Exception e)
            {
                _dbContext.Database.RollbackTransaction();
                throw new Exception("Exception while Creating new ThreeDOrder !!", e);
            }
        }
    }

    public interface IThreeDOrderRepository
    {
        Task CreateOrder(Guid customerId, Guid serviceId, Guid materialId, List<Guid> colorsId, byte[] fileData, string orderNote);
    }
}