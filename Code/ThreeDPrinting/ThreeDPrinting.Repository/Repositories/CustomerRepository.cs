﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GardarFramework.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using ThreeDPrinting.Model.AggregateRoot;
using ThreeDPrinting.Model.DTO;
using ThreeDPrinting.Model.Entities;
using ThreeDPrinting.Model.Exceptions;
using ThreeDPrinting.Model.Identity;

namespace ThreeDPrinting.Repository.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly DBContext _dbContext;

        public CustomerRepository(DBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddCustomer(SystemUser user)
        {
            var oldCustomer = await _dbContext.Customers.FirstOrDefaultAsync(x => x.UserId == user.Id);
            if (oldCustomer != null)
            {
                throw new CustomerAlreadyExistException($"Customer with user Id: {user.Id} Already Exist!!");
            }
            var customer = new Customer()
            {
                Id = Guid.NewGuid(),
                User = user,
                UserId = user.Id,
                Addresses = new List<Address>()
            };
            await _dbContext.Customers.AddAsync(customer);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<Guid> GetUserId(Guid customerId)
        {
            var customer = await _dbContext.Customers.FirstOrDefaultAsync(x => x.Id == customerId);
            if (customer == null)
            {
                throw new CustomerNotFoundException($"Customer with Id: {customerId} Not Found!!");
            }
            return customer.UserId;
        }
        public async Task<Customer> GetCustomerForUserId(Guid userId)
        {
            var customer = await _dbContext.Customers.FirstOrDefaultAsync(x => x.UserId == userId);
            if (customer == null)
            {
                throw new CustomerNotFoundException($"Customer with Id: {userId} Not Found!!");
            }
            return customer;
        }

        public async Task AddAddress(Address address)
        {
            try
            {
                using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                {
                    var customer = await _dbContext.Customers.Include(x => x.Addresses)
                        .SingleOrDefaultAsync(x => x.Id == address.CustomerId);
                    if (customer == null)
                        throw new CustomerNotFoundException($"Customer with Id: {address.CustomerId} Not Found!!");
                    if (address.IsDefaultShipping)
                        customer.Addresses.Where(x => x.IsDefaultShipping).ExecuteAction(x => x.SetNotDefaultShipping());
                    if (address.IsDefaultBilling)
                        customer.Addresses.Where(x => x.IsDefaultBilling).ExecuteAction(x => x.SetNotDefaultShipping());
                    address.AddedDate = DateTime.Now;
                    customer.Addresses.Add(address);
                    _dbContext.Customers.Update(customer);
                    await _dbContext.SaveChangesAsync();
                    transaction.Commit();
                }
            }
            catch (CustomerNotFoundException)
            {
                _dbContext.Database.RollbackTransaction();
                throw;
            }
            catch (Exception e)
            {
                _dbContext.Database.RollbackTransaction();
                throw new Exception("Exception while Adding new Address !!", e);
            }
        }

        public async Task EditAddress(Guid customerId, Guid addressId, string apartmentSuiteUnitBuildingFloor, string city, string contactName,
            string country, string countryCode, bool isDefaultShipping, string mobile, string securityAccessCode,
            string stateProvinceCounty, string streetAddress, string streetAddressNumberPOBox, string zipPostalCode, bool isDefaultBilling)
        {
            try
            {
                using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                {
                    var customer = await _dbContext.Customers.Include(x => x.Addresses)
                        .SingleOrDefaultAsync(x => x.Id == customerId);
                    if (customer == null)
                        throw new CustomerNotFoundException($"Customer {customerId} Not Found");
                    var address = customer.Addresses.SingleOrDefault(x => x.Id == addressId);
                    if (address == null)
                        throw new AddressNotFoundException($"Address {addressId} Not Found");
                    if (isDefaultShipping)
                    {
                        customer.Addresses.Where(x => x.IsDefaultShipping).ExecuteAction(x => x.SetNotDefaultShipping());
                    }
                    if (isDefaultBilling)
                        customer.Addresses.Where(x => x.IsDefaultBilling).ExecuteAction(x => x.SetNotDefaultShipping());
                    address.EditAddress(contactName, countryCode, mobile, streetAddress, streetAddressNumberPOBox,
                        apartmentSuiteUnitBuildingFloor, country, stateProvinceCounty, city, zipPostalCode,
                        securityAccessCode, isDefaultShipping, isDefaultBilling);
                    address.ModifiedDate = DateTime.Now;
                    _dbContext.Customers.Update(customer);
                    await _dbContext.SaveChangesAsync();
                    transaction.Commit();
                }
            }
            catch (CustomerNotFoundException)
            {
                _dbContext.Database.RollbackTransaction();
                throw;
            }
            catch (AddressNotFoundException)
            {
                _dbContext.Database.RollbackTransaction();
                throw;
            }
            catch (Exception e)
            {
                _dbContext.Database.RollbackTransaction();
                throw new Exception("Exception while Updating an Address !!", e);
            }

        }

        public async Task DeleteAddress(Guid customerId, Guid addressId)
        {
            try
            {
                using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                {
                    var customer = await _dbContext.Customers.Include(x => x.Addresses)
                        .SingleOrDefaultAsync(x => x.Id == customerId);
                    if (customer == null)
                        throw new CustomerNotFoundException($"Customer {customerId} Not Found");
                    var address = customer.Addresses.SingleOrDefault(x => x.Id == addressId);
                    if (address == null)
                        throw new AddressNotFoundException($"Address {addressId} Not Found");
                    customer.Addresses.Remove(address);
                    _dbContext.Customers.Update(customer);
                    await _dbContext.SaveChangesAsync();
                    transaction.Commit();
                }
            }
            catch (CustomerNotFoundException)
            {
                _dbContext.Database.RollbackTransaction();
                throw;
            }
            catch (AddressNotFoundException)
            {
                _dbContext.Database.RollbackTransaction();
                throw;
            }
            catch (Exception e)
            {
                _dbContext.Database.RollbackTransaction();
                throw new Exception("Exception while Deleting an Address !!", e);
            }
        }
        public async Task DeletePaymentMethod(Guid customerId, Guid paymentMethodId)
        {
            try
            {
                using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                {
                    var customer = await _dbContext.Customers.Include(x => x.PaymentMethods)
                        .SingleOrDefaultAsync(x => x.Id == customerId);
                    if (customer == null)
                        throw new CustomerNotFoundException($"Customer {customerId} Not Found");
                    var paymentMethod = customer.PaymentMethods.SingleOrDefault(x => x.Id == paymentMethodId);
                    if (paymentMethod == null)
                        throw new PaymentMethodNotFoundException($"Payment Method {paymentMethodId} Not Found");
                    customer.PaymentMethods.Remove(paymentMethod);
                    _dbContext.Customers.Update(customer);
                    await _dbContext.SaveChangesAsync();
                    transaction.Commit();
                }
            }
            catch (CustomerNotFoundException)
            {
                _dbContext.Database.RollbackTransaction();
                throw;
            }
            catch (PaymentMethodNotFoundException)
            {
                _dbContext.Database.RollbackTransaction();
                throw;
            }
            catch (Exception e)
            {
                _dbContext.Database.RollbackTransaction();
                throw new Exception("Exception while Deleting a Payment Method !!", e);
            }
        }

        public async Task EditPaymentMethod(Guid customerId, Guid paymentMethodId, string name, string cardNumber, string cardCompany,
            DateTime expiryDate)
        {
            try
            {
                using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                {
                    var customer = await _dbContext.Customers.Include(x => x.PaymentMethods)
                        .SingleOrDefaultAsync(x => x.Id == customerId);
                    if (customer == null)
                        throw new CustomerNotFoundException($"Customer with Id: {customerId} Not Found!!");
                    var paymentMethod = customer.PaymentMethods.SingleOrDefault(x => x.Id == paymentMethodId);
                    if (paymentMethod == null)
                        throw new PaymentMethodNotFoundException($"Payment Method {paymentMethodId} Not Found");
                    paymentMethod.EditPaymentMethod(cardNumber, name, expiryDate, cardCompany);
                    paymentMethod.ModifiedDate = DateTime.Now;
                    _dbContext.Customers.Update(customer);
                    await _dbContext.SaveChangesAsync();
                    transaction.Commit();
                }
            }
            catch (CustomerNotFoundException)
            {
                _dbContext.Database.RollbackTransaction();
                throw;
            }
            catch (PaymentMethodNotFoundException)
            {
                _dbContext.Database.RollbackTransaction();
                throw;
            }
            catch (Exception e)
            {
                _dbContext.Database.RollbackTransaction();
                throw new Exception("Exception while Editing Payment Method !!", e);
            }
        }
        public async Task<List<AddressDTO>> GetCustomerAddresses(Guid customerId)
        {
            var customer = await _dbContext.Customers.Include(x => x.Addresses)
                .SingleOrDefaultAsync(x => x.Id == customerId);
            if (customer == null)
                throw new CustomerNotFoundException($"Customer {customerId} Not Found");
            return customer.Addresses.Select(x => new AddressDTO()
            {
                AddressId = x.Id,
                ApartmentSuiteUnitBuildingFloor = x.ApartmentSuiteUnitBuildingFloor,
                City = x.City,
                ContactName = x.ContactName,
                Country = x.Country,
                CountryCode = x.CountryCode,
                CustomerId = customerId,
                IsDefaultBilling = x.IsDefaultBilling,
                IsDefaultShipping = x.IsDefaultShipping,
                Mobile = x.Mobile,
                SecurityAccessCode = x.SecurityAccessCode,
                StateProvinceCounty = x.StateProvinceCounty,
                StreetAddress = x.StreetAddress,
                StreetAddressNumberPOBox = x.StreetAddressNumberPOBox,
                ZipPostalCode = x.ZipPostalCode
            }).ToList();

        }

        public async Task<AddressDTO> GetCustomerAddress(Guid customerId, Guid addressId)
        {
            var customer = await _dbContext.Customers.Include(x => x.Addresses)
                .SingleOrDefaultAsync(x => x.Id == customerId);
            if (customer == null)
                throw new CustomerNotFoundException($"Customer {customerId} Not Found");
            return customer.Addresses.Where(x => x.Id == addressId).Select(x => new AddressDTO()
            {
                CustomerId = customerId,
                AddressId = addressId,
                IsDefaultBilling = x.IsDefaultBilling,
                IsDefaultShipping = x.IsDefaultShipping,
                ApartmentSuiteUnitBuildingFloor = x.ApartmentSuiteUnitBuildingFloor,
                Country = x.Country,
                Mobile = x.Mobile,
                StateProvinceCounty = x.StateProvinceCounty,
                ZipPostalCode = x.ZipPostalCode,
                StreetAddress = x.StreetAddress,
                StreetAddressNumberPOBox = x.StreetAddressNumberPOBox,
                ContactName = x.ContactName,
                SecurityAccessCode = x.SecurityAccessCode,
                City = x.City,
                CountryCode = x.CountryCode,
            }).SingleOrDefault();
        }

        public async Task AddPaymentMethod(PaymentMethod paymentMethod)
        {
            try
            {
                using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                {
                    var customer = await _dbContext.Customers.Include(x => x.PaymentMethods)
                        .SingleOrDefaultAsync(x => x.Id == paymentMethod.CustomerId);
                    if (customer == null)
                        throw new CustomerNotFoundException($"Customer with Id: {paymentMethod.CustomerId} Not Found!!");
                    paymentMethod.AddedDate = DateTime.Now;
                    customer.PaymentMethods.Add(paymentMethod);
                    _dbContext.Customers.Update(customer);
                    await _dbContext.SaveChangesAsync();
                    transaction.Commit();
                }
            }
            catch (CustomerNotFoundException)
            {
                _dbContext.Database.RollbackTransaction();
                throw;
            }
            catch (Exception e)
            {
                _dbContext.Database.RollbackTransaction();
                throw new Exception("Exception while Adding new Payment Method !!", e);
            }
        }

        public async Task<List<PaymentMethodDTO>> GetAllPaymentMethods(Guid customerId)
        {
            var customer = await _dbContext.Customers.Include(x => x.PaymentMethods)
                .SingleOrDefaultAsync(x => x.Id == customerId);
            if (customer == null)
                throw new CustomerNotFoundException($"Customer with Id: {customerId} Not Found!!");
            return customer.PaymentMethods.Select(x => new PaymentMethodDTO()
            {
                Name = x.Name,
                CustomerId = customerId,
                CardCompany = x.CardCompany,
                CardNumber = x.CardNumber,
                ExpiryDate = x.ExpiryDate,
                PaymentMethodId = x.Id
            }).ToList();
        }

        public async Task<PaymentMethodDTO> GetPaymentMethod(Guid customerId, Guid paymentMethodId)
        {
            var customer = await _dbContext.Customers.Include(x => x.PaymentMethods)
                .SingleOrDefaultAsync(x => x.Id == customerId);
            if (customer == null)
                throw new CustomerNotFoundException($"Customer with Id: {customerId} Not Found!!");
            return customer.PaymentMethods.Where(x => x.Id == paymentMethodId).Select(x => new PaymentMethodDTO()
            {
                Name = x.Name,
                CustomerId = customerId,
                CardCompany = x.CardCompany,
                CardNumber = x.CardNumber,
                ExpiryDate = x.ExpiryDate,
                PaymentMethodId = x.Id
            }).SingleOrDefault();
        }

    }

    public interface ICustomerRepository
    {
        Task AddCustomer(SystemUser user);
        Task<Guid> GetUserId(Guid customerId);
        Task AddAddress(Address address);
        Task EditAddress(Guid customerId, Guid addressId, string apartmentSuiteUnitBuildingFloor, string city, string contactName, string country, string countryCode, bool isDefaultShipping, string mobile, string securityAccessCode, string stateProvinceCounty, string streetAddress, string streetAddressNumberPOBox, string zipPostalCode, bool isDefaultBilling);
        Task<List<AddressDTO>> GetCustomerAddresses(Guid customerId);
        Task DeleteAddress(Guid customerId, Guid addressId);
        Task AddPaymentMethod(PaymentMethod paymentMethod);
        Task<List<PaymentMethodDTO>> GetAllPaymentMethods(Guid customerId);
        Task<PaymentMethodDTO> GetPaymentMethod(Guid customerId, Guid paymentMethodId);
        Task DeletePaymentMethod(Guid customerId, Guid paymentMethodId);
        Task EditPaymentMethod(Guid customerId, Guid paymentMethodId, string name, string cardNumber, string cardCompany, DateTime expiryDate);
        Task<AddressDTO> GetCustomerAddress(Guid customerId, Guid addressId);
        Task<Customer> GetCustomerForUserId(Guid userId);
    }
}