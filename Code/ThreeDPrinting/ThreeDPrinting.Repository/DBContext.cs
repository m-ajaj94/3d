﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ThreeDPrinting.Mapping;
using ThreeDPrinting.Model.AggregateRoot;
using ThreeDPrinting.Model.Entities;
using ThreeDPrinting.Model.Identity;

namespace ThreeDPrinting.Repository
{
    public class DBContext : DbContext
    {
        public DBContext(DbContextOptions<DBContext> options)
            : base(options)
        {
        }
        public DbSet<SystemUser> SystemUsers { get; set; }
        public DbSet<SystemRole> SystemRoles { get; set; }
        public DbSet<UserRole> UsersRoles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<POS> POSs { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Color> Colors { get; set; }
        public DbSet<Material> Materials { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<CustomerOrder> ThreeDOrder { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            //var mappingTypes = typeof(CriteriaMap).Assembly.GetTypes()
            //    .Where(x =>
            //        !x.GetTypeInfo().IsAbstract &&
            //        x.GetInterfaces().Any(y => y.GetTypeInfo().IsGenericType && y.GetGenericTypeDefinition() == typeof(IEntityMappingConfiguration<>)));
            //foreach (var config in mappingTypes.Select(Activator.CreateInstance).Cast<IEntityMappingConfiguration>())
            //    config.Map(builder);
            builder.ApplyConfiguration(new SystemUserMap());
            builder.ApplyConfiguration(new SystemRoleMap());
            builder.ApplyConfiguration(new CustomerMap());
            builder.ApplyConfiguration(new POSMap());
            builder.ApplyConfiguration(new AddressMap());
            builder.ApplyConfiguration(new PaymentMethodMap());
            builder.ApplyConfiguration(new ColorMap());
            builder.ApplyConfiguration(new MaterialMap());
            builder.ApplyConfiguration(new UserRoleMap());
            builder.ApplyConfiguration(new MaterialColorMap());
            builder.ApplyConfiguration(new CustomerOrderMap());
            builder.ApplyConfiguration(new OrderColorMap());
            builder.ApplyConfiguration(new ServiceMap());
            builder.ApplyConfiguration(new POSServiceMap());
            builder.ApplyConfiguration(new CNCMaterialMap());
            builder.ApplyConfiguration(new WorkTypeMap());
            builder.ApplyConfiguration(new CriteriaMap());
            builder.ApplyConfiguration(new POSMaterialMap());
            builder.ApplyConfiguration(new POSCNCMaterialMap());
            builder.ApplyConfiguration(new AttachedFileMap());
        }
    }
}