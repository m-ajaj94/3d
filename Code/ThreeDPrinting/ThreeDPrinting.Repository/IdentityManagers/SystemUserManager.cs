﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ThreeDPrinting.Model.Identity;

namespace ThreeDPrinting.Repository.IdentityManagers
{
    public class SystemUserManager:UserManager<SystemUser>, ISystemUserManager
    {
        public SystemUserManager(IUserStore<SystemUser> store, IOptions<IdentityOptions> optionsAccessor, IPasswordHasher<SystemUser> passwordHasher, IEnumerable<IUserValidator<SystemUser>> userValidators, IEnumerable<IPasswordValidator<SystemUser>> passwordValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, IServiceProvider services, ILogger<UserManager<SystemUser>> logger) 
            : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
        }

    }

    public interface ISystemUserManager
    {
        Task<IdentityResult> CreateAsync(SystemUser user, string password);
        Task<IdentityResult> UpdateAsync(SystemUser user);
        Task<SystemUser> FindByIdAsync(string userId);
        Task<SystemUser> FindByNameAsync(string userName);
        Task<SystemUser> GetUserAsync(ClaimsPrincipal principal);
        Task<IdentityResult> ResetPasswordAsync(SystemUser user, string token, string newPassword);
        Task<IdentityResult> ChangePasswordAsync(SystemUser user, string currentPassword, string newPassword);
        Task<IdentityResult> AddToRoleAsync(SystemUser user, string role);
        Task<IdentityResult> RemoveFromRoleAsync(SystemUser user, string role);
        Task<IdentityResult> ConfirmEmailAsync(SystemUser user, string token);
        Task<string> GenerateEmailConfirmationTokenAsync(SystemUser user);
        Task<bool> CheckPasswordAsync(SystemUser user, string password);
        Task<byte[]> CreateSecurityTokenAsync(SystemUser user);
        Task<bool> VerifyUserTokenAsync(SystemUser user, string tokenProvider, string purpose, string token);
        Task<IdentityResult> AddClaimsAsync(SystemUser user, IEnumerable<Claim> claims);
    }
}