﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using ThreeDPrinting.Model.Identity;

namespace ThreeDPrinting.Repository.IdentityManagers
{
    public class SystemRoleManager:RoleManager<SystemRole>, ISystemRoleManager
    {
        public SystemRoleManager(IRoleStore<SystemRole> store, IEnumerable<IRoleValidator<SystemRole>> roleValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, ILogger<RoleManager<SystemRole>> logger) : base(store, roleValidators, keyNormalizer, errors, logger)
        {
        }
    }

    public interface ISystemRoleManager
    {
        Task<IdentityResult> CreateAsync(SystemRole role);
        Task<IdentityResult> UpdateAsync(SystemRole role);
        Task<IdentityResult> DeleteAsync(SystemRole role);
    }
}