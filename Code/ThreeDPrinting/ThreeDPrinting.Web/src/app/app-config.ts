export interface AppConfig {
  apiEndpoint: string;
  identityEndpoint: string;
  title: string;
}
