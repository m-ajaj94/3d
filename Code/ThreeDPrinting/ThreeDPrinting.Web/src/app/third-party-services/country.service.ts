import { Injectable } from '@angular/core';
import { HttpServiceBase } from '../http-service-base';
import { Country } from './country';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';

@Injectable()
export class CountryService {
  getCountries(): Observable<Country[]> {
    return this.http.get(this.countryApiUri).map(x=>x.json()).catch(err=>err.json());
  }
  countryApiUri: string;
  constructor(private http: Http) {
    this.countryApiUri = 'https://restcountries.eu/rest/v2/all?fields=name;callingCodes';
  }
}
