import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';
import { Routes, RouterModule } from '@angular/router';
const routes: Routes = [
  {
    path: 'pageNotFound',
    component: PageNotFoundComponent
  },
  {
    path: '**',
    redirectTo: '/pageNotFound'
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule { }
