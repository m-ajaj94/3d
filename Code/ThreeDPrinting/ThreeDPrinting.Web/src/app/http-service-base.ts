import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

export class HttpServiceBase {
  protected getResponseData<T>(resp: HttpResponse<T>) {
    return resp;
  }

  protected handleError(err) {
    console.log(err);
    if (err instanceof HttpResponse) {
      console.log(err);
      const error = <HttpResponse<any>>err;
      const contentType = err.headers.get('Content-type');
      if (contentType == 'application/json') {
        Observable.throw(error.statusText + ': ' + error.body);
      } else if (contentType == 'application/text') {
        Observable.throw(error.statusText + ': ' + error.body);
      }
      return Observable.throw(error.statusText + ': ' + error.body);
    }
    return Observable.throw(err);
  }
}
