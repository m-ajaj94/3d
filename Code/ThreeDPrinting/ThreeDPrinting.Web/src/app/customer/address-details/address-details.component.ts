import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { CustomerAddress } from '../customer-address';
import 'rxjs/add/operator/switchMap';
@Component({
  templateUrl: './address-details.component.html',
  styleUrls: ['./address-details.component.css']
})
export class AddressDetailsComponent implements OnInit {

  address: CustomerAddress;
  constructor(private customerService:CustomerService,private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.address =new CustomerAddress();
    this.loadData();
  }

  loadData() {
    this.route.params
    .switchMap((params: Params) =>
      this.customerService.getCustomerAddress(params['customerId'], params['addressId']))
    .subscribe(address => this.address = address);
  }

}
