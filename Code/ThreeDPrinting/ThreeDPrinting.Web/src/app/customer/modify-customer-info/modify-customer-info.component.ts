import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { ModifyCustomerModel } from '../modify-customer-model';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-modify-customer-info',
  templateUrl: './modify-customer-info.component.html',
  styleUrls: ['./modify-customer-info.component.css']
})
export class ModifyCustomerInfoComponent implements OnInit {
  successMessage: string;
  model: ModifyCustomerModel;
  errorMessage: string;
  constructor(private router: Router, private route: ActivatedRoute,private customerService: CustomerService) { }

  ngOnInit() {
    this.model = new ModifyCustomerModel();
    this.customerService.getCustomerInfo().subscribe(x=>{
      console.log(x);
      this.model= new ModifyCustomerModel();
      this.model.customerId= x.cutomerId;
      this.model.firstName =x.firstName;
      this.model.lastName=x.lastName;
      this.model.email=x.email;
      this.model.photo = x.photo;
    });
  }

  submit() {
    this.customerService.modifyCustomerInfo(this.model).subscribe(result => {
      this.router.navigate(['..', {  }],
      { relativeTo: this.route });
    }, err => {
      this.errorMessage = err;
    });
  }
}
