import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyCustomerInfoComponent } from './modify-customer-info.component';

describe('ModifyCustomerInfoComponent', () => {
  let component: ModifyCustomerInfoComponent;
  let fixture: ComponentFixture<ModifyCustomerInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyCustomerInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyCustomerInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
