import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertModule, TypeaheadModule, ButtonsModule } from 'ngx-bootstrap';
import { ImageUploadModule } from 'angular2-image-upload';
import { DirectivesModule } from '../directives/directives.module';
import { ModifyCustomerInfoComponent } from './modify-customer-info/modify-customer-info.component';
import { CustomerComponent } from './customer/customer.component';
import { CustomerRoutingModule } from './customer-routing/customer-routing.module';
import { CustomerService } from './customer.service';
import { CustomerAddressesComponent } from './customer-addresses/customer-addresses.component';
import { NewAddressComponent } from './new-address/new-address.component';
import { AddressDetailsComponent } from './address-details/address-details.component';
import { EditAddressComponent } from './edit-address/edit-address.component';
import { PaymentMethodsComponent } from './payment-methods/payment-methods.component';
import { AddPaymentMethodComponent } from './add-payment-method/add-payment-method.component';
import { PaymentMethodDetailsComponent } from './payment-method-details/payment-method-details.component';
import { EditPaymentMethodComponent } from './edit-payment-method/edit-payment-method.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
@NgModule({
  imports: [
    AlertModule.forRoot(),
    TypeaheadModule.forRoot(),
    ButtonsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ImageUploadModule.forRoot(),
    CommonModule,
    DirectivesModule,
    CustomerRoutingModule,
    FormsModule
  ],
  declarations: [ModifyCustomerInfoComponent, CustomerComponent, CustomerAddressesComponent, NewAddressComponent, AddressDetailsComponent, EditAddressComponent, PaymentMethodsComponent, AddPaymentMethodComponent, PaymentMethodDetailsComponent, EditPaymentMethodComponent],
  providers: [CustomerService]
})
export class CustomerModule { }
