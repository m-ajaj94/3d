export class ModifyCustomerModel {
  email: string;
  customerId: string;
  firstName: string;
  lastName:string;
  photo: string;
}
