import { Component, OnInit } from '@angular/core';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { CustomerService } from '../customer.service';
import { CountryService } from '../../third-party-services/country.service';
import { AddAddressModel } from '../add-address-model';
import { Country } from '../../third-party-services/country';
import { EditAddressModel } from '../../customer/edit-address-model';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
  templateUrl: './edit-address.component.html',
  styleUrls: ['./edit-address.component.css']
})
export class EditAddressComponent implements OnInit {


  selectedCountry: Country;
  countries: Country[];
  successMessage: string;
  model: EditAddressModel;
  errorMessage: string;
  constructor(private customerService: CustomerService, private countryService: CountryService,private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.model = new EditAddressModel();
    this.getCountries();
    this.loadData();
  }

  loadData() {
    this.route.params
    .switchMap((params: Params) =>
      this.customerService.getCustomerAddress(params['customerId'], params['addressId']))
    .subscribe(address => this.model = address);
  }

  submit() {
    this.customerService.modifyCustomerAddress(this.model).subscribe(result => {
      this.router.navigate(['/customer/addresses']);
    }, err => {
      this.errorMessage = err;
    });
  }

  getCountries() {
    this.countryService
      .getCountries()
      .subscribe(
        countryArray => {
          this.countries = countryArray;
          this.selectedCountry = this.countries[0];
        }, err => {
          console.log(err);
          this.errorMessage = err;
        });
  }
  countryChanged(event: TypeaheadMatch) {
    this.model.countryCode = '+' + (<Country>event.item).callingCodes[0];
  }
}
