import { Component, OnInit } from '@angular/core';
import { AddAddressModel } from '../add-address-model';
import { CustomerService } from '../customer.service';
import { CountryService } from '../../third-party-services/country.service';
import { Country } from '../../third-party-services/country';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap';

@Component({
  selector: 'app-new-address',
  templateUrl: './new-address.component.html',
  styleUrls: ['./new-address.component.css']
})
export class NewAddressComponent implements OnInit {
  selectedCountry: Country;
  countries: Country[];
  successMessage: string;
  model: AddAddressModel;
  errorMessage: string;
  constructor(private customerService: CustomerService, private countryService: CountryService) { }

  ngOnInit() {
    this.model = new AddAddressModel();
    this.getCountries();
  }
  submit() {
    this.customerService.addCustomerAddress(this.model).subscribe(result => {
      this.successMessage = 'Address Added...';
    }, err => {
      this.errorMessage = err;
    });
  }

  getCountries() {
    this.countryService
      .getCountries()
      .subscribe(
        countryArray => {
          this.countries = countryArray;
          this.selectedCountry = this.countries[0];
        }, err => {
          console.log(err);
          this.errorMessage = err;
        });
  }
  countryChanged(event: TypeaheadMatch) {
    this.model.countryCode = '+' + event.item.callingCodes[0];
  }
}
