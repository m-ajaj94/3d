import { Injectable, Inject } from '@angular/core';
import { ModifyCustomerModel } from './modify-customer-model';
import { Http, Headers } from '@angular/http';
import { HttpServiceBase } from '../http-service-base';
import { Observable } from 'rxjs/Observable';
import { CustomerInfo } from './customer-info';
import { AppConfig } from '../app-config';
import { APP_CONFIG } from '../app.config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CustomerAddress } from './customer-address';
import { AddAddressModel } from './add-address-model';
import { DeleteAddressModel } from './delete-address-model';
import { EditAddressModel } from '../customer/edit-address-model';
import { PaymentMethodModel } from './payment-method-model';
import { AddPaymentModel } from './add-payment-model';
import { DeletePaymentModel } from './delete-payment-model';
import { EditPaymentModel } from './edit-payment-model';

@Injectable()
export class CustomerService extends HttpServiceBase {
  modifyPaymentMethod(model: EditPaymentModel): any {
    const headers = new HttpHeaders({ 'content-type': 'application/json' });
    return this.http.put(`${this.customerApiUrl}/editPaymentMethod`, model, {
      headers: headers
    }).catch(this.handleError);

  }
  deletePaymentMethod(model: DeletePaymentModel): Observable<any> {
    return this.http.delete(this.customerApiUrl + `/DeletePaymentMethod/${model.customerId}/${model.paymentMethodId}`).map(this.getResponseData).catch(this.handleError);
  }
  addPaymentMethod(model: AddPaymentModel): Observable<any> {
    const headers = new HttpHeaders({ 'content-type': 'application/json' });
    model.customerId = 'b410ab40-a3ee-4e4a-b28f-0475698fd727';
    return this.http.put(`${this.customerApiUrl}/addPaymentMethod`, model, {
      headers: headers
    }).catch(this.handleError);
  }
  getCustomerPaymentMethods(): Observable<PaymentMethodModel[]> {
    return this.http.get(this.customerApiUrl + '/getAllPaymentMethods?customerId=b410ab40-a3ee-4e4a-b28f-0475698fd727').map(this.getResponseData).catch(this.handleError);
  }
  getCustomerPaymentMethod(customerId: string ,paymentMethodId: string): Observable<PaymentMethodModel> {
    return this.http.get(this.customerApiUrl + `/getPaymentMethod?customerId=${customerId}&paymentMethodId=${paymentMethodId}`).map(this.getResponseData).catch(this.handleError);
  }
  modifyCustomerAddress(model: EditAddressModel): Observable<any>{
    const headers = new HttpHeaders({ 'content-type': 'application/json' });
    return this.http.put(`${this.customerApiUrl}/editAddress`, model, {
      headers: headers
    }).catch(this.handleError);
  }
  getCustomerAddress(customerId: string ,addressId: string): Observable<CustomerAddress> {
    return this.http.get(this.customerApiUrl + `/getCustomerAddress?customerId=${customerId}&addressId=${addressId}`).map(this.getResponseData).catch(this.handleError);
  }
  addCustomerAddress(model: AddAddressModel): Observable<any> {
    const headers = new HttpHeaders({ 'content-type': 'application/json' });
    model.customerId = 'b410ab40-a3ee-4e4a-b28f-0475698fd727';
    return this.http.put(`${this.customerApiUrl}/addAddress`, model, {
      headers: headers
    }).catch(this.handleError);
  }
  getCustomerAddresses(): Observable<CustomerAddress[]> {
    return this.http.get(this.customerApiUrl + '/getCustomerAddresses?customerId=b410ab40-a3ee-4e4a-b28f-0475698fd727').map(this.getResponseData).catch(this.handleError);
  }
  deleteCustomerAddress(model: DeleteAddressModel): Observable<any> {
    return this.http.delete(this.customerApiUrl + `/DeleteAddress/${model.customerId}/${model.addressId}`).map(this.getResponseData).catch(this.handleError);
  }
  private customerApiUrl: string;
  constructor(private http: HttpClient, @Inject(APP_CONFIG) config: AppConfig) {
    super();
    this.customerApiUrl = `${config.apiEndpoint}/api/customer`;
  }

  getCustomerInfo(): Observable<CustomerInfo> {
    return this.http.get(this.customerApiUrl + '/getCustomerInfo?customerId='+'b410ab40-a3ee-4e4a-b28f-0475698fd727').map(this.getResponseData).catch(this.handleError);
  }

  modifyCustomerInfo(model: ModifyCustomerModel): any {
    const headers = new HttpHeaders({ 'content-type': 'application/json' });
    return this.http.put(`${this.customerApiUrl}/modifyCustomer`, model, {
      headers: headers
    }).catch(this.handleError);
  }

}
