export class AddPaymentModel {
  customerId: string;
  cardNumber: string;
  name: string;
  expiryDate:Date;
  cardCompany: string;
}
