export class PaymentMethodModel {
  customerId: string;
  paymentMethodId: string;
  cardNumber: string;
  name: string;
  expiryDate:Date;
  cardCompany: string;
}
