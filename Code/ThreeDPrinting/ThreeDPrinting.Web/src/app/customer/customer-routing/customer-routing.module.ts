import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModifyCustomerInfoComponent } from '../modify-customer-info/modify-customer-info.component';
import { CustomerComponent } from '../customer/customer.component';
import { RouterModule } from '@angular/router';
import { CustomerAddressesComponent } from '../customer-addresses/customer-addresses.component';
import { NewAddressComponent } from '../new-address/new-address.component';
import { AddressDetailsComponent } from '../address-details/address-details.component';
import { EditAddressComponent } from '../edit-address/edit-address.component';
import { PaymentMethodsComponent } from '../payment-methods/payment-methods.component';
import { PaymentMethodDetailsComponent } from '../payment-method-details/payment-method-details.component';
import { EditPaymentMethodComponent } from '../edit-payment-method/edit-payment-method.component';
import { AddPaymentMethodComponent } from '../add-payment-method/add-payment-method.component';

const routes = [{
  path: 'customer',
  component: CustomerComponent,
  children: [{
    path: 'modify',
    component: ModifyCustomerInfoComponent
  },
  {
    path: 'addresses',
    component: CustomerAddressesComponent,
    children: [{
      path: ':customerId/:addressId',
      component: AddressDetailsComponent
    }]
  },
  {
    path: 'paymentMethods',
    component: PaymentMethodsComponent,
    children: [{
      path: ':customerId/:paymentMethodId',
      component: PaymentMethodDetailsComponent
    }]
  },
  {
    path: 'newAddress',
    component: NewAddressComponent
  },
  {
    path: 'addresses/edit/:customerId/:addressId',
    component: EditAddressComponent
  },
  {
    path: 'newPayment',
    component: AddPaymentMethodComponent
  },
  {
    path: 'paymentMethods/edit/:customerId/:paymentMethodId',
    component: EditPaymentMethodComponent
  }]
}];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }
