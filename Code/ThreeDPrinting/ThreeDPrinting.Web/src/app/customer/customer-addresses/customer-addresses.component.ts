import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { CustomerAddress } from '../customer-address';
import { DeleteAddressModel } from '../delete-address-model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  templateUrl: './customer-addresses.component.html',
  styleUrls: ['./customer-addresses.component.css']
})
export class CustomerAddressesComponent implements OnInit {


  selectedAddress: CustomerAddress;
  addresses: CustomerAddress[];
  errorMessage: string;
  constructor(private customerService: CustomerService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.loadData();

  }
  loadData() {
    this.customerService
      .getCustomerAddresses()
      .subscribe(
        addressArray => this.addresses = addressArray, err => {
          this.errorMessage = err;
        });
  }
  isSelected(addressId: string){
    return this.selectedAddress  && this.selectedAddress.addressId===addressId;
  }
  removeAddress(address: CustomerAddress) {
    this.customerService
      .deleteCustomerAddress({
        addressId: address.addressId,
        customerId: address.customerId
      })
      .subscribe(
        () => this.loadData(), err => {
          this.errorMessage = err;
        });
  }
  selectAddress(address: CustomerAddress) {
    this.selectedAddress = address;
    this.router.navigate(['.',this.selectedAddress.customerId, this.selectedAddress.addressId], {
      relativeTo: this.route
    });
  }
  editAddress(address:CustomerAddress){
    this.router.navigate(['./edit',address.customerId,address.addressId],{
      relativeTo: this.route
    })
  }
}
