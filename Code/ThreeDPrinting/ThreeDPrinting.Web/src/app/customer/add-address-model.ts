export class AddAddressModel {
  customerId: string;
  contactName: string;
  countryCode: string;
  mobile: string;
  streetAddress: string;
  streetAddressNumberPOBox: string;
  apartmentSuiteUnitBuildingFloor: string;
  country: string;
  stateProvinceCounty: string;
  city: string;
  zipPostalCode: string;
  securityAccessCode: string;
  isDefaultShipping: boolean;
  isDefaultBilling: boolean;

}
