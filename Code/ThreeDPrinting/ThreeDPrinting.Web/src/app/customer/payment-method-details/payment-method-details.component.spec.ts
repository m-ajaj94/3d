import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentMethodDetailsComponent } from './payment-method-details.component';

describe('PaymentMethodDetailsComponent', () => {
  let component: PaymentMethodDetailsComponent;
  let fixture: ComponentFixture<PaymentMethodDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentMethodDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentMethodDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
