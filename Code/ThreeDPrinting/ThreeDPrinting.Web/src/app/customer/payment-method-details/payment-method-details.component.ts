import { Component, OnInit } from '@angular/core';
import { PaymentMethodModel } from '../payment-method-model';
import { Params, Router, ActivatedRoute } from '@angular/router';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-payment-method-details',
  templateUrl: './payment-method-details.component.html',
  styleUrls: ['./payment-method-details.component.css']
})
export class PaymentMethodDetailsComponent implements OnInit {
  paymentMethod: PaymentMethodModel;
  constructor(private customerService:CustomerService,private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.paymentMethod =new PaymentMethodModel();
    this.loadData();
  }
  loadData(){
    this.route.params
    .switchMap((params: Params) =>
      this.customerService.getCustomerPaymentMethod(params['customerId'], params['paymentMethodId']))
    .subscribe(paymentMethod=> this.paymentMethod =paymentMethod);
  }

}
