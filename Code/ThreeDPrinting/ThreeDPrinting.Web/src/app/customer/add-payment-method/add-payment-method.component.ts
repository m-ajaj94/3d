import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { AddPaymentModel } from '../add-payment-model';
import {  Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-payment-method',
  templateUrl: './add-payment-method.component.html',
  styleUrls: ['./add-payment-method.component.css']
})
export class AddPaymentMethodComponent implements OnInit {
  successMessage: string;
  model: AddPaymentModel;
  errorMessage: string;
  constructor(private customerService: CustomerService,private router:Router,private route :ActivatedRoute) { }

  ngOnInit() {
    this.model = new AddPaymentModel();
  }

  submit() {
    this.errorMessage = undefined;
    this.customerService.addPaymentMethod(this.model).subscribe(result => {
      this.successMessage = 'Payment Method Added...';
      this.router.navigate(['../paymentMethods'],{relativeTo:this.route});
    }, err => {
      this.errorMessage = err;
    });
  }


}
