import { Component, OnInit } from '@angular/core';
import { EditPaymentModel } from '../edit-payment-model';
import { CustomerService } from '../customer.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-edit-payment-method',
  templateUrl: './edit-payment-method.component.html',
  styleUrls: ['./edit-payment-method.component.css']
})
export class EditPaymentMethodComponent implements OnInit {
  successMessage: string;
  model: EditPaymentModel;
  errorMessage: string;
  constructor(private customerService: CustomerService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.model = new EditPaymentModel();
    this.loadData();
  }

  loadData() {
    this.route.params
    .switchMap((params: Params) =>
      this.customerService.getCustomerPaymentMethod(params['customerId'], params['paymentMethodId']))
    .subscribe(paymentMethod => this.model = paymentMethod);
  }

  submit() {
    this.customerService.modifyPaymentMethod(this.model).subscribe(result => {
      this.router.navigate(['/customer/paymentMethods']);
    }, err => {
      this.errorMessage = err;
    });
  }
}
