import { Component, OnInit } from '@angular/core';
import { PaymentMethodModel } from '../payment-method-model';
import { CustomerService } from '../customer.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-payment-methods',
  templateUrl: './payment-methods.component.html',
  styleUrls: ['./payment-methods.component.css']
})
export class PaymentMethodsComponent implements OnInit {

  paymentMethods: PaymentMethodModel[];
  selectedPaymentMethod: PaymentMethodModel;
  errorMessage: string;
  constructor(private customerService: CustomerService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.loadData();

  }
  loadData() {
    this.errorMessage = undefined;
    this.customerService
      .getCustomerPaymentMethods()
      .subscribe(
        paymentsArray => this.paymentMethods = paymentsArray, err => {
          this.errorMessage = err;
        });
  }
  isSelected(paymentMethodId: string) {
    return this.selectedPaymentMethod && this.selectedPaymentMethod.paymentMethodId === paymentMethodId;
  }
  removePaymentMethod(paymentMethod: PaymentMethodModel) {
    this.customerService
      .deletePaymentMethod({
        paymentMethodId: paymentMethod.paymentMethodId,
        customerId: paymentMethod.customerId
      })
      .subscribe(
        () => this.loadData(), err => {
          this.errorMessage = err;
        });
  }
  selectPaymentMethod(paymentMethod: PaymentMethodModel) {
    this.selectedPaymentMethod = paymentMethod;
    this.router.navigate(['.', this.selectedPaymentMethod.customerId, this.selectedPaymentMethod.paymentMethodId], {
      relativeTo: this.route
    });
  }
  editPaymentMethod(paymentMethod: PaymentMethodModel) {
    this.router.navigate(['./edit', paymentMethod.customerId, paymentMethod.paymentMethodId], {
      relativeTo: this.route
    })
  }
}
