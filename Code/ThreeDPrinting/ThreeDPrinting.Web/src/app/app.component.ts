import { Component } from '@angular/core';
import { AuthenticationService } from './authentication/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  username: string;
  year: number;
  constructor(authenticationService: AuthenticationService) {
    authenticationService.fillAuthenticationData();
    this.year = new Date().getFullYear();
    if (authenticationService.authenticationData)
      this.username = authenticationService.authenticationData.username;
  }

}
