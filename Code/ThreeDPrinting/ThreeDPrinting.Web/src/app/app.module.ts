import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AuthenticationModule } from './authentication/authentication.module';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { AuthenticationService } from './authentication/authentication.service';
import { UserModule } from './user/user.module';
import { AlertModule, TypeaheadModule } from 'ngx-bootstrap';
import { DirectivesModule } from './directives/directives.module';
import { CustomerModule } from './customer/customer.module';
import { APP_DI_CONFIG, APP_CONFIG } from './app.config';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { TokenInterceptor } from './token-interceptor';
import { ThirdPartyServicesModule } from './third-party-services/third-party-services.module';
import { CountryService } from './third-party-services/country.service';
import { AdminModule } from './admin/admin.module';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
  ],
  imports: [
    AlertModule.forRoot(),
    TypeaheadModule.forRoot(),
    BrowserModule,
    HttpClientModule,
    AuthenticationModule,
    DirectivesModule,
    UserModule,
    CustomerModule,
    AdminModule,
    AppRoutingModule
  ],
  providers: [{ provide: APP_CONFIG, useValue: APP_DI_CONFIG },{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  },CountryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
