import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './register/register.component';
import { UserRoutingModule } from './user-routing/user-routing.module';
import { UserComponent } from './user/user.component';
import {AlertModule} from 'ngx-bootstrap';
import { ImageUploadModule } from 'angular2-image-upload';
import { UserService } from './user.service';
import { DirectivesModule } from '../directives/directives.module';

@NgModule({
  imports: [
    AlertModule.forRoot(),
    ImageUploadModule.forRoot(),
    CommonModule,
    DirectivesModule,
    UserRoutingModule,
    FormsModule
  ],
  declarations: [RegisterComponent, UserComponent],
  providers: [UserService]
})
export class UserModule { }
