import { Injectable, Inject } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { NewUserModel } from './new-user-model';
import { HttpServiceBase } from '../http-service-base';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { AppConfig } from '../app-config';
import { APP_CONFIG } from '../app.config';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class UserService extends HttpServiceBase {
  private userApiUrl: string;
  constructor(private http: HttpClient,@Inject(APP_CONFIG) config: AppConfig) {
    super();
    this.userApiUrl = `${config.apiEndpoint}/api/customer`;
  }

  register(model: NewUserModel): Observable<string> {
    const headers = new HttpHeaders({ 'content-type': 'application/json' });
    return this.http.post(`${this.userApiUrl}/registerCustomer`, model, {
      headers: headers
    }).map(this.getResponseData).catch(this.handleError);
  }
}
