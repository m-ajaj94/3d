import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from '../register/register.component';
import { UserComponent } from '../user/user.component';

const routes = [{
  path: 'user',
  component: UserComponent,
  children: [{
    path: 'register',
    component: RegisterComponent
  }]
}];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class UserRoutingModule { }
