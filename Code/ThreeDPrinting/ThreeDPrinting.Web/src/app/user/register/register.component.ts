import { Component, OnInit } from '@angular/core';
import { NewUserModel } from '../new-user-model';
import { UserService } from '../user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  successMessage: string;
  model: NewUserModel;
  errorMessage: string;
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.model = new NewUserModel();
  }

  submit() {
    this.userService.register(this.model).subscribe(result => {
      this.successMessage = 'Thanks for registering at our website. You will receive a verification'
        + ' email soon to proceed with registration.';
    }, err => {
      this.errorMessage = err;
    });
  }
}
