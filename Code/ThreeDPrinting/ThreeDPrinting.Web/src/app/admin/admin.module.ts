import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ColorListComponent } from './color-list/color-list.component';
import { AdminComponent } from './admin/admin.component';
import { AdminRoutingModule } from './admin-routing/admin-routing.module';
import { FormsModule } from '@angular/forms';
import { DirectivesModule } from '../directives/directives.module';
import { ColorService } from './color.service';
import { EditColorComponent } from './edit-color/edit-color.component';
import { AddColorComponent } from './add-color/add-color.component';
import { AlertModule } from 'ngx-bootstrap';
import { ColorPickerModule } from 'ngx-color-picker';

@NgModule({
  imports: [
    AlertModule.forRoot(),
    CommonModule,
    DirectivesModule,
    AdminRoutingModule,
    FormsModule,
    ColorPickerModule
  ],
  declarations: [ColorListComponent, AdminComponent, EditColorComponent, AddColorComponent],
  providers: [ColorService]
})
export class AdminModule { }
