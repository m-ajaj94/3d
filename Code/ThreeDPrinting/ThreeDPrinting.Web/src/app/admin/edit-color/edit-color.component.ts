import { Component, OnInit } from '@angular/core';
import { ColorService } from '../color.service';
import { EditColorModel } from '../edit-color-model';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ColorHelpers } from '../../helpers/color-helpers';

@Component({
  selector: 'app-edit-color',
  templateUrl: './edit-color.component.html',
  styleUrls: ['./edit-color.component.css']
})
export class EditColorComponent implements OnInit {

  successMessage: string;
  model: EditColorModel;
  errorMessage: string;
  color: string;
  constructor(private colorService: ColorService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.model = new EditColorModel();
    this.loadData();
  }

  loadData() {
    this.route.params
      .switchMap((params: Params) =>
        this.colorService.getColor(params['colorId']))
      .subscribe(colorModel => {
        this.model = colorModel;
        this.color = ColorHelpers.rgbToHex(this.model.r, this.model.g, this.model.b);
      });
  }
  submit() {
    var rgb = ColorHelpers.hexToRgb(this.color);
    this.model.r = rgb.r;
    this.model.g = rgb.g;
    this.model.b = rgb.b;
    this.colorService.editColor(this.model).subscribe(result => {
      this.router.navigate(['/admin/colors']);
    }, err => {
      this.errorMessage = err;
    });
  }
}
