export class EditColorModel {
  colorId: string;
  colorName: string;
  r: number;
  g: number;
  b: number;
}
