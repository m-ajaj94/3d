import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ColorListComponent } from '../color-list/color-list.component';
import { AdminComponent } from '../admin/admin.component';
import { AddColorComponent } from '../add-color/add-color.component';
import { EditColorComponent } from '../edit-color/edit-color.component';
const routes = [{
  path: 'admin',
  component: AdminComponent,
  children: [{
    path: 'colors',
    component: ColorListComponent
  }, {
    path: 'newColor',
    component: AddColorComponent
  },
  {
    path: 'colors/edit/:colorId',
    component: EditColorComponent
  },],


}]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
