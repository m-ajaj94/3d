import { Injectable, Inject } from '@angular/core';
import { HttpServiceBase } from '../http-service-base';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../app.config';
import { Observable } from 'rxjs/Observable';
import { ColorModel } from './color-model';
import { AddColorModel } from './add-color-model';
import { EditColorModel } from './edit-color-model';

@Injectable()
export class ColorService extends HttpServiceBase {
  private colorApiUrl: string;
  constructor(private http: HttpClient, @Inject(APP_CONFIG) config: AppConfig) {
    super();
    this.colorApiUrl = `${config.apiEndpoint}/api/color`;
  }

  getAllColors(): Observable<ColorModel[]> {
    return this.http.get(this.colorApiUrl + '/getAllColors').map(this.getResponseData).catch(this.handleError);
  }

  getColor(colorId: string): Observable<ColorModel> {
    return this.http.get(this.colorApiUrl + '/getColor', {
      params: {
        colorId: colorId
      }
    }).map(this.getResponseData).catch(this.handleError);
  }

  addColor(model: AddColorModel): Observable<ColorModel> {
    const headers = new HttpHeaders({ 'content-type': 'application/json' });
    return this.http.post(this.colorApiUrl + '/addColor', model, {
      headers: headers
    }).map(this.getResponseData).catch(this.handleError);
  }

  editColor(model: EditColorModel): Observable<ColorModel> {
    const headers = new HttpHeaders({ 'content-type': 'application/json' });
    return this.http.put(this.colorApiUrl + '/editColor', model, {
      headers: headers
    }).map(this.getResponseData).catch(this.handleError);
  }

  deleteColor(colorId: string): Observable<any> {
   return this.http.delete(`${this.colorApiUrl}/deleteColor`, {
      params: {
        colorId: colorId
      }
    })
  }
}
