export class ColorModel {
  colorId: string;
  colorName: string;
  r: number;
  g: number;
  b: number;
}
