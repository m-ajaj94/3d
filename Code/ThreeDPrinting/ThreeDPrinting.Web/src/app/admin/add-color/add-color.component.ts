import { Component, OnInit } from '@angular/core';
import { ColorService } from '../color.service';
import { AddColorModel } from '../add-color-model';
import { Router, ActivatedRoute } from '@angular/router';
import { setTimeout } from 'timers';
import { ColorHelpers } from '../../helpers/color-helpers';

@Component({
  templateUrl: './add-color.component.html',
  styleUrls: ['./add-color.component.css']
})
export class AddColorComponent implements OnInit {

  successMessage: string;
  model: AddColorModel;
  errorMessage: string;
  constructor(private colorService: ColorService, private router: Router, private route: ActivatedRoute) { }
  color: string;
  ngOnInit() {
    this.color = '#ffffff';
    this.model = new AddColorModel();
  }
  submit() {
    var rgb= ColorHelpers.hexToRgb(this.color);
    this.model.r =rgb.r ;
    this.model.g =rgb.g;
    this.model.b = rgb.b;
    this.errorMessage = undefined;
    this.colorService.addColor(this.model).subscribe(result => {
      this.successMessage = 'Color added...';
      this.router.navigate(['../colors'], { relativeTo: this.route })
    }, err => {
      this.errorMessage = err;
    });
  }
}
