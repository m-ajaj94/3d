import { Component, OnInit } from '@angular/core';
import { ColorModel } from '../color-model';
import { ColorService } from '../color.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ColorHelpers } from '../../helpers/color-helpers';

@Component({
  templateUrl: './color-list.component.html',
  styleUrls: ['./color-list.component.css']
})
export class ColorListComponent implements OnInit {
  selectedColor: ColorModel;
  colors: ColorModel[];
  errorMessage: string;
  constructor(private colorService: ColorService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.loadData();
  }
  loadData() {
    this.colorService
      .getAllColors()
      .subscribe(
        colorsArray => this.colors = colorsArray, err => {
          this.errorMessage = err;
        });
  }
  isSelected(colorId: string) {
    return this.selectedColor && this.selectedColor.colorId === colorId;
  }
  removeColor(color: ColorModel) {
    this.colorService
      .deleteColor(
        color.colorId
      )
      .subscribe(
        () => this.loadData(), err => {
          this.errorMessage = err;
        });
  }
  selectColor(color: ColorModel) {
    this.selectedColor = color;
  }
  editColor(color: ColorModel) {
    this.router.navigate(['./edit', color.colorId], {
      relativeTo: this.route
    })
  }
  getHex(color: ColorModel): string {
    return ColorHelpers.rgbToHex(color.r, color.g, color.b);
  }
}
