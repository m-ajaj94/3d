export class AddColorModel {
  colorName: string;
  r: number;
  g: number;
  b: number;
}
