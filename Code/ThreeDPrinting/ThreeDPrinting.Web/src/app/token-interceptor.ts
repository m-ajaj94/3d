import { Injectable, Injector } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from './authentication/authentication.service';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private injector: Injector) { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    var auth = this.injector.get(AuthenticationService);
    console.log(auth.authenticationData);
    if (auth.authenticationData)
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${auth.authenticationData.accessToken}`
        }
      });
    return next.handle(request);
  }
}
