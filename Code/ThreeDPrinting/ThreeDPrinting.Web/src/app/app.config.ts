import { AppConfig } from './app-config';
export { AppConfig } from './app-config';

import { InjectionToken } from '@angular/core';

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

export const APP_DI_CONFIG: AppConfig = {
  apiEndpoint: 'https://threedprintingapi20180307121324.azurewebsites.net',
  identityEndpoint: 'http://threedprintingidentityserver20180307011837.azurewebsites.net/',
  title: 'Dependency Injection'
};
