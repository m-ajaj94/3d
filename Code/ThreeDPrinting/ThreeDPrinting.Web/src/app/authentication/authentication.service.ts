import { Injectable, Inject } from '@angular/core';
import { AuthenticationData } from './authentication-data';
import { identifierModuleUrl } from '@angular/compiler';
import { HttpServiceBase } from '../http-service-base';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { APP_CONFIG } from '../app.config';
import { AppConfig } from '../app-config';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { AuthenticationResult } from './authentication-result';

@Injectable()
export class AuthenticationService extends HttpServiceBase {
  authenticationData: AuthenticationData;
  identityUri: string;
  private saveToLocalStorage() {
    localStorage.setItem('authenticationData', JSON.stringify(this.authenticationData));
  }

  constructor(private http: HttpClient, @Inject(APP_CONFIG) config: AppConfig) {
    super();
    this.identityUri = config.identityEndpoint;
  }

  login(username: string, password: string): Observable<any> {
    const headers = new HttpHeaders({ 'content-type': 'application/x-www-form-urlencoded' });
    var data = `client_id=TestClient&username=${username}&password=${password}&scope=openid WebAPI roles profile&grant_type=password`;
    return this.http.post(`${this.identityUri}/connect/token`, data, { headers: headers }).map((response:any) => {
      var result = response;
      console.log(response.access_token);
      this.authenticationData = new AuthenticationData();
      this.authenticationData.accessToken = result.access_token;
      this.saveToLocalStorage();
      console.log(result);
    }).catch(this.handleError);
  }
  getProfileInfo() {
    const headers = new HttpHeaders({ 'content-type': 'application/x-www-form-urlencoded', 'Authorization': 'Bearer ' + this.authenticationData.accessToken });
    return this.http.get(`${this.identityUri}/connect/userinfo`, { headers: headers }).map((response: any) => {
      var result = response;
      this.authenticationData.username = result.preferred_username;
      if (result.roles instanceof Array)
        this.authenticationData.roles = result.role;
      else
        this.authenticationData.roles = [result.role];
      this.saveToLocalStorage();
    }).catch(this.handleError);
  }

  fillAuthenticationData() {
    const storedValue = localStorage.getItem('authenticationData');
    if (storedValue) {
      this.authenticationData = JSON.parse(storedValue);
    }
  }

  logout() {
    this.authenticationData = undefined;
    localStorage.removeItem('authenticationData');
  }
}
