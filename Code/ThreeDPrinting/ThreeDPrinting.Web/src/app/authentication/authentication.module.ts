import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import {AlertModule} from 'ngx-bootstrap';
import { DirectivesModule } from '../directives/directives.module';
import { LoginComponent } from './login/login.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticationService } from './authentication.service';

const routes: Routes = [{
  path: 'login',
  component: LoginComponent
}];
@NgModule({
  imports: [
    AlertModule.forRoot(),
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule
  ],
  declarations: [LoginComponent],
  exports: [RouterModule],
  providers: [AuthenticationService]
})
export class AuthenticationModule { }
