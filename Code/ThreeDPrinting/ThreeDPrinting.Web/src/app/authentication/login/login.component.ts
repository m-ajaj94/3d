import { Component, OnInit } from '@angular/core';
import {LoginModel} from '../login-model'
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private errorMessage: string;
  private model : LoginModel;
  constructor(private authenticationService: AuthenticationService,private router : Router) { }

  ngOnInit() {
    this.model = {
      username: '',
      password:''
    };
  }

  login() {
    this.authenticationService.login(this.model.username,this.model.password).subscribe(result => {
      this.authenticationService.getProfileInfo().subscribe(result=>
        {
          this.router.navigate(['/customer', {  }])});
    }, err => {
      this.errorMessage = err;
    });
  }


}
