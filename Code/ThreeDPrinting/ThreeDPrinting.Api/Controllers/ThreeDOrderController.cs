﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using ThreeDPrinting.Api.Models;
using ThreeDPrinting.Model.Exceptions;
using ThreeDPrinting.Repository.Helpers;
using ThreeDPrinting.Repository.Repositories;

namespace ThreeDPrinting.Api.Controllers
{
    [Route("api/ThreeDOrder")]
    public class ThreeDOrderController : Controller
    {
        private readonly IThreeDOrderRepository _threeDOrderRepository;

        public ThreeDOrderController(IThreeDOrderRepository threeDOrderRepository)
        {
            _threeDOrderRepository = threeDOrderRepository;
        }

        [HttpPost]
        [Route("CreateThreeDOrder")]
        [DisableFormValueModelBinding]
        public async Task<IActionResult> CreateThreeDOrder()
        {
            try
            {
                FormValueProvider formModel;
                byte[] fileData;
                using (var memorySteam = new MemoryStream())
                {
                    formModel = await Request.StreamFile(memorySteam);
                    fileData = memorySteam.ToArray();
                }
                var threeDOrderModel = new ThreeDOrderModel();

                var bindingSuccessful = await TryUpdateModelAsync(threeDOrderModel, prefix: "",
                    valueProvider: formModel);
                if (!bindingSuccessful)
                {
                    if (!ModelState.IsValid)
                    {
                        return BadRequest(ModelState);
                    }
                }
                await _threeDOrderRepository.CreateOrder(threeDOrderModel.CustomerId, threeDOrderModel.ServiceId,
                    threeDOrderModel.MaterialId, threeDOrderModel.ColorsId, fileData,
                    threeDOrderModel.OrderNote);
                return Ok();

            }
            catch (CustomerNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (ServiceNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (MaterialNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

    }
}