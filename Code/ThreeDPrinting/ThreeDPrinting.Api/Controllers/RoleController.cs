﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ThreeDPrinting.Model.Identity;
using ThreeDPrinting.Repository.IdentityManagers;

namespace ThreeDPrinting.Api.Controllers
{
    [Route("api/role")]
    public class RoleController : Controller
    {
        private readonly ISystemRoleManager _roleManager;

        public RoleController(ISystemRoleManager roleManager)
        {
            _roleManager = roleManager;
        }

        [HttpPost]
        [Route("init")]
        public async Task<IActionResult> Init()
        {
            var result = await _roleManager.CreateAsync(new SystemRole()
            {
                Id = Guid.NewGuid(),
                Name = "Customer"
            });
            if (result.Succeeded)
                return Ok();
            return BadRequest(result.Errors);
        }
    }
}