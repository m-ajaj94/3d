﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ThreeDPrinting.Api.Models;
using ThreeDPrinting.Model.AggregateRoot;
using ThreeDPrinting.Model.Exceptions;
using ThreeDPrinting.Repository.Repositories;

namespace ThreeDPrinting.Api.Controllers
{
    [Route("api/Material")]
    public class MaterialController : Controller
    {
        private readonly IMaterialRepository _materialRepository;

        public MaterialController(IMaterialRepository materialRepository)
        {
            _materialRepository = materialRepository;
        }

        [HttpPost]
        [Route("AddMaterial")]
        public async Task<IActionResult> AddMaterial([FromBody] AddMaterialModel addMaterialModel)
        {
            try
            {
                var material = new Material(addMaterialModel.MaterialName, addMaterialModel.Currency, addMaterialModel.PricePerGram);
                await _materialRepository.AddMaterial(material,addMaterialModel.Colors);
                return Ok();
            }
            catch (MaterialAlreadyExistException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [Route("EditMaterial")]
        [HttpPut]
        public async Task<IActionResult> EditMaterial([FromBody] EditMaterialModel editMaterialModel)
        {
            try
            {
                await _materialRepository.EditMaterial(editMaterialModel.MaterialId, editMaterialModel.MaterialName,
                    editMaterialModel.Currency, editMaterialModel.PricePerGram,editMaterialModel.Colors);
                return Ok();
            }
            catch (MaterialNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpDelete]
        [Route("DeleteMaterial")]
        public async Task<IActionResult> DeleteMaterial(Guid materialId)
        {
            try
            {
                await _materialRepository.DeleteMaterial(materialId);
                return Ok();
            }
            catch (MaterialNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }
        [Route("GetMaterial")]
        [HttpGet]
        public async Task<IActionResult> GetMaterial(Guid materialId)
        {
            try
            {
                var material = await _materialRepository.GetMaterial(materialId);
                return Ok(material);
            }
            catch (MaterialNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }
        [Route("GetAllMaterials")]
        [HttpGet]
        public async Task<IActionResult> GetAllMaterials()
        {
            try
            {
                var materials = await _materialRepository.GetAllMaterials();
                return Ok(materials);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}