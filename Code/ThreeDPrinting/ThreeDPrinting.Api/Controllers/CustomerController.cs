using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ThreeDPrinting.Api.Models;
using ThreeDPrinting.Model.DTO;
using ThreeDPrinting.Model.Entities;
using ThreeDPrinting.Model.Enums;
using ThreeDPrinting.Model.Exceptions;
using ThreeDPrinting.Model.Identity;
using ThreeDPrinting.Repository.IdentityManagers;
using ThreeDPrinting.Repository.Repositories;

namespace ThreeDPrinting.Api.Controllers
{
    [Route("api/Customer")]
    public class CustomerController : Controller
    {
        private readonly ISystemUserManager _systemUserManager;
        private readonly ICustomerRepository _customerRepository;

        public CustomerController(ISystemUserManager systemUserManager, ICustomerRepository customerRepository)
        {
            _systemUserManager = systemUserManager;
            _customerRepository = customerRepository;
        }

        [Route("RegisterCustomer")]
        [HttpPost]
        public async Task<IActionResult> RegisterCustomer([FromBody] AddCustomerModel customerModel)
        {
            try
            {
                var user = new SystemUser()
                {
                    Id = Guid.NewGuid(),
                    UserName = customerModel.UserName,
                    FirstName = customerModel.FirstName,
                    LastName = customerModel.LastName,
                    Email = customerModel.Email,
                    NormalizedEmail = customerModel.Email.ToUpper(),
                    NormalizedUserName = customerModel.UserName.ToUpper(),
                    Photo = customerModel.Photo,
                    UserType = UserType.Customer,
                    UserRoles = new List<UserRole>()
                };
                var result1 = await _systemUserManager.CreateAsync(user, customerModel.Password);
                if (!result1.Succeeded)
                    return BadRequest(result1.Errors.Select(x => x.Description).ToList());
                result1 = await _systemUserManager.AddToRoleAsync(user, "Customer");
                if (!result1.Succeeded)
                    return BadRequest(result1.Errors.Select(x => x.Description).ToList());
                await _customerRepository.AddCustomer(user);
                return Ok();
            }
            catch (CustomerAlreadyExistException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [Route("ModifyCustomer")]
        [HttpPut]
        public async Task<IActionResult> ModifyCustomer([FromBody] ModifyCustomerModel customerModel)
        {
            try
            {
                var userId = await _customerRepository.GetUserId(customerModel.CustomerId);
                var user = await _systemUserManager.FindByIdAsync(userId.ToString());
                user.FirstName = customerModel.FirstName;
                user.LastName = customerModel.LastName;
                user.Email = customerModel.Email;
                user.Photo = customerModel.Photo;
                var result = await _systemUserManager.UpdateAsync(user);
                if (!result.Succeeded)
                    return BadRequest(result.Errors.Select(x => x.Description).ToList());
                return Ok();
            }
            catch (CustomerNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [Route("GetCustomerInfo")]
        [HttpGet]
        public async Task<IActionResult> GetCustomerInfo(Guid customerId)
        {
            try
            {
                var userId = await _customerRepository.GetUserId(customerId);
                var user = await _systemUserManager.FindByIdAsync(userId.ToString());
                var customerDTO = new CustomerDTO()
                {
                    CutomerId = customerId,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Email = user.Email,
                    Photo = user.Photo
                };
                return Ok(customerDTO);
            }
            catch (CustomerNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }
        [Route("VerifyCustomerEmail")]
        [HttpPut]
        public async Task<IActionResult> VerifyCustomerEmail(
            [FromBody] VerifyCustomerEmailModel verifyCustomerEmailModel)
        {
            try
            {
                var userId = await _customerRepository.GetUserId(verifyCustomerEmailModel.CustomerId);
                var user = await _systemUserManager.FindByIdAsync(userId.ToString());
                if (user == null)
                    return NotFound("Customer Not Found!!");
                var result = await _systemUserManager.ConfirmEmailAsync(user, verifyCustomerEmailModel.Token);
                if (!result.Succeeded)
                    return BadRequest(result.Errors.Select(x => x.Description).ToList());
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [Route("AddAddress")]
        [HttpPut]
        public async Task<IActionResult> AddAddress([FromBody] AddAddressModel addAddressModel)
        {
            try
            {
                var address = new Address(addAddressModel.ContactName, addAddressModel.CountryCode,
                    addAddressModel.Mobile, addAddressModel.StreetAddress, addAddressModel.StreetAddressNumberPOBox,
                    addAddressModel.ApartmentSuiteUnitBuildingFloor, addAddressModel.Country,
                    addAddressModel.StateProvinceCounty, addAddressModel.City, addAddressModel.ZipPostalCode,
                    addAddressModel.SecurityAccessCode, addAddressModel.IsDefaultShipping,addAddressModel.IsDefaultBilling, addAddressModel.CustomerId);
                await _customerRepository.AddAddress(address);
                return Ok();
            }
            catch (CustomerNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpPut]
        [Route("EditAddress")]
        public async Task<IActionResult> EditAddress([FromBody] EditAddressModel editAddressModel)
        {
            try
            {
                await _customerRepository.EditAddress(editAddressModel.CustomerId, editAddressModel.AddressId,
                    editAddressModel.ApartmentSuiteUnitBuildingFloor, editAddressModel.City,
                    editAddressModel.ContactName, editAddressModel.Country, editAddressModel.CountryCode,
                    editAddressModel.IsDefaultShipping,
                    editAddressModel.Mobile, editAddressModel.SecurityAccessCode, editAddressModel.StateProvinceCounty,
                    editAddressModel.StreetAddress,
                    editAddressModel.StreetAddressNumberPOBox, editAddressModel.ZipPostalCode, editAddressModel.IsDefaultBilling);
                return Ok();
            }
            catch (AddressNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpDelete]
        [Route("DeleteAddress/{customerId}/{addressId}")]
        public async Task<IActionResult> DeleteAddress(Guid customerId,Guid addressId)
        {
            try
            {
                await _customerRepository.DeleteAddress(customerId, addressId);
                return Ok();
            }
            catch (AddressNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [Route("GetCustomerAddresses")]
        [HttpGet]
        public async Task<IActionResult> GetCustomerAddresses(Guid customerId)
        {
            try
            {
                var addresses = await _customerRepository.GetCustomerAddresses(customerId);
                return Ok(addresses);
            }
            catch (CustomerNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }
        [Route("GetCustomerAddress")]
        [HttpGet]
        public async Task<IActionResult> GetCustomerAddress(Guid customerId, Guid addressId)
        {
            try
            {
                var addresses = await _customerRepository.GetCustomerAddress(customerId, addressId);
                return Ok(addresses);
            }
            catch (CustomerNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [Route("AddPaymentMethod")]
        [HttpPut]
        public async Task<IActionResult> AddPaymentMethod([FromBody] AddPaymentMethodModel addPaymentMethodModel)
        {
            try
            {
                var paymentMethod = new PaymentMethod(addPaymentMethodModel.CardNumber, addPaymentMethodModel.Name, addPaymentMethodModel.ExpiryDate, addPaymentMethodModel.CardCompany, addPaymentMethodModel.CustomerId);
                await _customerRepository.AddPaymentMethod(paymentMethod);
                return Ok();
            }
            catch (CustomerNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [Route("GetPaymentMethod")]
        [HttpGet]
        public async Task<IActionResult> GetPaymentMethod(Guid customerId, Guid paymentMethodId)
        {
            try
            {
                var paymentMethod = await _customerRepository.GetPaymentMethod(customerId,
                    paymentMethodId);
                return Ok(paymentMethod);
            }
            catch (CustomerNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }
        [Route("GetAllPaymentMethods")]
        [HttpGet]
        public async Task<IActionResult> GetAllPaymentMethods(Guid customerId)
        {
            try
            {
                var paymentMethods = await _customerRepository.GetAllPaymentMethods(customerId);
                return Ok(paymentMethods);
            }
            catch (CustomerNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpDelete]
        [Route("DeletePaymentMethod/{customerId}/{paymentMethodId}")]
        public async Task<IActionResult> DeletePaymentMethod(Guid customerId,Guid paymentMethodId)
        {
            try
            {
                await _customerRepository.DeletePaymentMethod(customerId, paymentMethodId);
                return Ok();
            }
            catch (CustomerNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (PaymentMethodNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [Route("EditPaymentMethod")]
        [HttpPut]
        public async Task<IActionResult> EditPaymentMethod([FromBody] EditPaymentMethodModel editPaymentMethodModel)
        {
            try
            {
                await _customerRepository.EditPaymentMethod(editPaymentMethodModel.CustomerId,
                    editPaymentMethodModel.PaymentMethodId, editPaymentMethodModel.Name,
                    editPaymentMethodModel.CardNumber, editPaymentMethodModel.CardCompany,
                    editPaymentMethodModel.ExpiryDate);
                return Ok();
            }
            catch (CustomerNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (PaymentMethodNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }

        }


    }
}