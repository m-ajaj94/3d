﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ThreeDPrinting.Model.Exceptions;
using ThreeDPrinting.Repository.IdentityManagers;

namespace ThreeDPrinting.Api.Controllers
{
    [Route("api/User")]
    public class UserController:Controller
    {
        private readonly ISystemUserManager _systemUserManager;

        public UserController(ISystemUserManager systemUserManager)
        {
            _systemUserManager = systemUserManager;
        }

        [Route("Login")]
        [HttpGet]
        public async Task<IActionResult> Login(string userName, string password)
        {
            try
            {
                var user = await _systemUserManager.FindByNameAsync(userName);
                if (user == null)
                    throw new UserNotFoundException($"User {userName} Not Found !!");
                var correctPassword = await _systemUserManager.CheckPasswordAsync(user, password);
                if (!correctPassword)
                    return BadRequest("Wrong User Name or Password !!");
                var token = await _systemUserManager.CreateSecurityTokenAsync(user);
                return Ok(token);
            }
            catch (UserNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}