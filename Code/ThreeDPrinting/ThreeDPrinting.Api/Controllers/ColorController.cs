﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ThreeDPrinting.Api.Models;
using ThreeDPrinting.Model.AggregateRoot;
using ThreeDPrinting.Model.Exceptions;
using ThreeDPrinting.Repository.Repositories;

namespace ThreeDPrinting.Api.Controllers
{
    [Route("api/Color")]
    public class ColorController : Controller
    {
        private readonly IColorRepository _colorRepository;

        public ColorController(IColorRepository colorRepository)
        {
            _colorRepository = colorRepository;
        }

        [HttpPost]
        [Route("AddColor")]
        public async Task<IActionResult> AddColor([FromBody] AddColorModel addColorModel)
        {
            try
            {
                var color = new Color(addColorModel.ColorName, addColorModel.R, addColorModel.G, addColorModel.B);
                await _colorRepository.AddColor(color);
                return Ok();
            }
            catch (ColorAlreadyExistException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int) HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [Route("EditColor")]
        [HttpPut]
        public async Task<IActionResult> EditColor([FromBody] EditColorModel editColorModel)
        {
            try
            {
                await _colorRepository.EditColor(editColorModel.ColorId, editColorModel.ColorName, editColorModel.R,
                    editColorModel.G, editColorModel.B);
                return Ok();
            }
            catch (ColorNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int) HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpDelete]
        [Route("DeleteColor")]
        public async Task<IActionResult> DeleteColor(Guid colorId)
        {
            try
            {
                await _colorRepository.DeleteColor(colorId);
                return Ok();
            }
            catch (ColorNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int) HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [Route("GetColor")]
        [HttpGet]
        public async Task<IActionResult> GetColor(Guid colorId)
        {
            try
            {
                var color = await _colorRepository.GetColor(colorId);
                return Ok(color);
            }
            catch (ColorNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int) HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [Route("GetAllColors")]
        [HttpGet]
        public async Task<IActionResult> GetAllColors()
        {
            try
            {
                var colors = await _colorRepository.GetAllColors();
                return Ok(colors);
            }
            catch (Exception e)
            {
                return StatusCode((int) HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}