﻿using System;

namespace ThreeDPrinting.Api.Models
{
    public class VerifyCustomerEmailModel
    {
        public Guid CustomerId { get; set; }
        public string Token { get; set; }
    }
}