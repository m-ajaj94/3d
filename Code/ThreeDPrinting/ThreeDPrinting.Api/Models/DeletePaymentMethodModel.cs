﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ThreeDPrinting.Api.Models
{
    public class DeletePaymentMethodModel
    {
        public Guid CustomerId { get; set; }
        public Guid PaymentMethodId { get; set; }
    }
}