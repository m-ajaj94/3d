﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ThreeDPrinting.Api.Models
{
    public class AddPaymentMethodModel
    {
        public Guid CustomerId { get; set; }
        public string CardNumber { get; set; }
        public string Name { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string CardCompany { get; set; }
    }
}