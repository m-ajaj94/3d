﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ThreeDPrinting.Api.Models
{
    public class DeleteAddressModel
    {
        public Guid CustomerId { get; set; }
        public Guid AddressId { get; set; }
    }
}