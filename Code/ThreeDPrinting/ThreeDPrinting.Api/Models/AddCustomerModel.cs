﻿namespace ThreeDPrinting.Api.Models
{
    public class AddCustomerModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public byte[] Photo { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}