﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ThreeDPrinting.Api.Models
{
    public class EditPaymentMethodModel
    {
        public Guid CustomerId { get; set; }
        public Guid PaymentMethodId { get; set; }
        public string CardNumber { get; set; }
        public string Name { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string CardCompany { get; set; }
    }
}