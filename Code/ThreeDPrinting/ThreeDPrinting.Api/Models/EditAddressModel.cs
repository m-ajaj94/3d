﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ThreeDPrinting.Api.Models
{
    public class EditAddressModel
    {
        public Guid CustomerId { get; set; }
        public Guid AddressId { get; set; }
        public string ContactName { get; set; }
        public string CountryCode { get; set; }
        public string Mobile { get; set; }
        public string StreetAddress { get; set; }
        public string StreetAddressNumberPOBox { get; set; }
        public string ApartmentSuiteUnitBuildingFloor { get; set; }
        public string Country { get; set; }
        public string StateProvinceCounty { get; set; }
        public string City { get; set; }
        public string ZipPostalCode { get; set; }
        public string SecurityAccessCode { get; set; }
        public bool IsDefaultShipping { get; set; }
        public bool IsDefaultBilling { get; set; }
    }
}