﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ThreeDPrinting.Model.Enums;

namespace ThreeDPrinting.Api.Models
{
    public class EditMaterialModel
    {
        public Guid MaterialId { get; set; }
        public string MaterialName { get; set; }
        public CurrencyType Currency { get; set; }
        public int PricePerGram { get; set; }
        public List<Guid> Colors { get; set; }
    }
}