﻿using System;

namespace ThreeDPrinting.Api.Models
{
    public class ModifyCustomerModel
    {
        public Guid CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public byte[] Photo { get; set; }
    }
}