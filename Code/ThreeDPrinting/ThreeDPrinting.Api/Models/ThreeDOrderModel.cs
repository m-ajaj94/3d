using System;
using System.Collections.Generic;

namespace ThreeDPrinting.Api.Models
{
    public class ThreeDOrderModel
    {
        public Guid CustomerId { get; set; }
        public Guid ServiceId { get; set; }
        public Guid MaterialId { get; set; }
        public List<Guid> ColorsId { get; set; }
        public string OrderNote { get; set; }
    }
}