﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ThreeDPrinting.Api.Models
{
    public class AddColorModel
    {
        public string ColorName { get; set; }
        public int R { get; set; }
        public int G { get; set; }
        public int B { get; set; }
    }
}