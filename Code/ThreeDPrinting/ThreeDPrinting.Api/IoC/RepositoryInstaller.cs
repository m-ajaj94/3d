﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using ThreeDPrinting.Repository;
using ThreeDPrinting.Repository.Repositories;

namespace ThreeDPrinting.Api.IoC
{
    public class RepositoryInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<ICustomerRepository>().ImplementedBy<CustomerRepository>().LifestyleTransient()
                );
        }
    }
}