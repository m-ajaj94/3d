﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using ThreeDPrinting.Repository.IdentityManagers;

namespace ThreeDPrinting.Api.IoC
{
    public class IdentityInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<ISystemUserManager>().ImplementedBy<SystemUserManager>().LifestyleTransient(),
                Component.For<ISystemRoleManager>().ImplementedBy<SystemRoleManager>().LifestyleTransient()
                );
        }
    }
}