﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GardarFramework.Data.Entity;
using IdentityModel;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using ThreeDPrinting.IdentityServer.Services;
using ThreeDPrinting.Model.Identity;
using ThreeDPrinting.Repository;
using ThreeDPrinting.Repository.IdentityManagers;
using ThreeDPrinting.Repository.IdentityStores;
using ThreeDPrinting.Repository.Repositories;

namespace ThreeDPrinting.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DBContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), x => x.MigrationsAssembly(typeof(DBContext).Assembly.GetName().Name)));

            services.TryAddScoped<DbContext, DBContext>();
            services.TryAddScoped<ISystemUserManager, SystemUserManager>();
            services.TryAddScoped<ISystemRoleManager, SystemRoleManager>();
            services.TryAddScoped<ICustomerRepository, CustomerRepository>();
            services.TryAddScoped<IColorRepository, ColorRepository>();
            services.TryAddScoped<IMaterialRepository, MaterialRepository>();
            services.TryAddScoped<IThreeDOrderRepository, ThreeDOrderRepository>();
            services.TryAddScoped<IProfileService, ProfileService>();
            // Add identity types
            services.AddCors(options =>
            {
                options.AddPolicy("AllowSpecificOrigin",
                    builder => builder.AllowAnyOrigin().AllowAnyMethod().SetIsOriginAllowed(s=>true));
            });
            services.AddIdentity<SystemUser, SystemRole>()
                .AddUserStore<SystemUserStore>()
                .AddRoleStore<SystemRoleStore>()
                .AddDefaultTokenProviders();
            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = false;
                options.Password.RequiredUniqueChars = 6;

                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;
                options.Lockout.AllowedForNewUsers = true;

                // User settings
                options.User.RequireUniqueEmail = true;
            });
            services.AddMvc();
            services.AddAuthentication("Bearer").AddIdentityServerAuthentication(option =>
            {
                option.Authority = "http://localhost:5000";
                option.RequireHttpsMetadata = false;
                option.ApiName = "WebAPI";
                option.RoleClaimType = JwtClaimTypes.Role;
                option.NameClaimType = JwtClaimTypes.PreferredUserName;
            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "ThreeD Printing Api V1");
            });
            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
