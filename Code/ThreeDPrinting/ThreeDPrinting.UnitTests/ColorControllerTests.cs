﻿using System;
using GardarFramework.Test.Nunit;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using ThreeDPrinting.Api.Controllers;
using ThreeDPrinting.Api.Models;
using ThreeDPrinting.Model.AggregateRoot;
using ThreeDPrinting.Model.Entities;
using ThreeDPrinting.Model.Exceptions;
using ThreeDPrinting.Repository.Repositories;

namespace ThreeDPrinting.UnitTests
{
    public class ColorControllerTests : TestBaseClass
    {
        private ColorController _colorController;
        private Mock<IColorRepository> _mockedColorRepository;

        protected override void BecauseOf()
        {
            base.BecauseOf();
            _mockedColorRepository = new Mock<IColorRepository>();
            _colorController = new ColorController(_mockedColorRepository.Object);
        }

        public class AddColorTests : ColorControllerTests
        {
            private AddColorModel _addColorModel;
            private IActionResult _returnedValue;

            protected override void BecauseOf()
            {
                base.BecauseOf();
                _addColorModel = new AddColorModel()
                {
                    ColorName = "RED",
                    B = 0,
                    R = 255,
                    G = 0
                };
            }

            public class AddColorMethodSucess : AddColorTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _returnedValue = _colorController.AddColor(_addColorModel).Result;
                }

                [Test]
                public void TestAddColorSucess()
                {
                    _mockedColorRepository.Verify(x => x.AddColor(It.IsAny<Color>()), Times.Once,
                        "AddColor not called");
                    Assert.That(_returnedValue, Is.InstanceOf<OkResult>(), "Returned result not correct");
                }
            }

            public class AddColorThrowException : AddColorTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedColorRepository.Setup(x => x.AddColor(It.IsAny<Color>()))
                        .Throws(new Exception("Object Reference Exception!!"));
                    _returnedValue = _colorController.AddColor(_addColorModel).Result;
                }

                [Test]
                public void TestAddColorThrowException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<ObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as ObjectResult;
                    _mockedColorRepository.Verify(x => x.AddColor(It.IsAny<Color>()), Times.Once,
                        "AddColor not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(500, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Object Reference Exception!!"),
                        "Excption Message not correct..");
                }
            }

            public class ColorAlreadyExistExceptionThrown : AddColorTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedColorRepository.Setup(x => x.AddColor(It.IsAny<Color>()))
                        .Throws(new ColorAlreadyExistException("Color Already Exist Exception !!"));
                    _returnedValue = _colorController.AddColor(_addColorModel).Result;
                }

                [Test]
                public void TestColorAlreadyExistExceptionThrown()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedColorRepository.Verify(x => x.AddColor(It.IsAny<Color>()), Times.Once,
                        "AddColor not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Color Already Exist Exception !!"),
                        "Excption Message not correct..");
                }
            }
        }

        public class EditColorTests : ColorControllerTests
        {
            private EditColorModel _editColorModel;
            private IActionResult _returnedValue;

            protected override void BecauseOf()
            {
                base.BecauseOf();
                _editColorModel = new EditColorModel()
                {
                    ColorId = Guid.NewGuid(),
                    ColorName = "Green",
                    R = 0,
                    G = 255,
                    B = 0
                };
            }

            public class EditColorSucess : EditColorTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _returnedValue = _colorController.EditColor(_editColorModel).Result;
                }

                [Test]
                public void TestEditColorSucess()
                {
                    _mockedColorRepository.Verify(
                        x => x.EditColor(_editColorModel.ColorId, _editColorModel.ColorName, _editColorModel.R,
                            _editColorModel.G, _editColorModel.B), Times.Once,
                        "EditColor not called");
                    Assert.That(_returnedValue, Is.InstanceOf<OkResult>(), "Returned result not correct");
                }
            }

            public class EditColorThrowException : EditColorTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedColorRepository.Setup(x => x.EditColor(_editColorModel.ColorId, _editColorModel.ColorName,
                            _editColorModel.R, _editColorModel.G, _editColorModel.B))
                        .Throws(new Exception("Object Reference Exception!!"));
                    _returnedValue = _colorController.EditColor(_editColorModel).Result;
                }

                [Test]
                public void TestEditColorThrowException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<ObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as ObjectResult;
                    _mockedColorRepository.Verify(
                        x => x.EditColor(_editColorModel.ColorId, _editColorModel.ColorName, _editColorModel.R,
                            _editColorModel.G, _editColorModel.B), Times.Once,
                        "EditColor not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(500, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Object Reference Exception!!"),
                        "Excption Message not correct..");
                }
            }

            public class ColorNotFoundExceptionThrown : EditColorTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedColorRepository.Setup(x => x.EditColor(_editColorModel.ColorId, _editColorModel.ColorName,
                            _editColorModel.R, _editColorModel.G, _editColorModel.B))
                        .Throws(new ColorNotFoundException("Color Not Found!!"));
                    _returnedValue = _colorController.EditColor(_editColorModel).Result;
                }

                [Test]
                public void TestColorNotFoundExceptionThrown()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedColorRepository.Verify(
                        x => x.EditColor(_editColorModel.ColorId, _editColorModel.ColorName, _editColorModel.R,
                            _editColorModel.G, _editColorModel.B), Times.Once,
                        "EditColor not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Color Not Found!!"), "Excption Message not correct..");
                }
            }
        }

        public class DeleteColorTests : ColorControllerTests
        {
            private IActionResult _returnedValue;
            private Guid _colorId = Guid.NewGuid();

            protected override void BecauseOf()
            {
                base.BecauseOf();
            }

            public class DeleteColorSucess : DeleteColorTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _returnedValue = _colorController.DeleteColor(_colorId).Result;
                }

                [Test]
                public void TestDeleteColorSucess()
                {
                    _mockedColorRepository.Verify(
                        x => x.DeleteColor(_colorId),
                        Times.Once(),
                        "DeleteColor not called");
                    Assert.That(_returnedValue, Is.InstanceOf<OkResult>(), "Returned result not correct");
                }
            }

            public class DeleteColorThrowException : DeleteColorTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedColorRepository.Setup(x =>
                            x.DeleteColor(_colorId))
                        .Throws(new Exception("Object Reference Exception!!"));
                    _returnedValue = _colorController.DeleteColor(_colorId).Result;
                }

                [Test]
                public void TestDeleteColorThrowException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<ObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as ObjectResult;
                    _mockedColorRepository.Verify(
                        x => x.DeleteColor(_colorId),
                        Times.Once(),
                        "DeleteColor not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(500, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Object Reference Exception!!"),
                        "Excption Message not correct..");
                }
            }

            public class ColorNotFoundExceptionThrown : DeleteColorTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedColorRepository.Setup(x => x.DeleteColor(_colorId))
                        .Throws(new ColorNotFoundException("Color Not Found!!"));
                    _returnedValue = _colorController.DeleteColor(_colorId).Result;
                }

                [Test]
                public void TestColorNotFoundExceptionThrown()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedColorRepository.Verify(
                        x => x.DeleteColor(_colorId), Times.Once,
                        "DeleteColor not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Color Not Found!!"), "Excption Message not correct..");
                }
            }
        }

        public class GetColorTests : ColorControllerTests
        {
            private Guid _colorId = Guid.NewGuid();
            private IActionResult _returnedValue;

            protected override void BecauseOf()
            {
                base.BecauseOf();
            }

            public class GetColorSucess : GetColorTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _returnedValue = _colorController.GetColor(_colorId).Result;
                }

                [Test]
                public void TestGetColorSucess()
                {
                    _mockedColorRepository.Verify(x => x.GetColor(_colorId), Times.Once,
                        "GetColor not called");
                    Assert.That(_returnedValue, Is.InstanceOf<OkObjectResult>(), "Returned result not correct");
                }
            }

            public class GetColorThrowException : GetColorTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedColorRepository.Setup(x => x.GetColor(_colorId))
                        .Throws(new Exception("Object Reference Exception!!"));
                    _returnedValue = _colorController.GetColor(_colorId).Result;
                }

                [Test]
                public void TestGetColorThrowException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<ObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as ObjectResult;
                    _mockedColorRepository.Verify(x => x.GetColor(_colorId), Times.Once,
                        "GetPaymentMethod not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(500, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Object Reference Exception!!"),
                        "Excption Message not correct..");
                }
            }

            public class ColorNotFoundExceptionThrown : GetColorTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedColorRepository.Setup(x => x.GetColor(_colorId))
                        .Throws(new ColorNotFoundException("Color Not Found!!"));
                    _returnedValue = _colorController.GetColor(_colorId).Result;
                }

                [Test]
                public void TestColorNotFoundExceptionThrown()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(),
                        "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedColorRepository.Verify(x => x.GetColor(_colorId), Times.Once,
                        "GetColor not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Color Not Found!!"), "Excption Message not correct..");
                }
            }
        }
    }
}