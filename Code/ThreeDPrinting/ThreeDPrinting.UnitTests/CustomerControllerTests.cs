﻿using System;
using System.Collections.Generic;
using GardarFramework.Test.Nunit;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using ThreeDPrinting.Api.Controllers;
using ThreeDPrinting.Api.Models;
using ThreeDPrinting.Model.Entities;
using ThreeDPrinting.Model.Enums;
using ThreeDPrinting.Model.Exceptions;
using ThreeDPrinting.Model.Identity;
using ThreeDPrinting.Repository;
using ThreeDPrinting.Repository.IdentityManagers;
using ThreeDPrinting.Repository.Repositories;

namespace ThreeDPrinting.UnitTests
{
    public class CustomerControllerTests : TestBaseClass
    {
        private CustomerController _customerController;
        private Mock<ISystemUserManager> _mockedUserManagerMock;
        private Mock<ICustomerRepository> _mockedCustomerRepository;

        protected override void BecauseOf()
        {
            base.BecauseOf();
            _mockedUserManagerMock = new Mock<ISystemUserManager>();
            _mockedCustomerRepository = new Mock<ICustomerRepository>();
            _customerController =
                new CustomerController(_mockedUserManagerMock.Object, _mockedCustomerRepository.Object);
        }

        public class RegisterCustomerTests : CustomerControllerTests
        {
            private AddCustomerModel _addCustomerModel;
            private string _expectedConfirmationToken;
            private IActionResult _returnedValue;

            protected override void BecauseOf()
            {
                base.BecauseOf();
                _addCustomerModel = new AddCustomerModel()
                {
                    FirstName = "fname",
                    LastName = "lname",
                    Email = "test@hotmail.com",
                    UserName = "user1",
                    Password = "Pass1",
                    Photo = new byte[2]
                };

                _expectedConfirmationToken = "CfDJ8DPmOf/gSDtFidCE7uyqln";
            }

            public class RegisterCustomerSuccess : RegisterCustomerTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedUserManagerMock.Setup(x => x.CreateAsync(It.IsAny<SystemUser>(), _addCustomerModel.Password))
                        .ReturnsAsync(IdentityResult.Success);
                    _mockedUserManagerMock.Setup(x => x.GenerateEmailConfirmationTokenAsync(It.IsAny<SystemUser>()))
                        .ReturnsAsync(_expectedConfirmationToken);
                    _returnedValue = _customerController.RegisterCustomer(_addCustomerModel).Result;
                }

                [Test]
                public void TestRegisterCustomer()
                {
                    var result = _returnedValue as OkObjectResult;
                    _mockedUserManagerMock.Verify(
                        x => x.CreateAsync(It.IsAny<SystemUser>(), _addCustomerModel.Password), Times.Once,
                        "CreateAsync not called..");
                    _mockedCustomerRepository.Verify(x => x.AddCustomer(It.IsAny<SystemUser>()), Times.Once,
                        "AddCustomer not called..");
                    _mockedUserManagerMock.Verify(x => x.GenerateEmailConfirmationTokenAsync(It.IsAny<SystemUser>()),
                        Times.Once, "Generate Email Confirmation Token Async not called..");
                    Assert.That(result.Value, Is.EqualTo(_expectedConfirmationToken),
                        "Confirmation Token not matched..");
                }
            }

            public class CreateAsyncFail : RegisterCustomerTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedUserManagerMock.Setup(x => x.CreateAsync(It.IsAny<SystemUser>(), _addCustomerModel.Password))
                        .ReturnsAsync(IdentityResult.Failed());
                    _returnedValue = _customerController.RegisterCustomer(_addCustomerModel).Result;
                }

                [Test]
                public void TestRegisterCustomer()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedUserManagerMock.Verify(
                        x => x.CreateAsync(It.IsAny<SystemUser>(), _addCustomerModel.Password), Times.Once,
                        "CreateAsync not called..");
                    _mockedCustomerRepository.Verify(x => x.AddCustomer(It.IsAny<SystemUser>()), Times.Never,
                        "AddCustomer called..");
                    _mockedUserManagerMock.Verify(x => x.GenerateEmailConfirmationTokenAsync(It.IsAny<SystemUser>()),
                        Times.Never, "Generate Email Confirmation Token Async called..");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                }
            }

            public class CustomerAlreadyExistExceptionViolation : RegisterCustomerTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedUserManagerMock.Setup(x => x.CreateAsync(It.IsAny<SystemUser>(), _addCustomerModel.Password))
                        .ReturnsAsync(IdentityResult.Success);
                    _mockedCustomerRepository.Setup(x => x.AddCustomer(It.IsAny<SystemUser>()))
                        .Throws(new CustomerAlreadyExistException("Customer Already Exist Exception!!"));
                    _returnedValue = _customerController.RegisterCustomer(_addCustomerModel).Result;
                }

                [Test]
                public void TestRegisterCustomer()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedUserManagerMock.Verify(
                        x => x.CreateAsync(It.IsAny<SystemUser>(), _addCustomerModel.Password), Times.Once,
                        "CreateAsync not called..");
                    _mockedCustomerRepository.Verify(x => x.AddCustomer(It.IsAny<SystemUser>()), Times.Once,
                        "AddCustomer called..");
                    _mockedUserManagerMock.Verify(x => x.GenerateEmailConfirmationTokenAsync(It.IsAny<SystemUser>()),
                        Times.Never, "Generate Email Confirmation Token Async called..");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Customer Already Exist Exception!!"),
                        "Excption Message not correct..");
                }
            }

            public class CustomerRegistrationThrowException : RegisterCustomerTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedUserManagerMock.Setup(x => x.CreateAsync(It.IsAny<SystemUser>(), _addCustomerModel.Password))
                        .ReturnsAsync(IdentityResult.Success);
                    _mockedUserManagerMock.Setup(x => x.GenerateEmailConfirmationTokenAsync(It.IsAny<SystemUser>()))
                        .Throws(new Exception("Object Reference Exception!!"));
                    _returnedValue = _customerController.RegisterCustomer(_addCustomerModel).Result;
                }

                [Test]
                public void TestRegisterCustomer()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<ObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as ObjectResult;
                    _mockedUserManagerMock.Verify(
                        x => x.CreateAsync(It.IsAny<SystemUser>(), _addCustomerModel.Password), Times.Once,
                        "CreateAsync not called..");
                    _mockedCustomerRepository.Verify(x => x.AddCustomer(It.IsAny<SystemUser>()), Times.Once,
                        "AddCustomer called..");
                    _mockedUserManagerMock.Verify(x => x.GenerateEmailConfirmationTokenAsync(It.IsAny<SystemUser>()),
                        Times.Once, "Generate Email Confirmation Token Async called..");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(500, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Object Reference Exception!!"),
                        "Excption Message not correct..");
                }
            }
        }

        public class ModifyCustomerTests : CustomerControllerTests
        {
            private ModifyCustomerModel _modifyCustomerModel;
            private IActionResult _returnedValue;

            protected override void BecauseOf()
            {
                base.BecauseOf();
                _modifyCustomerModel = new ModifyCustomerModel()
                {
                    CustomerId = Guid.NewGuid(),
                    FirstName = "fname",
                    LastName = "lname",
                    Email = "test@hotmail.com",
                    Photo = new byte[4]
                };
            }

            public class ModifyCustomerSucess : ModifyCustomerTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x => x.GetUserId(_modifyCustomerModel.CustomerId))
                        .ReturnsAsync(It.IsAny<Guid>());
                    _mockedUserManagerMock.Setup(x => x.FindByIdAsync(It.IsAny<string>()))
                        .ReturnsAsync(new SystemUser());
                    _mockedUserManagerMock.Setup(x => x.UpdateAsync(It.IsAny<SystemUser>()))
                        .ReturnsAsync(IdentityResult.Success);
                    _returnedValue = _customerController.ModifyCustomer(_modifyCustomerModel).Result;
                }

                [Test]
                public void TestModifyCustomerSucess()
                {
                    _mockedCustomerRepository.Verify(x => x.GetUserId(_modifyCustomerModel.CustomerId), Times.Once,
                        "GetUserId not called");
                    _mockedUserManagerMock.Verify(x => x.FindByIdAsync(It.IsAny<string>()), Times.Once,
                        "FindByIdAsync not called");
                    _mockedUserManagerMock.Verify(x => x.UpdateAsync(It.IsAny<SystemUser>()), Times.Once,
                        "UpdateAsync not called");
                    Assert.That(_returnedValue, Is.InstanceOf<OkResult>(), "Returned result not correct");
                }
            }

            public class UpdateCustomerFail : ModifyCustomerTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x => x.GetUserId(_modifyCustomerModel.CustomerId))
                        .ReturnsAsync(It.IsAny<Guid>());
                    _mockedUserManagerMock.Setup(x => x.FindByIdAsync(It.IsAny<string>()))
                        .ReturnsAsync(new SystemUser());
                    _mockedUserManagerMock.Setup(x => x.UpdateAsync(It.IsAny<SystemUser>()))
                        .ReturnsAsync(IdentityResult.Failed());
                    _returnedValue = _customerController.ModifyCustomer(_modifyCustomerModel).Result;
                }

                [Test]
                public void TestUpdateCustomerFail()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedCustomerRepository.Verify(x => x.GetUserId(_modifyCustomerModel.CustomerId), Times.Once,
                        "GetUserId not called");
                    _mockedUserManagerMock.Verify(x => x.FindByIdAsync(It.IsAny<string>()), Times.Once,
                        "FindByIdAsync not called");
                    _mockedUserManagerMock.Verify(x => x.UpdateAsync(It.IsAny<SystemUser>()), Times.Once,
                        "UpdateAsync not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                }
            }

            public class CustomerNotFoundExceptionViolation : ModifyCustomerTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x => x.GetUserId(_modifyCustomerModel.CustomerId))
                        .Throws(new CustomerNotFoundException("Customer Not Found!!"));

                    _returnedValue = _customerController.ModifyCustomer(_modifyCustomerModel).Result;
                }

                [Test]
                public void TestCustomerNotFoundException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedCustomerRepository.Verify(x => x.GetUserId(_modifyCustomerModel.CustomerId), Times.Once,
                        "GetUserId not called");
                    _mockedUserManagerMock.Verify(x => x.FindByIdAsync(It.IsAny<string>()), Times.Never,
                        "FindByIdAsync called");
                    _mockedUserManagerMock.Verify(x => x.UpdateAsync(It.IsAny<SystemUser>()), Times.Never,
                        "UpdateAsync called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Customer Not Found!!"), "Excption Message not correct..");
                }
            }

            public class ModifyCustomerThrowException : ModifyCustomerTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x => x.GetUserId(_modifyCustomerModel.CustomerId))
                        .ReturnsAsync(It.IsAny<Guid>());
                    _mockedUserManagerMock.Setup(x => x.FindByIdAsync(It.IsAny<string>()))
                        .ReturnsAsync(new SystemUser());
                    _mockedUserManagerMock.Setup(x => x.UpdateAsync(It.IsAny<SystemUser>()))
                        .Throws(new Exception("Object Reference Exception!!"));
                    _returnedValue = _customerController.ModifyCustomer(_modifyCustomerModel).Result;
                }

                [Test]
                public void TestModifyCustomerThrowException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<ObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as ObjectResult;
                    _mockedCustomerRepository.Verify(x => x.GetUserId(_modifyCustomerModel.CustomerId), Times.Once,
                        "GetUserId not called");
                    _mockedUserManagerMock.Verify(x => x.FindByIdAsync(It.IsAny<string>()), Times.Once,
                        "FindByIdAsync not called");
                    _mockedUserManagerMock.Verify(x => x.UpdateAsync(It.IsAny<SystemUser>()), Times.Once,
                        "UpdateAsync not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(500, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Object Reference Exception!!"),
                        "Excption Message not correct..");
                }
            }
        }

        public class VerifyCustomerEmailTests : CustomerControllerTests
        {
            private VerifyCustomerEmailModel _verifyCustomerEmailModel;
            private IActionResult _returnedValue;

            protected override void BecauseOf()
            {
                base.BecauseOf();
                _verifyCustomerEmailModel = new VerifyCustomerEmailModel()
                {
                    CustomerId = Guid.NewGuid(),
                    Token = "gesgsree"
                };
            }

            public class VerifyCustomerEmailSucess : VerifyCustomerEmailTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x => x.GetUserId(_verifyCustomerEmailModel.CustomerId))
                        .ReturnsAsync(It.IsAny<Guid>());
                    _mockedUserManagerMock.Setup(x => x.FindByIdAsync(It.IsAny<string>()))
                        .ReturnsAsync(new SystemUser());
                    _mockedUserManagerMock
                        .Setup(x => x.ConfirmEmailAsync(It.IsAny<SystemUser>(), _verifyCustomerEmailModel.Token))
                        .ReturnsAsync(IdentityResult.Success);
                    _returnedValue = _customerController.VerifyCustomerEmail(_verifyCustomerEmailModel).Result;
                }

                [Test]
                public void TestVerifyCustomerSucess()
                {
                    _mockedCustomerRepository.Verify(x => x.GetUserId(_verifyCustomerEmailModel.CustomerId), Times.Once,
                        "GetUserId not called");
                    _mockedUserManagerMock.Verify(x => x.FindByIdAsync(It.IsAny<string>()), Times.Once,
                        "FindByIdAsync not called");
                    _mockedUserManagerMock.Verify(
                        x => x.ConfirmEmailAsync(It.IsAny<SystemUser>(), _verifyCustomerEmailModel.Token), Times.Once,
                        "ConfirmEmailAsync not called");
                    Assert.That(_returnedValue, Is.InstanceOf<OkResult>(), "Returned result not correct");
                }
            }

            public class ConfirmEmailFail : VerifyCustomerEmailTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x => x.GetUserId(_verifyCustomerEmailModel.CustomerId))
                        .ReturnsAsync(It.IsAny<Guid>());
                    _mockedUserManagerMock.Setup(x => x.FindByIdAsync(It.IsAny<string>()))
                        .ReturnsAsync(new SystemUser());
                    _mockedUserManagerMock
                        .Setup(x => x.ConfirmEmailAsync(It.IsAny<SystemUser>(), _verifyCustomerEmailModel.Token))
                        .ReturnsAsync(IdentityResult.Failed());
                    _returnedValue = _customerController.VerifyCustomerEmail(_verifyCustomerEmailModel).Result;
                }

                [Test]
                public void TestConfirmEmailFail()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedCustomerRepository.Verify(x => x.GetUserId(_verifyCustomerEmailModel.CustomerId), Times.Once,
                        "GetUserId not called");
                    _mockedUserManagerMock.Verify(x => x.FindByIdAsync(It.IsAny<string>()), Times.Once,
                        "FindByIdAsync not called");
                    _mockedUserManagerMock.Verify(
                        x => x.ConfirmEmailAsync(It.IsAny<SystemUser>(), _verifyCustomerEmailModel.Token), Times.Once,
                        "ConfirmEmailAsync not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                }
            }

            public class VerifyEmailThrowException : VerifyCustomerEmailTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x => x.GetUserId(_verifyCustomerEmailModel.CustomerId))
                        .ReturnsAsync(It.IsAny<Guid>());
                    _mockedUserManagerMock.Setup(x => x.FindByIdAsync(It.IsAny<string>()))
                        .ReturnsAsync(new SystemUser());
                    _mockedUserManagerMock.Setup(x =>
                            x.ConfirmEmailAsync(It.IsAny<SystemUser>(), _verifyCustomerEmailModel.Token))
                        .Throws(new Exception("Object Reference Exception!!"));
                    _returnedValue = _customerController.VerifyCustomerEmail(_verifyCustomerEmailModel).Result;
                }

                [Test]
                public void TestVerifyEmailThrowException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<ObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as ObjectResult;
                    _mockedCustomerRepository.Verify(x => x.GetUserId(_verifyCustomerEmailModel.CustomerId), Times.Once,
                        "GetUserId not called");
                    _mockedUserManagerMock.Verify(x => x.FindByIdAsync(It.IsAny<string>()), Times.Once,
                        "FindByIdAsync not called");
                    _mockedUserManagerMock.Verify(
                        x => x.ConfirmEmailAsync(It.IsAny<SystemUser>(), _verifyCustomerEmailModel.Token), Times.Once,
                        "ConfirmEmailAsync not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(500, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Object Reference Exception!!"),
                        "Excption Message not correct..");
                }
            }

            public class CustomerNotFound : VerifyCustomerEmailTests
            {
                private SystemUser user = null;

                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x => x.GetUserId(_verifyCustomerEmailModel.CustomerId))
                        .ReturnsAsync(It.IsAny<Guid>());

                    _mockedUserManagerMock.Setup(x => x.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(user);
                    _returnedValue = _customerController.VerifyCustomerEmail(_verifyCustomerEmailModel).Result;
                }

                [Test]
                public void TestCustomerNotFound()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<NotFoundObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as NotFoundObjectResult;
                    _mockedCustomerRepository.Verify(x => x.GetUserId(_verifyCustomerEmailModel.CustomerId), Times.Once,
                        "GetUserId not called");
                    _mockedUserManagerMock.Verify(x => x.FindByIdAsync(It.IsAny<string>()), Times.Once,
                        "FindByIdAsync not called");
                    _mockedUserManagerMock.Verify(
                        x => x.ConfirmEmailAsync(It.IsAny<SystemUser>(), _verifyCustomerEmailModel.Token), Times.Never,
                        "ConfirmEmailAsync called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(404, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Customer Not Found!!"), "Exception Message not correct..");
                }
            }
        }

        public class AddAddressTests : CustomerControllerTests
        {
            private AddAddressModel _addAddressModel;
            private IActionResult _returnedValue;

            protected override void BecauseOf()
            {
                base.BecauseOf();
                _addAddressModel = new AddAddressModel()
                {
                    CustomerId = Guid.NewGuid(),
                    IsDefaultShipping = true,
                    City = "Damascus",
                    Mobile = "0977839332",
                };
            }

            public class AddAddressSucess : AddAddressTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _returnedValue = _customerController.AddAddress(_addAddressModel).Result;
                }

                [Test]
                public void TestAddAddressSucess()
                {
                    _mockedCustomerRepository.Verify(x => x.AddAddress(It.IsAny<Address>()), Times.Once,
                        "AddAddress not called");
                    Assert.That(_returnedValue, Is.InstanceOf<OkResult>(), "Returned result not correct");
                }
            }

            public class AddAddressThrowException : AddAddressTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x => x.AddAddress(It.IsAny<Address>()))
                        .Throws(new Exception("Object Reference Exception!!"));
                    _returnedValue = _customerController.AddAddress(_addAddressModel).Result;
                }

                [Test]
                public void TestAddAddressThrowException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<ObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as ObjectResult;
                    _mockedCustomerRepository.Verify(x => x.AddAddress(It.IsAny<Address>()), Times.Once,
                        "AddAddress not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(500, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Object Reference Exception!!"),
                        "Excption Message not correct..");
                }
            }

            public class CustomerNotFoundExceptionThrown : AddAddressTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x => x.AddAddress(It.IsAny<Address>()))
                        .Throws(new CustomerNotFoundException("Customer Not Found!!"));
                    _returnedValue = _customerController.AddAddress(_addAddressModel).Result;
                }

                [Test]
                public void TestCustomerNotFoundExceptionThrown()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedCustomerRepository.Verify(x => x.AddAddress(It.IsAny<Address>()), Times.Once,
                        "AddAddress not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Customer Not Found!!"), "Excption Message not correct..");
                }
            }
        }

        public class EditAddressTests : CustomerControllerTests
        {
            private EditAddressModel _editAddressModel;
            private IActionResult _returnedValue;
            private Guid _addressId = Guid.NewGuid();

            protected override void BecauseOf()
            {
                base.BecauseOf();
                _editAddressModel = new EditAddressModel()
                {
                    AddressId = Guid.NewGuid(),
                    ApartmentSuiteUnitBuildingFloor = "",
                    IsDefaultShipping = true,
                    CountryCode = "",
                    City = "damascus",
                    StateProvinceCounty = "",
                    ZipPostalCode = "",
                    Mobile = "2345433454",
                    ContactName = "Name",
                    StreetAddressNumberPOBox = "test",
                    StreetAddress = "",
                    SecurityAccessCode = "",
                    Country = "Syria"
                };
            }

            public class EditAddressSucess : EditAddressTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _returnedValue = _customerController.EditAddress(_editAddressModel).Result;
                }

                [Test]
                public void TestEditAddressSucess()
                {
                    _mockedCustomerRepository.Verify(
                        x => x.EditAddress(_editAddressModel.CustomerId, _editAddressModel.AddressId,
                            _editAddressModel.ApartmentSuiteUnitBuildingFloor, _editAddressModel.City,
                            _editAddressModel.ContactName, _editAddressModel.Country, _editAddressModel.CountryCode,
                            _editAddressModel.IsDefaultShipping, _editAddressModel.Mobile,
                            _editAddressModel.SecurityAccessCode, _editAddressModel.StateProvinceCounty,
                            _editAddressModel.StreetAddress, _editAddressModel.StreetAddressNumberPOBox,
                            _editAddressModel.ZipPostalCode, _editAddressModel.IsDefaultBilling), Times.Once(),
                        "EditAddress not called");
                    Assert.That(_returnedValue, Is.InstanceOf<OkResult>(), "Returned result not correct");
                }
            }

            public class EditAddressThrowException : EditAddressTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x => x.EditAddress(_editAddressModel.CustomerId,
                            _editAddressModel.AddressId,
                            _editAddressModel.ApartmentSuiteUnitBuildingFloor, _editAddressModel.City,
                            _editAddressModel.ContactName, _editAddressModel.Country, _editAddressModel.CountryCode,
                            _editAddressModel.IsDefaultShipping, _editAddressModel.Mobile,
                            _editAddressModel.SecurityAccessCode, _editAddressModel.StateProvinceCounty,
                            _editAddressModel.StreetAddress, _editAddressModel.StreetAddressNumberPOBox,
                            _editAddressModel.ZipPostalCode, _editAddressModel.IsDefaultBilling))
                        .Throws(new Exception("Object Reference Exception!!"));
                    _returnedValue = _customerController.EditAddress(_editAddressModel).Result;
                }

                [Test]
                public void TestEditAddressThrowException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<ObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as ObjectResult;
                    _mockedCustomerRepository.Verify(
                        x => x.EditAddress(_editAddressModel.CustomerId, _editAddressModel.AddressId,
                            _editAddressModel.ApartmentSuiteUnitBuildingFloor, _editAddressModel.City,
                            _editAddressModel.ContactName, _editAddressModel.Country, _editAddressModel.CountryCode,
                            _editAddressModel.IsDefaultShipping, _editAddressModel.Mobile,
                            _editAddressModel.SecurityAccessCode, _editAddressModel.StateProvinceCounty,
                            _editAddressModel.StreetAddress, _editAddressModel.StreetAddressNumberPOBox,
                            _editAddressModel.ZipPostalCode, _editAddressModel.IsDefaultBilling), Times.Once(),
                        "EditAddress not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(500, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Object Reference Exception!!"),
                        "Excption Message not correct..");
                }
            }

            public class AddressNotFoundExceptionTest : EditAddressTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x => x.EditAddress(_editAddressModel.CustomerId,
                            _editAddressModel.AddressId,
                            _editAddressModel.ApartmentSuiteUnitBuildingFloor, _editAddressModel.City,
                            _editAddressModel.ContactName, _editAddressModel.Country, _editAddressModel.CountryCode,
                            _editAddressModel.IsDefaultShipping, _editAddressModel.Mobile,
                            _editAddressModel.SecurityAccessCode, _editAddressModel.StateProvinceCounty,
                            _editAddressModel.StreetAddress, _editAddressModel.StreetAddressNumberPOBox,
                            _editAddressModel.ZipPostalCode, _editAddressModel.IsDefaultBilling))
                        .Throws(new AddressNotFoundException("Address Not Found!!!"));
                    _returnedValue = _customerController.EditAddress(_editAddressModel).Result;
                }

                [Test]
                public void TestAddressNotFoundException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedCustomerRepository.Verify(
                        x => x.EditAddress(_editAddressModel.CustomerId, _editAddressModel.AddressId,
                            _editAddressModel.ApartmentSuiteUnitBuildingFloor, _editAddressModel.City,
                            _editAddressModel.ContactName, _editAddressModel.Country, _editAddressModel.CountryCode,
                            _editAddressModel.IsDefaultShipping, _editAddressModel.Mobile,
                            _editAddressModel.SecurityAccessCode, _editAddressModel.StateProvinceCounty,
                            _editAddressModel.StreetAddress, _editAddressModel.StreetAddressNumberPOBox,
                            _editAddressModel.ZipPostalCode, _editAddressModel.IsDefaultBilling), Times.Once(),
                        "EditAddress not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Address Not Found!!!"), "Excption Message not correct..");
                }
            }
        }

        public class DeleteAddressTests : CustomerControllerTests
        {
            private IActionResult _returnedValue;
            private DeleteAddressModel _deleteAddressModel;
            private Guid _addressId = Guid.NewGuid();

            protected override void BecauseOf()
            {
                base.BecauseOf();
                _deleteAddressModel = new DeleteAddressModel()
                {
                    CustomerId = Guid.NewGuid(),
                    AddressId = Guid.NewGuid()
                };
            }

            public class DeleteAddressSucess : DeleteAddressTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _returnedValue = _customerController.DeleteAddress(_deleteAddressModel.CustomerId, _deleteAddressModel.AddressId).Result;
                }

                [Test]
                public void TestDeleteAddressSucess()
                {
                    _mockedCustomerRepository.Verify(
                        x => x.DeleteAddress(_deleteAddressModel.CustomerId, _deleteAddressModel.AddressId),
                        Times.Once(),
                        "DeleteAddress not called");
                    Assert.That(_returnedValue, Is.InstanceOf<OkResult>(), "Returned result not correct");
                }
            }

            public class DeleteAddressThrowException : DeleteAddressTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x =>
                            x.DeleteAddress(_deleteAddressModel.CustomerId, _deleteAddressModel.AddressId))
                        .Throws(new Exception("Object Reference Exception!!"));
                    _returnedValue = _customerController.DeleteAddress(_deleteAddressModel.CustomerId, _deleteAddressModel.AddressId).Result;
                }

                [Test]
                public void TestDeleteAddressThrowException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<ObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as ObjectResult;
                    _mockedCustomerRepository.Verify(
                        x => x.DeleteAddress(_deleteAddressModel.CustomerId, _deleteAddressModel.AddressId),
                        Times.Once(),
                        "DeleteAddress not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(500, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Object Reference Exception!!"),
                        "Excption Message not correct..");
                }
            }

            public class AddressNotFoundExceptionTest : DeleteAddressTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x =>
                            x.DeleteAddress(_deleteAddressModel.CustomerId, _deleteAddressModel.AddressId))
                        .Throws(new AddressNotFoundException("Address Not Found!!!"));
                    _returnedValue = _customerController.DeleteAddress(_deleteAddressModel.CustomerId, _deleteAddressModel.AddressId).Result;
                }

                [Test]
                public void TestAddressNotFoundException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedCustomerRepository.Verify(
                        x => x.DeleteAddress(_deleteAddressModel.CustomerId, _deleteAddressModel.AddressId),
                        Times.Once(),
                        "DeleteAddress not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Address Not Found!!!"), "Excption Message not correct..");
                }
            }
        }

        public class GetCustomerAddressesTests : CustomerControllerTests
        {
            private IActionResult _returnedValue;
            private Guid _customerId = Guid.NewGuid();

            protected override void BecauseOf()
            {
                base.BecauseOf();
            }

            public class GetCustomerAddressesSucess : GetCustomerAddressesTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _returnedValue = _customerController.GetCustomerAddresses(_customerId).Result;
                }

                [Test]
                public void TestGetCustomerAddressesSucess()
                {
                    _mockedCustomerRepository.Verify(x => x.GetCustomerAddresses(_customerId), Times.Once(),
                        "GetCustomerAddresses not called");
                    Assert.That(_returnedValue, Is.InstanceOf<OkObjectResult>(), "Returned result not correct");
                }
            }

            public class GetCustomerAddressesThrowException : GetCustomerAddressesTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x => x.GetCustomerAddresses(_customerId))
                        .Throws(new Exception("Object Reference Exception!!"));
                    _returnedValue = _customerController.GetCustomerAddresses(_customerId).Result;
                }

                [Test]
                public void TestGetCustomerAddressesThrowException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<ObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as ObjectResult;
                    _mockedCustomerRepository.Verify(x => x.GetCustomerAddresses(_customerId), Times.Once(),
                        "GetCustomerAddresses not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(500, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Object Reference Exception!!"),
                        "Excption Message not correct..");
                }
            }
        }

        public class AddPaymentMethodTests : CustomerControllerTests
        {
            private AddPaymentMethodModel _addPaymentMethodModel;
            private IActionResult _returnedValue;

            protected override void BecauseOf()
            {
                base.BecauseOf();
                _addPaymentMethodModel = new AddPaymentMethodModel()
                {
                    CustomerId = Guid.NewGuid(),
                    Name = "Test",
                    CardCompany = "TestCompay",
                    CardNumber = "5678986789",
                    ExpiryDate = DateTime.Today.AddDays(1)
                };
            }

            public class AddPaymentMethodSucess : AddPaymentMethodTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _returnedValue = _customerController.AddPaymentMethod(_addPaymentMethodModel).Result;
                }

                [Test]
                public void TestAddPaymentMethodSucess()
                {
                    _mockedCustomerRepository.Verify(x => x.AddPaymentMethod(It.IsAny<PaymentMethod>()), Times.Once,
                        "AddPaymentMethod not called");
                    Assert.That(_returnedValue, Is.InstanceOf<OkResult>(), "Returned result not correct");
                }
            }

            public class AddPaymentMethodThrowException : AddPaymentMethodTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x => x.AddPaymentMethod(It.IsAny<PaymentMethod>()))
                        .Throws(new Exception("Object Reference Exception!!"));
                    _returnedValue = _customerController.AddPaymentMethod(_addPaymentMethodModel).Result;
                }

                [Test]
                public void TestAddPaymentMethodThrowException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<ObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as ObjectResult;
                    _mockedCustomerRepository.Verify(x => x.AddPaymentMethod(It.IsAny<PaymentMethod>()), Times.Once,
                        "AddPaymentMethod not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(500, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Object Reference Exception!!"),
                        "Excption Message not correct..");
                }
            }

            public class CustomerNotFoundExceptionThrown : AddPaymentMethodTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x => x.AddPaymentMethod(It.IsAny<PaymentMethod>()))
                        .Throws(new CustomerNotFoundException("Customer Not Found!!"));
                    _returnedValue = _customerController.AddPaymentMethod(_addPaymentMethodModel).Result;
                }

                [Test]
                public void TestCustomerNotFoundExceptionThrown()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedCustomerRepository.Verify(x => x.AddPaymentMethod(It.IsAny<PaymentMethod>()), Times.Once,
                        "AddPaymentMethod not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Customer Not Found!!"), "Excption Message not correct..");
                }
            }
        }

        public class EditPaymentMethodTests : CustomerControllerTests
        {
            private EditPaymentMethodModel _editPaymentMethodModel;
            private IActionResult _returnedValue;

            protected override void BecauseOf()
            {
                base.BecauseOf();
                _editPaymentMethodModel = new EditPaymentMethodModel()
                {
                    CustomerId = Guid.NewGuid(),
                    PaymentMethodId = Guid.NewGuid(),
                    Name = "Test",
                    CardCompany = "TestCompay",
                    CardNumber = "5678986789",
                    ExpiryDate = DateTime.Today.AddDays(1)
                };
            }

            public class EditPaymentMethodSucess : EditPaymentMethodTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _returnedValue = _customerController.EditPaymentMethod(_editPaymentMethodModel).Result;
                }

                [Test]
                public void TestEditPaymentMethodSucess()
                {
                    _mockedCustomerRepository.Verify(
                        x => x.EditPaymentMethod(_editPaymentMethodModel.CustomerId,
                            _editPaymentMethodModel.PaymentMethodId, _editPaymentMethodModel.Name,
                            _editPaymentMethodModel.CardNumber, _editPaymentMethodModel.CardCompany,
                            _editPaymentMethodModel.ExpiryDate), Times.Once,
                        "EditPaymentMethod not called");
                    Assert.That(_returnedValue, Is.InstanceOf<OkResult>(), "Returned result not correct");
                }
            }

            public class EditPaymentMethodThrowException : EditPaymentMethodTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x => x.EditPaymentMethod(_editPaymentMethodModel.CustomerId,
                            _editPaymentMethodModel.PaymentMethodId, _editPaymentMethodModel.Name,
                            _editPaymentMethodModel.CardNumber, _editPaymentMethodModel.CardCompany,
                            _editPaymentMethodModel.ExpiryDate))
                        .Throws(new Exception("Object Reference Exception!!"));
                    _returnedValue = _customerController.EditPaymentMethod(_editPaymentMethodModel).Result;
                }

                [Test]
                public void TestEditPaymentMethodThrowException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<ObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as ObjectResult;
                    _mockedCustomerRepository.Verify(
                        x => x.EditPaymentMethod(_editPaymentMethodModel.CustomerId,
                            _editPaymentMethodModel.PaymentMethodId, _editPaymentMethodModel.Name,
                            _editPaymentMethodModel.CardNumber, _editPaymentMethodModel.CardCompany,
                            _editPaymentMethodModel.ExpiryDate), Times.Once,
                        "EditPaymentMethod not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(500, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Object Reference Exception!!"),
                        "Excption Message not correct..");
                }
            }

            public class CustomerNotFoundExceptionThrown : EditPaymentMethodTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x => x.EditPaymentMethod(_editPaymentMethodModel.CustomerId,
                            _editPaymentMethodModel.PaymentMethodId, _editPaymentMethodModel.Name,
                            _editPaymentMethodModel.CardNumber, _editPaymentMethodModel.CardCompany,
                            _editPaymentMethodModel.ExpiryDate))
                         .Throws(new CustomerNotFoundException("Customer Not Found!!"));
                    _returnedValue = _customerController.EditPaymentMethod(_editPaymentMethodModel).Result;
                }

                [Test]
                public void TestCustomerNotFoundExceptionThrown()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedCustomerRepository.Verify(
                        x => x.EditPaymentMethod(_editPaymentMethodModel.CustomerId,
                            _editPaymentMethodModel.PaymentMethodId, _editPaymentMethodModel.Name,
                            _editPaymentMethodModel.CardNumber, _editPaymentMethodModel.CardCompany,
                            _editPaymentMethodModel.ExpiryDate), Times.Once,
                        "EditPaymentMethod not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Customer Not Found!!"), "Excption Message not correct..");
                }
            }
            public class PaymentMethodNotFoundExceptionThrown : EditPaymentMethodTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x => x.EditPaymentMethod(_editPaymentMethodModel.CustomerId,
                            _editPaymentMethodModel.PaymentMethodId, _editPaymentMethodModel.Name,
                            _editPaymentMethodModel.CardNumber, _editPaymentMethodModel.CardCompany,
                            _editPaymentMethodModel.ExpiryDate))
                         .Throws(new PaymentMethodNotFoundException("Payment Method Not Found!!"));
                    _returnedValue = _customerController.EditPaymentMethod(_editPaymentMethodModel).Result;
                }

                [Test]
                public void TestPaymentMethodNotFoundExceptionThrown()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedCustomerRepository.Verify(
                        x => x.EditPaymentMethod(_editPaymentMethodModel.CustomerId,
                            _editPaymentMethodModel.PaymentMethodId, _editPaymentMethodModel.Name,
                            _editPaymentMethodModel.CardNumber, _editPaymentMethodModel.CardCompany,
                            _editPaymentMethodModel.ExpiryDate), Times.Once,
                        "EditPaymentMethod not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Payment Method Not Found!!"), "Excption Message not correct..");
                }
            }
        }

        public class GetPaymentMethodTests : CustomerControllerTests
        {
            private Guid _customerId = Guid.NewGuid();
            private Guid _paymentMethodId = Guid.NewGuid();
            private IActionResult _returnedValue;

            protected override void BecauseOf()
            {
                base.BecauseOf();
            }

            public class GetPaymentMethodSucess : GetPaymentMethodTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _returnedValue = _customerController.GetPaymentMethod(_customerId, _paymentMethodId).Result;
                }

                [Test]
                public void TestGetPaymentMethodSucess()
                {
                    _mockedCustomerRepository.Verify(x => x.GetPaymentMethod(_customerId, _paymentMethodId), Times.Once,
                        "GetPaymentMethod not called");
                    Assert.That(_returnedValue, Is.InstanceOf<OkObjectResult>(), "Returned result not correct");
                }
            }

            public class GetPaymentMethodThrowException : GetPaymentMethodTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x => x.GetPaymentMethod(_customerId, _paymentMethodId))
                        .Throws(new Exception("Object Reference Exception!!"));
                    _returnedValue = _customerController.GetPaymentMethod(_customerId, _paymentMethodId).Result;
                }

                [Test]
                public void TestGetPaymentMethodThrowException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<ObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as ObjectResult;
                    _mockedCustomerRepository.Verify(x => x.GetPaymentMethod(_customerId, _paymentMethodId), Times.Once,
                        "GetPaymentMethod not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(500, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Object Reference Exception!!"),
                        "Excption Message not correct..");
                }
            }

            public class CustomerNotFoundExceptionThrown : GetPaymentMethodTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x => x.GetPaymentMethod(_customerId, _paymentMethodId))
                        .Throws(new CustomerNotFoundException("Customer Not Found!!"));
                    _returnedValue = _customerController.GetPaymentMethod(_customerId, _paymentMethodId).Result;
                }

                [Test]
                public void TestCustomerNotFoundExceptionThrown()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedCustomerRepository.Verify(x => x.GetPaymentMethod(_customerId, _paymentMethodId), Times.Once,
                        "GetPaymentMethod not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Customer Not Found!!"), "Excption Message not correct..");
                }
            }
        }

        public class GetAllPaymentMethodsTests : CustomerControllerTests
        {
            private Guid _customerId = Guid.NewGuid();
            private IActionResult _returnedValue;

            protected override void BecauseOf()
            {
                base.BecauseOf();
            }

            public class GetAllPaymentMethodsSucess : GetAllPaymentMethodsTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _returnedValue = _customerController.GetAllPaymentMethods(_customerId).Result;
                }

                [Test]
                public void TestGetAllPaymentMethodsSucess()
                {
                    _mockedCustomerRepository.Verify(x => x.GetAllPaymentMethods(_customerId), Times.Once,
                        "GetPaymentMethod not called");
                    Assert.That(_returnedValue, Is.InstanceOf<OkObjectResult>(), "Returned result not correct");
                }
            }

            public class GetPaymentMethodThrowException : GetAllPaymentMethodsTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x => x.GetAllPaymentMethods(_customerId))
                        .Throws(new Exception("Object Reference Exception!!"));
                    _returnedValue = _customerController.GetAllPaymentMethods(_customerId).Result;
                }

                [Test]
                public void TestGetAllPaymentMethodThrowException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<ObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as ObjectResult;
                    _mockedCustomerRepository.Verify(x => x.GetAllPaymentMethods(_customerId), Times.Once,
                        "GetAllPaymentMethods not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(500, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Object Reference Exception!!"),
                        "Excption Message not correct..");
                }
            }

            public class CustomerNotFoundExceptionThrown : GetAllPaymentMethodsTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x => x.GetAllPaymentMethods(_customerId))
                        .Throws(new CustomerNotFoundException("Customer Not Found!!"));
                    _returnedValue = _customerController.GetAllPaymentMethods(_customerId).Result;
                }

                [Test]
                public void TestCustomerNotFoundExceptionThrown()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedCustomerRepository.Verify(x => x.GetAllPaymentMethods(_customerId), Times.Once,
                        "GetAllPaymentMethods not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Customer Not Found!!"), "Excption Message not correct..");
                }
            }
        }

        public class DeletePaymentMethodTests : CustomerControllerTests
        {
            private IActionResult _returnedValue;
            private DeletePaymentMethodModel _deletePaymentMethodModel;

            protected override void BecauseOf()
            {
                base.BecauseOf();
                _deletePaymentMethodModel = new DeletePaymentMethodModel()
                {
                    CustomerId = Guid.NewGuid(),
                    PaymentMethodId = Guid.NewGuid()
                };
            }

            public class DeletePaymentMethodSucess : DeletePaymentMethodTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _returnedValue = _customerController.DeletePaymentMethod(_deletePaymentMethodModel.CustomerId, _deletePaymentMethodModel.PaymentMethodId).Result;
                }

                [Test]
                public void TestDeletePaymentMethodSucess()
                {
                    _mockedCustomerRepository.Verify(
                        x => x.DeletePaymentMethod(_deletePaymentMethodModel.CustomerId,
                            _deletePaymentMethodModel.PaymentMethodId),
                        Times.Once(),
                        "DeletePaymentMethod not called");
                    Assert.That(_returnedValue, Is.InstanceOf<OkResult>(), "Returned result not correct");
                }
            }

            public class DeletePaymentMethodThrowException : DeletePaymentMethodTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x =>
                            x.DeletePaymentMethod(_deletePaymentMethodModel.CustomerId,
                                _deletePaymentMethodModel.PaymentMethodId))
                        .Throws(new Exception("Object Reference Exception!!"));
                    _returnedValue = _customerController.DeletePaymentMethod(_deletePaymentMethodModel.CustomerId, _deletePaymentMethodModel.PaymentMethodId).Result;
                }

                [Test]
                public void TestDeletePaymentMethodThrowException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<ObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as ObjectResult;
                    _mockedCustomerRepository.Verify(
                        x => x.DeletePaymentMethod(_deletePaymentMethodModel.CustomerId,
                            _deletePaymentMethodModel.PaymentMethodId),
                        Times.Once(),
                        "DeletePaymentMethod not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(500, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Object Reference Exception!!"),
                        "Excption Message not correct..");
                }
            }

            public class PaymentMethodNotFoundExceptionTest : DeletePaymentMethodTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedCustomerRepository.Setup(x =>
                            x.DeletePaymentMethod(_deletePaymentMethodModel.CustomerId,
                                _deletePaymentMethodModel.PaymentMethodId))
                        .Throws(new PaymentMethodNotFoundException("Payment Method Not Found!!!"));
                    _returnedValue = _customerController.DeletePaymentMethod(_deletePaymentMethodModel.CustomerId, _deletePaymentMethodModel.PaymentMethodId).Result;
                }

                [Test]
                public void TestPaymentMethodNotFoundException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedCustomerRepository.Verify(
                        x => x.DeletePaymentMethod(_deletePaymentMethodModel.CustomerId,
                            _deletePaymentMethodModel.PaymentMethodId),
                        Times.Once(),
                        "DeletePaymentMethod not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Payment Method Not Found!!!"),
                        "Excption Message not correct..");
                }
            }
        }
    }
}