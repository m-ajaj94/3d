﻿using System;
using System.Collections.Generic;
using System.IO;
using GardarFramework.Test.Nunit;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using ThreeDPrinting.Api.Controllers;
using ThreeDPrinting.Api.Models;
using ThreeDPrinting.Model.Exceptions;
using ThreeDPrinting.Repository.Repositories;

namespace ThreeDPrinting.UnitTests
{
    public class ThreeDOrderControllerTests:TestBaseClass
    {
        private ThreeDOrderController _threeDOrderController;
        private Mock<IThreeDOrderRepository> _mockedThreeDOrderRepository;
        protected override void BecauseOf()
        {
            base.BecauseOf();
            _mockedThreeDOrderRepository=new Mock<IThreeDOrderRepository>();
            _threeDOrderController =new ThreeDOrderController(_mockedThreeDOrderRepository.Object);
        }
        public class CreateThreeDOrderTests:ThreeDOrderControllerTests
        {
            private ThreeDOrderModel _threeDOrderModel;
            private IActionResult _returnedValue;
            private byte[] _fileData = null;
            protected override void BecauseOf()
            {
                base.BecauseOf();
                _threeDOrderModel=new ThreeDOrderModel()
                {
                    CustomerId = Guid.NewGuid(),
                    MaterialId = Guid.NewGuid(),
                    ServiceId = Guid.NewGuid(),
                    ColorsId = new List<Guid>(),
                    OrderNote = "test"
                };
            }
            public class CreateThreeDOrderSucess : CreateThreeDOrderTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _returnedValue = _threeDOrderController.CreateThreeDOrder().Result;
                }

               // [Test]
                public void TestCreateThreeDOrderSucess()
                {
                    _mockedThreeDOrderRepository.Verify(x => x.CreateOrder(_threeDOrderModel.CustomerId,_threeDOrderModel.ServiceId,_threeDOrderModel.MaterialId,_threeDOrderModel.ColorsId,_fileData,_threeDOrderModel.OrderNote), Times.Once,
                        "CreateOrder not called");
                    Assert.That(_returnedValue, Is.InstanceOf<OkResult>(), "Returned result not correct");
                }
            }
            public class CreateThreeDOrdeThrowException : CreateThreeDOrderTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedThreeDOrderRepository.Setup(x => x.CreateOrder(_threeDOrderModel.CustomerId, _threeDOrderModel.ServiceId, _threeDOrderModel.MaterialId, _threeDOrderModel.ColorsId, _fileData, _threeDOrderModel.OrderNote))
                        .Throws(new Exception("Object Reference Exception!!"));
                    _returnedValue = _threeDOrderController.CreateThreeDOrder().Result;
                }

                //[Test]
                public void TestCreateThreeDOrdeThrowException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<ObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as ObjectResult;
                    _mockedThreeDOrderRepository.Verify(x => x.CreateOrder(_threeDOrderModel.CustomerId, _threeDOrderModel.ServiceId, _threeDOrderModel.MaterialId, _threeDOrderModel.ColorsId, It.IsAny<byte []>(), _threeDOrderModel.OrderNote), Times.Once,
                        "CreateOrder not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(500, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Object Reference Exception!!"),
                        "Excption Message not correct..");
                }
            }
            public class CustomerNotFoundExceptionThrown : CreateThreeDOrderTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedThreeDOrderRepository.Setup(x => x.CreateOrder(_threeDOrderModel.CustomerId, _threeDOrderModel.ServiceId, _threeDOrderModel.MaterialId, _threeDOrderModel.ColorsId, _fileData, _threeDOrderModel.OrderNote))
                        .Throws(new CustomerNotFoundException("Customer Not Found!!"));
                    _returnedValue = _threeDOrderController.CreateThreeDOrder().Result;
                }

               // [Test]
                public void TestCustomeNotFoundExceptionThrown()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedThreeDOrderRepository.Verify(x => x.CreateOrder(_threeDOrderModel.CustomerId, _threeDOrderModel.ServiceId, _threeDOrderModel.MaterialId, _threeDOrderModel.ColorsId, _fileData, _threeDOrderModel.OrderNote), Times.Once,
                        "CreateOrder not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Customer Not Found!!"), "Excption Message not correct..");
                }
            }
            public class ServiceNotFoundExceptionThrown : CreateThreeDOrderTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedThreeDOrderRepository.Setup(x => x.CreateOrder(_threeDOrderModel.CustomerId, _threeDOrderModel.ServiceId, _threeDOrderModel.MaterialId, _threeDOrderModel.ColorsId, _fileData, _threeDOrderModel.OrderNote))
                        .Throws(new ServiceNotFoundException("Service Not Found!!"));
                    _returnedValue = _threeDOrderController.CreateThreeDOrder().Result;
                }

               // [Test]
                public void TestServiceNotFoundExceptionThrown()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedThreeDOrderRepository.Verify(x => x.CreateOrder(_threeDOrderModel.CustomerId, _threeDOrderModel.ServiceId, _threeDOrderModel.MaterialId, _threeDOrderModel.ColorsId, _fileData, _threeDOrderModel.OrderNote), Times.Once,
                        "CreateOrder not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Service Not Found!!"), "Excption Message not correct..");
                }
            }
            public class MaterialNotFoundExceptionThrown : CreateThreeDOrderTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedThreeDOrderRepository.Setup(x => x.CreateOrder(_threeDOrderModel.CustomerId, _threeDOrderModel.ServiceId, _threeDOrderModel.MaterialId, _threeDOrderModel.ColorsId, _fileData, _threeDOrderModel.OrderNote))
                        .Throws(new MaterialNotFoundException("Material Not Found!!"));
                    _returnedValue = _threeDOrderController.CreateThreeDOrder().Result;
                }

               // [Test]
                public void TestMaterialNotFoundExceptionThrown()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedThreeDOrderRepository.Verify(x => x.CreateOrder(_threeDOrderModel.CustomerId, _threeDOrderModel.ServiceId, _threeDOrderModel.MaterialId, _threeDOrderModel.ColorsId, _fileData, _threeDOrderModel.OrderNote), Times.Once,
                        "CreateOrder not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Material Not Found!!"), "Excption Message not correct..");
                }
            }
        }
    }
}