﻿using System;
using System.Collections.Generic;
using GardarFramework.Test.Nunit;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using ThreeDPrinting.Api.Controllers;
using ThreeDPrinting.Api.Models;
using ThreeDPrinting.Model.AggregateRoot;
using ThreeDPrinting.Model.Enums;
using ThreeDPrinting.Model.Exceptions;
using ThreeDPrinting.Repository.Repositories;

namespace ThreeDPrinting.UnitTests
{
    public class MaterialControllerTests : TestBaseClass
    {
        private MaterialController _materialController;
        private Mock<IMaterialRepository> _mockedMaterialRepository;

        protected override void BecauseOf()
        {
            base.BecauseOf();
            _mockedMaterialRepository = new Mock<IMaterialRepository>();
            _materialController = new MaterialController(_mockedMaterialRepository.Object);
        }

        public class AddMaterialTests : MaterialControllerTests
        {
            private AddMaterialModel _addMaterialModel;
            private IActionResult _returnedValue;

            protected override void BecauseOf()
            {
                base.BecauseOf();
                _addMaterialModel = new AddMaterialModel()
                {
                    MaterialName = "Iron",
                    Currency = CurrencyType.USDollar,
                    PricePerGram = 100
                };
            }

            public class AddMaterialMethodSucess : AddMaterialTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _returnedValue = _materialController.AddMaterial(_addMaterialModel).Result;
                }

                [Test]
                public void TestAddMaterialSucess()
                {
                    _mockedMaterialRepository.Verify(x => x.AddMaterial(It.IsAny<Material>(), It.IsAny<List<Guid>>()),
                        Times.Once,
                        "AddMaterial not called");
                    Assert.That(_returnedValue, Is.InstanceOf<OkResult>(), "Returned result not correct");
                }
            }

            public class AddMaterialThrowException : AddMaterialTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedMaterialRepository.Setup(x => x.AddMaterial(It.IsAny<Material>(), It.IsAny<List<Guid>>()))
                        .Throws(new Exception("Object Reference Exception!!"));
                    _returnedValue = _materialController.AddMaterial(_addMaterialModel).Result;
                }

                [Test]
                public void TestAddMaterialThrowException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<ObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as ObjectResult;
                    _mockedMaterialRepository.Verify(x => x.AddMaterial(It.IsAny<Material>(), It.IsAny<List<Guid>>()),
                        Times.Once,
                        "AddMaterial not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(500, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Object Reference Exception!!"),
                        "Excption Message not correct..");
                }
            }

            public class MaterialAlreadyExistExceptionThrown : AddMaterialTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedMaterialRepository.Setup(x => x.AddMaterial(It.IsAny<Material>(), It.IsAny<List<Guid>>()))
                        .Throws(new MaterialAlreadyExistException("Material Already Exist Exception !!"));
                    _returnedValue = _materialController.AddMaterial(_addMaterialModel).Result;
                }

                [Test]
                public void TestMaterialAlreadyExistExceptionThrown()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedMaterialRepository.Verify(x => x.AddMaterial(It.IsAny<Material>(), It.IsAny<List<Guid>>()),
                        Times.Once,
                        "AddMaterial not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Material Already Exist Exception !!"),
                        "Excption Message not correct..");
                }
            }
        }

        public class EditMaterialTests : MaterialControllerTests
        {
            private EditMaterialModel _editMaterialModel;
            private IActionResult _returnedValue;

            protected override void BecauseOf()
            {
                base.BecauseOf();
                _editMaterialModel = new EditMaterialModel()
                {
                    MaterialId = Guid.NewGuid(),
                    MaterialName = "Green",
                    Currency = CurrencyType.Euro,
                    PricePerGram = 50
                };
            }

            public class EditMaterialSucess : EditMaterialTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _returnedValue = _materialController.EditMaterial(_editMaterialModel).Result;
                }

                [Test]
                public void TestEditMaterialSucess()
                {
                    _mockedMaterialRepository.Verify(
                        x => x.EditMaterial(_editMaterialModel.MaterialId, _editMaterialModel.MaterialName,
                            _editMaterialModel.Currency, _editMaterialModel.PricePerGram, It.IsAny<List<Guid>>()),
                        Times.Once,
                        "EditMaterial not called");
                    Assert.That(_returnedValue, Is.InstanceOf<OkResult>(), "Returned result not correct");
                }
            }

            public class EditMaterialThrowException : EditMaterialTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedMaterialRepository.Setup(x => x.EditMaterial(_editMaterialModel.MaterialId,
                            _editMaterialModel.MaterialName, _editMaterialModel.Currency,
                            _editMaterialModel.PricePerGram, It.IsAny<List<Guid>>()))
                        .Throws(new Exception("Object Reference Exception!!"));
                    _returnedValue = _materialController.EditMaterial(_editMaterialModel).Result;
                }

                [Test]
                public void TestEditMaterialThrowException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<ObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as ObjectResult;
                    _mockedMaterialRepository.Verify(
                        x => x.EditMaterial(_editMaterialModel.MaterialId, _editMaterialModel.MaterialName,
                            _editMaterialModel.Currency, _editMaterialModel.PricePerGram, It.IsAny<List<Guid>>()),
                        Times.Once,
                        "EditMaterial not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(500, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Object Reference Exception!!"),
                        "Excption Message not correct..");
                }
            }

            public class MaterialNotFoundExceptionThrown : EditMaterialTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedMaterialRepository.Setup(x => x.EditMaterial(_editMaterialModel.MaterialId,
                            _editMaterialModel.MaterialName, _editMaterialModel.Currency,
                            _editMaterialModel.PricePerGram, It.IsAny<List<Guid>>()))
                        .Throws(new MaterialNotFoundException("Material Not Found!!"));
                    _returnedValue = _materialController.EditMaterial(_editMaterialModel).Result;
                }

                [Test]
                public void TestMaterialNotFoundExceptionThrown()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedMaterialRepository.Verify(
                        x => x.EditMaterial(_editMaterialModel.MaterialId, _editMaterialModel.MaterialName,
                            _editMaterialModel.Currency, _editMaterialModel.PricePerGram, It.IsAny<List<Guid>>()),
                        Times.Once,
                        "EditMaterial not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Material Not Found!!"), "Excption Message not correct..");
                }
            }
        }

        public class DeleteMaterialTests : MaterialControllerTests
        {
            private IActionResult _returnedValue;
            private Guid _MaterialId = Guid.NewGuid();

            protected override void BecauseOf()
            {
                base.BecauseOf();
            }

            public class DeleteMaterialSucess : DeleteMaterialTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _returnedValue = _materialController.DeleteMaterial(_MaterialId).Result;
                }

                [Test]
                public void TestDeleteMaterialSucess()
                {
                    _mockedMaterialRepository.Verify(
                        x => x.DeleteMaterial(_MaterialId),
                        Times.Once(),
                        "DeleteMaterial not called");
                    Assert.That(_returnedValue, Is.InstanceOf<OkResult>(), "Returned result not correct");
                }
            }

            public class DeleteMaterialThrowException : DeleteMaterialTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedMaterialRepository.Setup(x =>
                            x.DeleteMaterial(_MaterialId))
                        .Throws(new Exception("Object Reference Exception!!"));
                    _returnedValue = _materialController.DeleteMaterial(_MaterialId).Result;
                }

                [Test]
                public void TestDeleteMaterialThrowException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<ObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as ObjectResult;
                    _mockedMaterialRepository.Verify(
                        x => x.DeleteMaterial(_MaterialId),
                        Times.Once(),
                        "DeleteMaterial not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(500, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Object Reference Exception!!"),
                        "Excption Message not correct..");
                }
            }

            public class MaterialNotFoundExceptionThrown : DeleteMaterialTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedMaterialRepository.Setup(x => x.DeleteMaterial(_MaterialId))
                        .Throws(new MaterialNotFoundException("Material Not Found!!"));
                    _returnedValue = _materialController.DeleteMaterial(_MaterialId).Result;
                }

                [Test]
                public void TestMaterialNotFoundExceptionThrown()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedMaterialRepository.Verify(
                        x => x.DeleteMaterial(_MaterialId), Times.Once,
                        "DeleteMaterial not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Material Not Found!!"), "Excption Message not correct..");
                }
            }
        }

        public class GetMaterialTests : MaterialControllerTests
        {
            private Guid _MaterialId = Guid.NewGuid();
            private IActionResult _returnedValue;

            protected override void BecauseOf()
            {
                base.BecauseOf();
            }

            public class GetMaterialSucess : GetMaterialTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _returnedValue = _materialController.GetMaterial(_MaterialId).Result;
                }

                [Test]
                public void TestGetMaterialSucess()
                {
                    _mockedMaterialRepository.Verify(x => x.GetMaterial(_MaterialId), Times.Once,
                        "GetMaterial not called");
                    Assert.That(_returnedValue, Is.InstanceOf<OkObjectResult>(), "Returned result not correct");
                }
            }

            public class GetMaterialThrowException : GetMaterialTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedMaterialRepository.Setup(x => x.GetMaterial(_MaterialId))
                        .Throws(new Exception("Object Reference Exception!!"));
                    _returnedValue = _materialController.GetMaterial(_MaterialId).Result;
                }

                [Test]
                public void TestGetMaterialThrowException()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<ObjectResult>(), "Returned result not correct");
                    var result = _returnedValue as ObjectResult;
                    _mockedMaterialRepository.Verify(x => x.GetMaterial(_MaterialId), Times.Once,
                        "GetPaymentMethod not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(500, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Object Reference Exception!!"),
                        "Excption Message not correct..");
                }
            }

            public class MaterialNotFoundExceptionThrown : GetMaterialTests
            {
                protected override void BecauseOf()
                {
                    base.BecauseOf();
                    _mockedMaterialRepository.Setup(x => x.GetMaterial(_MaterialId))
                        .Throws(new MaterialNotFoundException("Material Not Found!!"));
                    _returnedValue = _materialController.GetMaterial(_MaterialId).Result;
                }

                [Test]
                public void TestMaterialNotFoundExceptionThrown()
                {
                    Assert.That(_returnedValue, Is.InstanceOf<BadRequestObjectResult>(),
                        "Returned result not correct");
                    var result = _returnedValue as BadRequestObjectResult;
                    _mockedMaterialRepository.Verify(x => x.GetMaterial(_MaterialId), Times.Once,
                        "GetMaterial not called");
                    Assert.IsNotNull(result, "Return Value is null");
                    Assert.That(400, Is.EqualTo(result.StatusCode), "Status Code not correct");
                    Assert.That(result.Value, Is.EqualTo("Material Not Found!!"), "Excption Message not correct..");
                }
            }
        }
    }
}