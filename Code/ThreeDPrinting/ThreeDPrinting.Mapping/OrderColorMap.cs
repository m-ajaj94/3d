﻿using System;
using ThreeDPrinting.Model.AggregateRoot;
using GardarFramework.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ThreeDPrinting.Model.Entities;
using ThreeDPrinting.Model.Identity;

namespace ThreeDPrinting.Mapping
{
    public class OrderColorMap : IEntityTypeConfiguration<CustomerOrderColor>
    {
        public void Configure(EntityTypeBuilder<CustomerOrderColor> builder)
        {
            builder.HasKey(x => new {x.CustomerOrderId,x.ColorId});
            builder.HasOne(x => x.CustomerOrder).WithMany(x => x.OrderColors).HasForeignKey(x => x.CustomerOrderId);
            builder.HasOne(x => x.Color).WithMany(x => x.OrderColors).HasForeignKey(x => x.ColorId);
        }
    }
}