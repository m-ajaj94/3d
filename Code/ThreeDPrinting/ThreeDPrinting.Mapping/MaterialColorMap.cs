﻿using System;
using ThreeDPrinting.Model.AggregateRoot;
using GardarFramework.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ThreeDPrinting.Model.Entities;

namespace ThreeDPrinting.Mapping
{
    public class MaterialColorMap : IEntityTypeConfiguration<MaterialColor>
    {
        public void Configure(EntityTypeBuilder<MaterialColor> builder)
        {
            builder.HasKey(x => new {x.MaterialId,x.ColorId});
            builder.HasOne(x => x.Color).WithMany(x => x.MaterialColors).HasForeignKey(x => x.ColorId);
            builder.HasOne(x => x.Material).WithMany(x => x.MaterialColors).HasForeignKey(x => x.MaterialId);
        }
    }
}