﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ThreeDPrinting.Model.Entities;
using ThreeDPrinting.Model.Identity;

namespace ThreeDPrinting.Mapping
{
    public class POSMaterialMap : IEntityTypeConfiguration<POSMaterial>
    {
        public void Configure(EntityTypeBuilder<POSMaterial> builder)
        {
            builder.HasKey(x => new { x.POSId, x.MaterialId });
            builder.HasOne(x => x.Material).WithMany(x => x.POSMaterials).HasForeignKey(x => x.MaterialId);
            builder.HasOne(x => x.POS).WithMany(x => x.POSMaterials).HasForeignKey(x => x.POSId);
        }
    }
}