﻿using System;
using ThreeDPrinting.Model.AggregateRoot;
using GardarFramework.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ThreeDPrinting.Model.Entities;

namespace ThreeDPrinting.Mapping
{
    public class POSMap : IEntityTypeConfiguration<POS>
    {
        public void Configure(EntityTypeBuilder<POS> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.POSUser);
            builder.Property(x => x.Email).IsRequired();
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.Telephone).IsRequired();
        }
    }
}