﻿using System;
using ThreeDPrinting.Model.AggregateRoot;
using GardarFramework.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ThreeDPrinting.Model.Entities;

namespace ThreeDPrinting.Mapping
{
    public class AttachedFileMap : IEntityTypeConfiguration<AttachedFile>
    {
        public void Configure(EntityTypeBuilder<AttachedFile> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.FileData).IsRequired();
            builder.HasOne(x => x.Order);
        }
    }
}