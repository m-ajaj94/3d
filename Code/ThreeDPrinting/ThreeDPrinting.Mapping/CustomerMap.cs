﻿using System;
using ThreeDPrinting.Model.AggregateRoot;
using GardarFramework.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ThreeDPrinting.Mapping
{
    public class CustomerMap : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
           builder.HasKey(x => x.Id);
           builder.HasMany(x => x.Addresses).WithOne(x => x.Customer);
           builder.HasMany(x => x.PaymentMethods).WithOne(x => x.Customer);
           builder.HasMany(x => x.Orders).WithOne(x => x.Customer);
           builder.HasOne(x => x.User);
        }
    }
}