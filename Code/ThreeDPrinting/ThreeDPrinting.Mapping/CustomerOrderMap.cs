﻿using System;
using ThreeDPrinting.Model.AggregateRoot;
using GardarFramework.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ThreeDPrinting.Model.Entities;

namespace ThreeDPrinting.Mapping
{
    public class CustomerOrderMap : IEntityTypeConfiguration<CustomerOrder>
    {
        public void Configure(EntityTypeBuilder<CustomerOrder> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.Material);
            builder.HasOne(x => x.Customer);
            builder.HasOne(x => x.Service);
            builder.HasMany(x => x.AttachedFiles).WithOne(x => x.Order);
        }
    }
}