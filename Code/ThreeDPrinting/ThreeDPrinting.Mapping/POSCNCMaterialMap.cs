﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ThreeDPrinting.Model.Entities;
using ThreeDPrinting.Model.Identity;

namespace ThreeDPrinting.Mapping
{
    public class POSCNCMaterialMap : IEntityTypeConfiguration<POSCNCMaterial>
    {
        public void Configure(EntityTypeBuilder<POSCNCMaterial> builder)
        {
            builder.HasKey(x => new { x.POSId, x.CNCMaterialId });
            builder.HasOne(x => x.CNCMaterial).WithMany(x => x.POSCNCMaterials).HasForeignKey(x => x.CNCMaterialId);
            builder.HasOne(x => x.POS).WithMany(x => x.POSCNCMaterials).HasForeignKey(x => x.POSId);
        }
    }
}