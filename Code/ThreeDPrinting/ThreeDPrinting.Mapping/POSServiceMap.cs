﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ThreeDPrinting.Model.Entities;
using ThreeDPrinting.Model.Identity;

namespace ThreeDPrinting.Mapping
{
    public class POSServiceMap : IEntityTypeConfiguration<POSService>
    {
        public void Configure(EntityTypeBuilder<POSService> builder)
        {
            builder.HasKey(x => new { x.POSId, x.ServiceId });
            builder.HasOne(x => x.Service).WithMany(x => x.POSServices).HasForeignKey(x => x.ServiceId);
            builder.HasOne(x => x.POS).WithMany(x => x.POSServices).HasForeignKey(x => x.POSId);
        }
    }
}