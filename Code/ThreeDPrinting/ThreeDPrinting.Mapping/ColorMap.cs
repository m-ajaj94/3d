﻿using System;
using ThreeDPrinting.Model.AggregateRoot;
using GardarFramework.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ThreeDPrinting.Mapping
{
    public class ColorMap: IEntityTypeConfiguration<Color>
    {
        public void Configure(EntityTypeBuilder<Color> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.ColorName).IsRequired();
            builder.Property(x => x.R).IsRequired();
            builder.Property(x => x.G).IsRequired();
            builder.Property(x => x.B).IsRequired();
        }
    }
}