﻿using System;
using ThreeDPrinting.Model.AggregateRoot;
using GardarFramework.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ThreeDPrinting.Model.Entities;

namespace ThreeDPrinting.Mapping
{
    public class AddressMap : IEntityTypeConfiguration<Address>
    {
        public void Configure(EntityTypeBuilder<Address> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.ApartmentSuiteUnitBuildingFloor).IsRequired();
            builder.Property(x => x.City).IsRequired();
            builder.Property(x => x.ContactName).IsRequired();
            builder.Property(x => x.Country).IsRequired();
            builder.Property(x => x.CountryCode).IsRequired();
            builder.Property(x => x.Mobile).IsRequired();
            builder.Property(x => x.StateProvinceCounty).IsRequired();
            builder.Property(x => x.StreetAddress).IsRequired();
            builder.Property(x => x.StreetAddressNumberPOBox).IsRequired();
            builder.Property(x => x.ZipPostalCode).IsRequired();
            builder.Property(x => x.ApartmentSuiteUnitBuildingFloor).IsRequired();
        }
    }
}