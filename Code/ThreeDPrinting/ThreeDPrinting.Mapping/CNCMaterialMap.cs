﻿using System;
using ThreeDPrinting.Model.AggregateRoot;
using GardarFramework.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ThreeDPrinting.Mapping
{
    public class CNCMaterialMap : IEntityTypeConfiguration<CNCMaterial>
    {
        public void Configure(EntityTypeBuilder<CNCMaterial> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.MaterialName).IsRequired();
            builder.HasOne(x => x.WorkType);
        }
    }
    
}